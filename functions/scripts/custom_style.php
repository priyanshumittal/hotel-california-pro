<?php
$enable_custom_typography = get_theme_mod('enable_custom_typography',false);
if($enable_custom_typography == true)
{
?>
<style>
 .slide-text-bg1 p, .format-status1 p, .format-aside1 p, .format-video1 p, .entry-content, .entry-content p, .sm-aboutus-section p, .blog-author p, .comment-detail p, .widget .textwidget p, .cont-description p, .error_404 p, body
{
	font-size:<?php echo get_theme_mod('general_typography_fontsize','13').'px'; ?> !important;
	line-height:<?php echo get_theme_mod('general_typography_fontsize','13') + 5 .'px'; ?> !important;
	font-family:<?php echo get_theme_mod('general_typography_fontfamily','Dosis'); ?> !important;
	font-style:<?php echo get_theme_mod('general_typography_fontstyle','normal'); ?> !important;
	
}

.navbar .nav > li > a{
	font-size:<?php echo get_theme_mod('menu_title_fontsize','18').'px'; ?> !important;
	font-family:<?php echo get_theme_mod('menu_title_fontfamily','Dosis'); ?> !important;
	font-style:<?php echo get_theme_mod('menu_title_fontstyle','normal'); ?> !important;
}

.section-title h1 {
	font-size:<?php echo get_theme_mod('section_title_fontsize','36').'px'; ?> !important;
	line-height:<?php echo get_theme_mod('section_title_fontsize','36') + 5 .'px'; ?> !important;
	font-family:<?php echo get_theme_mod('section_title_fontfamily','Dosis'); ?> !important;
	font-style:<?php echo get_theme_mod('section_title_fontstyle','normal'); ?> !important;
	
}

.section-title p{
	font-size:<?php echo get_theme_mod('section_description_fontsize','18').'px'; ?> !important;
	line-height:<?php echo get_theme_mod('section_description_fontsize','18') + 5 .'px'; ?> !important;
	font-family:<?php echo get_theme_mod('section_description_fontfamily','Dosis'); ?> !important;
	font-style:<?php echo get_theme_mod('section_description_fontstyle','normal'); ?> !important;
	
	
}

.widget .widget-title  {
	font-size:<?php echo get_theme_mod('widgets_title_fontsize','24').'px'; ?> !important;
	font-family:<?php echo get_theme_mod('widgets_title_fontfamily','Dosis'); ?> !important;
	font-style:<?php echo get_theme_mod('widgets_title_fontstyle','normal'); ?> !important;
}
</style>
<?php } ?>
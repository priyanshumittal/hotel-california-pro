<?php

add_action( 'widgets_init','graphite_contact_info_widget'); 
function graphite_contact_info_widget() 
{ 
	return   register_widget( 'graphite_contact_info_widget' );
}

class graphite_contact_info_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'graphite_contact_info_widget', // Base ID
			__('WBR : Contact Info Widget', 'graphite'), // Name
			array( 'description' => __( 'Contact template information widget', 'graphite' ), ) // Args
		);
	}

	public function widget( $args , $instance ) {
		echo $args['before_widget'];
		
		$instance['title'] = (isset($instance['title'])?$instance['title']:'');
		$instance['contact_phone'] = (isset($instance['contact_phone'])?$instance['contact_phone']:'');
		$instance['contact_email'] = (isset($instance['contact_email'])?$instance['contact_email']:'');
		$instance['contact_address'] = (isset($instance['contact_address'])?$instance['contact_address']:'');
		$instance['contact_website'] = (isset($instance['contact_website'])?$instance['contact_website']:'');
		
		$instance['other_info'] = ( $instance['other_info']!=null? $instance['other_info'] :'');
		$instance['contact_social'] = ( $instance['contact_social']!=null ? $instance['contact_social'] :'');
		
		if($instance['title'])
		echo $args['before_title'] . $instance['title'] . $args['after_title'];
		
		echo '<div class="sm-cont-widget">';
		
			echo '<div class="cont-info">';
			if($instance['contact_phone'])
			echo '<address><i class="fa fa-phone"></i> '.$instance['contact_phone'].' </address>';
			if($instance['contact_email'])
			echo '<address><i class="fa fa-envelope"></i> <a href="mailto:'.$instance['contact_email'].'">'.$instance['contact_email'].'</a></address>';
			if($instance['contact_address'])
			echo '<address><i class="fa fa-map-marker"></i> '.$instance['contact_address'].'</address>';
			if($instance['contact_website'])
			echo '<address><i class="fa fa-globe"></i> '.$instance['contact_website'].'</address>';
			echo '</div>';
			
			if($instance['other_info'])
			echo '<div class="cont-info">'.$instance['other_info'].'</div>';
			
			if($instance['contact_social'])
			echo '<ul class="cont-social">'.$instance['contact_social'].'</ul>';
		
		echo '</div>';
		echo $args['after_widget'];
	}

	public function form( $instance ) {
		
		$instance['title'] = (isset($instance['title'])?$instance['title']:'');
		$instance['contact_phone'] = (isset($instance['contact_phone'])?$instance['contact_phone']:'');
		$instance['contact_email'] = (isset($instance['contact_email'])?$instance['contact_email']:'');
		$instance['contact_address'] = (isset($instance['contact_address'])?$instance['contact_address']:'');
		$instance['contact_website'] = (isset($instance['contact_website'])?$instance['contact_website']:'');
		
		$instance['other_info'] = ( isset($instance['other_info']) ? $instance['other_info'] : '<h4 class="cont-title">'.__('Business hours','graphite').'</h4><address><b>'.__('Monday - Saturday','graphite').'</b>'.__('10am to 6pm','graphite').'</address><address><b>'.__('Sunday','graphite').'</b>'.__('Closed','graphite').'</address>');
		$instance['contact_social'] = ( isset($instance['contact_social']) ? $instance['contact_social'] : '<h4 class="cont-title">'.__('Follow us','graphite').'</h4>
		<li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
		<li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
		<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
		<li class="googleplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
		<li class="skype"><a href="#"><i class="fa fa-skype"></i></a></li>');
		?>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title','graphite' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'contact_phone' ); ?>"><?php _e( 'Phone','graphite' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'contact_phone' ); ?>" name="<?php echo $this->get_field_name( 'contact_phone' ); ?>" type="text" value="<?php echo esc_attr( $instance['contact_phone'] ); ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'contact_email' ); ?>"><?php _e( 'Email','graphite' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'contact_email' ); ?>" name="<?php echo $this->get_field_name( 'contact_email' ); ?>" type="text" value="<?php echo esc_attr( $instance['contact_email'] ); ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'contact_address' ); ?>"><?php _e( 'Address','graphite' ); ?></label> 
		<textarea rows="3" class="widefat" id="<?php echo $this->get_field_id( 'contact_address' ); ?>" name="<?php echo $this->get_field_name( 'contact_address' ); ?>"><?php echo esc_attr( $instance['contact_address'] ); ?></textarea>
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'contact_website' ); ?>"><?php _e( 'Website','graphite' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'contact_website' ); ?>" name="<?php echo $this->get_field_name( 'contact_website' ); ?>" type="text" value="<?php echo esc_attr( $instance['contact_website'] ); ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'other_info' ); ?>"><?php _e( 'Other infomation','graphite' ); ?></label> 
		<textarea rows="6" class="widefat" id="<?php echo $this->get_field_id( 'other_info' ); ?>" name="<?php echo $this->get_field_name( 'other_info' ); ?>"><?php echo esc_attr( $instance['other_info'] ); ?></textarea>
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'contact_social' ); ?>"><?php _e( 'Social icons','graphite' ); ?></label> 
		<textarea rows="6" class="widefat" id="<?php echo $this->get_field_id( 'contact_social' ); ?>" name="<?php echo $this->get_field_name( 'contact_social' ); ?>"><?php echo esc_attr( $instance['contact_social'] ); ?></textarea>
		</p>
		
		
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? $new_instance['title'] : '';
		$instance['contact_phone'] = ( ! empty( $new_instance['contact_phone'] ) ) ? $new_instance['contact_phone'] : '';
		$instance['contact_email'] = ( ! empty( $new_instance['contact_email'] ) ) ? $new_instance['contact_email'] : '';
		$instance['contact_address'] = ( ! empty( $new_instance['contact_address'] ) ) ? $new_instance['contact_address'] : '';
		$instance['contact_website'] = ( ! empty( $new_instance['contact_website'] ) ) ? $new_instance['contact_website'] : '';
		
		$instance['other_info'] = ( ! empty( $new_instance['other_info'] ) ) ? $new_instance['other_info'] : '';
		$instance['contact_social'] = ( ! empty( $new_instance['contact_social'] ) ) ? $new_instance['contact_social'] : '';
		
		return $instance;
	}

} // class
?>
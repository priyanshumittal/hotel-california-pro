<?php // Adding customizer cta section settings
function graphite_cta_section_customizer( $wp_customize ){

/* cta Section Panel */
	$wp_customize->add_panel( 'cta_section', array(
		'priority'       => 430,
		'title'      => __('Call to action top sections', 'graphite'),
	) );
	
	
		$wp_customize->add_section('home_cta_page_section',array(
		'title' => __('General section settings','graphite'),
		'panel' => 'cta_section',
		'priority'       => 10,
		));
		
			
			// enable service section
			$wp_customize->add_setting('cta_section_enable',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
			$wp_customize->add_control('cta_section_enable',array(
			'label' => __('Hide call to action top','graphite'),
			'section' => 'home_cta_page_section',
			'type' => 'checkbox',
			) );
			
		
		// CTA color settings
		$wp_customize->add_section('home_cta_color_setting',array(
		'title' => __('Background color','graphite'),
		'panel' => 'cta_section',
		'priority'       => 11,
		));		
			
			
			$wp_customize->add_setting(
			'cta_color', array(
			'capability'     => 'edit_theme_options',
			'default' => '#21202e'
			));
			
		
			
			$wp_customize->add_control( 
			new WP_Customize_Color_Control( 
			$wp_customize, 
			'cta_color', 
			array(
				'label'      => __('Background color', 'graphite' ),
				'section'    => 'home_cta_color_setting',
				'settings'   => 'cta_color',
			) ) );
			
			
			
			
			
	
}
add_action( 'customize_register', 'graphite_cta_section_customizer' );


// Adding customizer service section settings
function graphite_service_section_customizer( $wp_customize ){

/* Service Section Panel */
	$wp_customize->add_panel( 'service_section', array(
		'priority'       => 440,
		'title'      => __('Service sections', 'graphite'),
	) );
	
	
		$wp_customize->add_section('home_service_page_section',array(
		'title' => __('Section settings','graphite'),
		'panel' => 'service_section',
		'priority'       => 10,
		));
		
			
			// enable service section
			$wp_customize->add_setting('service_section_enable',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
			$wp_customize->add_control('service_section_enable',array(
			'label' => __('Hide service section from homepage','graphite'),
			'section' => 'home_service_page_section',
			'type' => 'checkbox',
			) );
			
			
			// Number of Column layout
			$wp_customize->add_setting('service_column_layout',array(
			'default' => 4,
			'sanitize_callback' => 'sanitize_text_field',
			) );

			$wp_customize->add_control('service_column_layout',array(
			'type' => 'select',
			'label' => __('Select column layout','graphite'),
			'section' => 'home_service_page_section',
			'choices' => array(1=>1,2=>2,3=>3,4=>4),
			) );
			
		$wp_customize->add_section('home_service_page_section_header',array(
			'title' => __('Section Header','graphite'),
			'panel' => 'service_section',
			'priority'       => 10,
			));	
		
		    // room section title
			$wp_customize->add_setting( 'home_service_section_title',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'graphite_home_page_sanitize_text',
			));	
			$wp_customize->add_control( 'home_service_section_title',array(
			'label'   => __('Title','graphite'),
			'section' => 'home_service_page_section_header',
			'type' => 'text',
			));	
			
			//room section discription
			$wp_customize->add_setting( 'home_service_section_discription',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'graphite_home_page_sanitize_text',
			));	
			$wp_customize->add_control( 'home_service_section_discription',array(
			'label'   => __('Description','graphite'),
			'section' => 'home_service_page_section_header',
			'type' => 'textarea',
			));		
			
			
			
	
}
add_action( 'customize_register', 'graphite_service_section_customizer' );

// Adding customizer room section settings
function graphite_room_section_customizer( $wp_customize ){

/* Room Section Panel */
	$wp_customize->add_panel( 'room_section', array(
		'priority'       => 450,
		'title'      => __('Room sections', 'graphite'),
	) );
	
	
		$wp_customize->add_section('home_room_page_section',array(
		'title' => __('Section settings','graphite'),
		'panel' => 'room_section',
		'priority'       => 10,
		));
		
			
			// enable room section
			$wp_customize->add_setting('room_section_enable',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
			$wp_customize->add_control('room_section_enable',array(
			'label' => __('Hide room page section','graphite'),
			'section' => 'home_room_page_section',
			'type' => 'checkbox',
			) );
			
			
			// Number of Column layout
			$wp_customize->add_setting('room_column_layout',array(
			'default' => 2,
			'sanitize_callback' => 'sanitize_text_field',
			) );

			$wp_customize->add_control('room_column_layout',array(
			'type' => 'select',
			'label' => __('Select column layout','graphite'),
			'section' => 'home_room_page_section',
			'choices' => array(1=>1,2=>2,3=>3,4=>4),
			) );
			
		$wp_customize->add_section('home_room_page_section_header',array(
			'title' => __('Section Header','graphite'),
			'panel' => 'room_section',
			'priority'       => 10,
			));	
		
		    // room section title
			$wp_customize->add_setting( 'home_room_section_title',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'graphite_home_page_sanitize_text',
			));	
			$wp_customize->add_control( 'home_room_section_title',array(
			'label'   => __('Title','graphite'),
			'section' => 'home_room_page_section_header',
			'type' => 'text',
			));	
			
			//room section discription
			$wp_customize->add_setting( 'home_room_section_discription',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'graphite_home_page_sanitize_text',
			));	
			$wp_customize->add_control( 'home_room_section_discription',array(
			'label'   => __('Description','graphite'),
			'section' => 'home_room_page_section_header',
			'type' => 'textarea',
			));		
			
			
			
	
}
add_action( 'customize_register', 'graphite_room_section_customizer' );



// Adding customizer Testimonial section settings

function graphite_testimonial_section_customizer( $wp_customize ){

/* Testimonial Section Panel */
	$wp_customize->add_panel( 'testimonial_section', array(
		'priority'       => 460,
		'title'      => __('Testimonial sections', 'graphite'),
	) );
	
	
		$wp_customize->add_section('graphite_testimonial_section',array(
		'title' => __('Section settings','graphite'),
		'panel' => 'testimonial_section',
		'priority'       => 20,
		));
		
			// enable room section
			$wp_customize->add_setting('testimonial_section_enable',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
			$wp_customize->add_control('testimonial_section_enable',array(
			'label' => __('Hide testimonial section','graphite'),
			'section' => 'graphite_testimonial_section',
			'type' => 'checkbox',
			) );
			
			// Number of Column layout
			$wp_customize->add_setting('testimonial_column_layout',array(
			'default' => 1,
			'sanitize_callback' => 'sanitize_text_field',
			) );

			$wp_customize->add_control('testimonial_column_layout',array(
			'type' => 'select',
			'label' => __('Select column layout','graphite'),
			'section' => 'graphite_testimonial_section',
			'choices' => array(1=>1,2=>2,3=>3,4=>4),
			) );
			
			
			//Testimonial Background Image
			$wp_customize->add_setting( 'testimonial_callout_background', array(
			  'sanitize_callback' => 'esc_url_raw',
			) );
			
			$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'testimonial_callout_background', array(
			  'label'    => __( 'Background Image', 'graphite' ),
			  'section'  => 'graphite_testimonial_section',
			  'settings' => 'testimonial_callout_background',
			) ) );
			
			
		// testimonial section header
		$wp_customize->add_section('home_testimonial_section_header',array(
		'title' => __('Section Header','graphite'),
		'panel' => 'testimonial_section',
		'priority'       => 20,
		));	
		
		    // testimonial section title
			$wp_customize->add_setting( 'home_testimonial_section_title',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'graphite_home_page_sanitize_text',
			));	
			$wp_customize->add_control( 'home_testimonial_section_title',array(
			'label'   => __('Title','graphite'),
			'section' => 'home_testimonial_section_header',
			'type' => 'text',
			));	
			
			//testimonial section discription
			$wp_customize->add_setting( 'home_testimonial_section_discription',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'graphite_home_page_sanitize_text',
			));	
			$wp_customize->add_control( 'home_testimonial_section_discription',array(
			'label'   => __('Description','graphite'),
			'section' => 'home_testimonial_section_header',
			'type' => 'textarea',
			));		
			
	
}
add_action( 'customize_register', 'graphite_testimonial_section_customizer' );


// Adding Latest News section settings

function graphite_latest_news_section_customizer( $wp_customize ){

/* News Section Panel */
	$wp_customize->add_panel( 'latest_news_section', array(
		'priority'       => 470,
		'title'      => __('Latest news sections', 'graphite'),
	) );
	
	
		$wp_customize->add_section('graphite_latest_news_section',array(
		'title' => __('Section settings','graphite'),
		'panel' => 'latest_news_section',
		'priority'       => 30,
		));
		
			// enable latest news section
			$wp_customize->add_setting('latest_news_section_enable',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
			$wp_customize->add_control('latest_news_section_enable',array(
			'label' => __('Hide latest news section','graphite'),
			'section' => 'graphite_latest_news_section',
			'type' => 'checkbox',
			) );
}
add_action( 'customize_register', 'graphite_latest_news_section_customizer' );


// Adding About Info section settings

function graphite_about_info_section_customizer( $wp_customize ){

/* About Info Section Panel */
	$wp_customize->add_panel( 'about_info_section', array(
		'priority'       => 480,
		'title'      => __('About info sections', 'graphite'),
	) );
	
	
		$wp_customize->add_section('graphite_about_section',array(
		'title' => __('Section settings','graphite'),
		'panel' => 'about_info_section',
		'priority'       => 40
		));
		
			// enable about info section
			$wp_customize->add_setting('about_info_section_enable',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
			$wp_customize->add_control('about_info_section_enable',array(
			'label' => __('Hide about info section','graphite'),
			'section' => 'graphite_about_section',
			'type' => 'checkbox',
			) );
}
add_action( 'customize_register', 'graphite_about_info_section_customizer' );


// Adding Google Map section settings

function graphite_google_map_section_customizer( $wp_customize ){

/* About Info Section Panel */
	$wp_customize->add_panel( 'google_map_section', array(
		'priority'       => 480,
		'title'      => __('Google map sections', 'graphite'),
	) );
	
	
		$wp_customize->add_section('graphite_google_map_section',array(
		'title' => __('Section settings','graphite'),
		'panel' => 'google_map_section',
		'priority'       => 50
		));
		
			// enable google map section
			$wp_customize->add_setting('google_map_section_enable',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
			$wp_customize->add_control('google_map_section_enable',array(
			'label' => __('Hide Google Map','graphite'),
			'section' => 'graphite_google_map_section',
			'type' => 'checkbox',
			) );
}
add_action( 'customize_register', 'graphite_google_map_section_customizer' );	


// Adding Call to Action section settings

function graphite_call_action_section_customizer( $wp_customize ){

/* About Info Section Panel */
	$wp_customize->add_panel( 'call_action_section', array(
		'priority'       => 480,
		'title'      => __('Call to action bottom sections', 'graphite'),
	) );
	
	
		$wp_customize->add_section('graphite_call_action_section',array(
		'title' => __('General section settings','graphite'),
		'panel' => 'call_action_section',
		'priority'       => 60
		));
		
			// enable google map section
			$wp_customize->add_setting('call_action_section_enable',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
			$wp_customize->add_control('call_action_section_enable',array(
			'label' => __('Hide call to action','graphite'),
			'section' => 'graphite_call_action_section',
			'type' => 'checkbox',
			) );
}
add_action( 'customize_register', 'graphite_call_action_section_customizer' );


// Gallery settings

function graphite_gallery_section_customizer( $wp_customize ){

/* About gallery Panel */
	$wp_customize->add_panel( 'gallery_section', array(
		'priority'       => 480,
		'title'      => __('Gallery sections', 'graphite'),
	) );
	
	
		
		$wp_customize->add_section('home_gallery_header',array(
			'title' => __('Section Header','graphite'),
			'panel' => 'gallery_section',
			'priority'       => 10,
			));	
		
		    // room section title
			$wp_customize->add_setting( 'home_gallery_title',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'graphite_home_page_sanitize_text',
			));	
			$wp_customize->add_control( 'home_gallery_title',array(
			'label'   => __('Title','graphite'),
			'section' => 'home_gallery_header',
			'type' => 'text',
			));	
			
			//room section discription
			$wp_customize->add_setting( 'home_gallery_discription',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'graphite_home_page_sanitize_text',
			));	
			$wp_customize->add_control( 'home_gallery_discription',array(
			'label'   => __('Description','graphite'),
			'section' => 'home_gallery_header',
			'type' => 'textarea',
			));	
		
		
		$wp_customize->add_section('graphite_gallery_section',array(
		'title' => __('Gallery settings','graphite'),
		'panel' => 'gallery_section',
		'priority'       => 60
		));
		
			// enable gallery section
			$wp_customize->add_setting('gallery_section_enable',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
			$wp_customize->add_control('gallery_section_enable',array(
			'label' => __('Hide gallery section','graphite'),
			'section' => 'graphite_gallery_section',
			'type' => 'checkbox',
			) );
			
			
			// Exlude gallery
			$wp_customize->add_setting('gallery_exclude',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
			$wp_customize->add_control('gallery_exclude',array(
			'label' => __('Exclude gallery on page','graphite'),
			'description' => __('Exclude page ID like: 23, 25','graphite'),
			'section' => 'graphite_gallery_section',
			'type' => 'textarea',
			) );
			
			
			
			// enbale/ disable carousel
			$wp_customize->add_setting('gallery_enable_carousel',array(
			'default' => true,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
			$wp_customize->add_control('gallery_enable_carousel',array(
			'label' => __('Carousel Effect','graphite'),
			'section' => 'graphite_gallery_section',
			'type' => 'checkbox',
			) );
			
			
			
			// Exlude gallery
			$wp_customize->add_setting('gallery_enable_overlay',array(
			'default' => true,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
			$wp_customize->add_control('gallery_enable_overlay',array(
			'label' => __('Overlay','graphite'),
			'section' => 'graphite_gallery_section',
			'type' => 'checkbox',
			) );
			
			
			// Remove gallery filter
 			$wp_customize->add_setting('gallery_filter_enable',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
			$wp_customize->add_control('gallery_filter_enable',array(
			'label' => __('Use only WordPress gallery','graphite'),
			'section' => 'graphite_gallery_section',
			'type' => 'checkbox',
			) );
						
}
add_action( 'customize_register', 'graphite_gallery_section_customizer' );

// Adding Additional section settings

function graphite_additional_section_customizer( $wp_customize ){

/* About Info Section Panel */
	$wp_customize->add_panel( 'additional_panel', array(
		'priority'       => 490,
		'title'      => __('Additional sections','graphite'),
	) );
	
	
		$wp_customize->add_section('graphite_additional_section',array(
		'title' => __('General section settings','graphite'),
		'panel' => 'additional_panel',
		'priority'       => 1
		));
		
			// enable service section
			$wp_customize->add_setting('additional_enable',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
			$wp_customize->add_control('additional_enable',array(
			'label' => __('Hide additional section','graphite'),
			'section' => 'graphite_additional_section',
			'type' => 'checkbox',
			) );
			
			// Number of services
			$wp_customize->add_setting('additional_column_layout',array(
			'default' => 4,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );

			$wp_customize->add_control('additional_column_layout',array(
			'type' => 'select',
			'label' => __('Select column layout','graphite'),
			'section' => 'graphite_additional_section',
			'choices' => array(1=>1,2=>2,3=>3,4=>4),
			) );
			
			
			// headings
		$wp_customize->add_section( 'additional_headings' , array(
		'title'      => __('Section Header', 'graphite'),
		'panel'  => 'additional_panel',
		'priority'       => 2
		) );
			
			// Additional title
			$wp_customize->add_setting('additional_title',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'graphite_home_page_sanitize_text',
			) );
			
			$wp_customize->add_control('additional_title',array(
			'label' => __('Title','graphite'),
			'section' => 'additional_headings',
			'type' => 'text',
			) );
			
			// service description
			$wp_customize->add_setting('additional_description',array(
			'sanitize_callback' => 'graphite_home_page_sanitize_text',
			) );
			
			$wp_customize->add_control('additional_description',array(
			'label' => __('Subtitle','graphite'),
			'section' => 'additional_headings',
			'type' => 'textarea',
			) );
}
add_action( 'customize_register', 'graphite_additional_section_customizer' );

function graphite_client_slider_section_customizer( $wp_customize ){

/* About Info Section Panel */
	$wp_customize->add_panel( 'client_panel', array(
		'priority'       => 510,
		'title'      => __('Client sections', 'graphite'),
	) );
	
	
	$wp_customize->add_section('home_client_section',array(
		'title' => __('Client settings','graphite'),
		'panel' => 'client_panel',
		));
	
		// enable service section
			$wp_customize->add_setting('client_section_enable',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
			$wp_customize->add_control('client_section_enable',array(
			'label' => __('Hide client section','graphite'),
			'section' => 'home_client_section',
			'type' => 'checkbox',
			) );
			
			// Number of Column layout
			$wp_customize->add_setting('client_layout',array(
			'default' => 1,
			'sanitize_callback' => 'sanitize_text_field',
			) );

			$wp_customize->add_control('client_layout',array(
			'type' => 'select',
			'label' => __('Select column layout','graphite'),
			'section' => 'home_client_section',
			'choices' => array(1=>1,2=>2,3=>3,4=>4),
			) );
	
	$wp_customize->add_section('home_client_section_header',array(
		'title' => __('Section Header','graphite'),
		'panel' => 'client_panel',
		));	
		
		    // client section title
			$wp_customize->add_setting( 'home_client_section_title',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'graphite_home_page_sanitize_text',
			));	
			$wp_customize->add_control( 'home_client_section_title',array(
			'label'   => __('Title','graphite'),
			'section' => 'home_client_section_header',
			'type' => 'text',
			));	
			
			//client section discription
			$wp_customize->add_setting( 'home_client_section_discription',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'graphite_home_page_sanitize_text',
			));	
			$wp_customize->add_control( 'home_client_section_discription',array(
			'label'   => __('Description','graphite'),
			'section' => 'home_client_section_header',
			'type' => 'textarea',
			));
}
add_action( 'customize_register', 'graphite_client_slider_section_customizer' );



function graphite_home_page_sanitize_text( $input ) {

			return wp_kses_post( force_balance_tags( $input ) );

			}
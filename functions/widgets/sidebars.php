<?php	
add_action( 'widgets_init', 'graphite_widgets_init');
function graphite_widgets_init() {
	
	
	//Service Layout
	$service_layout =get_theme_mod('service_column_layout',4);
	$service_layout = 12 / $service_layout;
	
	//Room Layout
	$room_layout =get_theme_mod('room_column_layout',2);
	$room_layout = 12 / $room_layout;
	
	//Amanities Layout
	$amanities_layout = get_theme_mod('amanities_column_layout',4);
	$amanities_layout = 12 / $amanities_layout;
	
	//Staff Layoutss
	$staff_layout = get_theme_mod('team_column_layout',4);
	$staff_layout = 12 / $staff_layout;
	
	
	$additional_layout = get_theme_mod('additional_column_layout',4);
	$additional_layout = 12 / $additional_layout;
	
	$testimonial_column_layout = get_theme_mod('testimonial_column_layout',1);
	$testimonial_column_layout = 12 / $testimonial_column_layout;
	
	$client_layout = get_theme_mod('client_layout',1);
	$client_layout = 12 / $client_layout;
	
	
	
	

	/*sidebar*/
	
	register_sidebar( array(
		'name' => __('Sidebar widget area','graphite'),
		'id' => 'sidebar_primary',
		'description' => __('Sidebar widget area','graphite'),
		'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInDown animated" data-wow-delay="0.4s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="section-title wow fadeInDown animated" data-wow-delay="0.4s"><h3 class="widget-title">',
		'after_title' => '</h3></div>',
	) );
		
	register_sidebar( array(
		'name' => __( 'Top header sidebar left','graphite'),
		'id' => 'home-header-sidebar_left',
		'description' => __( 'Top header sidebar left','graphite'),
		'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInDown animated" data-wow-delay="0.4s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="section-title"><h3 class="widget-title">',
		'after_title' => '</h3><span></span></div>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Top header sidebar right','graphite'),
		'id' => 'home-header-sidebar_right',
		'description' => __('Top header sidebar right','graphite'),
		'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInDown animated" data-wow-delay="0.4s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="section-title"><h3 class="widget-title">',
		'after_title' => '</h3><span></span></div>',
	) );
	
	//CTA sidebar
	register_sidebar( array(
		'name' => __('Left widget area below slider','graphite' ),
		'id' => 'cta_sidebar_left',
		'description' => __('Left widget area below slider','graphite'),
		'before_widget' => '',
		'after_widget' => '<div class="clearfix"></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __('Right widget area below slider', 'graphite' ),
		'id' => 'cta_sidebar_right',
		'description' => __('Right widget area below slider','graphite'),
		'before_widget' => '',
		'after_widget' => '<div class="clearfix"></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __('Center widget area below slider','graphite' ),
		'id' => 'cta_sidebar_center',
		'description' => __('Center widget area below slider', 'graphite'),
		'before_widget' => '',
		'after_widget' => '<div class="clearfix"></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );
	
	
	
	//Service Content Sidebar
	register_sidebar( array(
		'name' => __('Homepage service / page section sidebar', 'graphite' ),
		'id' => 'wbr_service_sidebar',
		'description' => __( 'This section for suitable widgets like WBR : Page / Service Widget.', 'graphite' ),
		'before_widget' => '<div id="%1$s" class="widget col-md-'.$service_layout.' %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="0ms">',
		'after_title' => '</h3>'
	));
	
	
	
	// business content top sidebar
	register_sidebar( array(
		'name' => __( 'Homepage rooms section sidebar', 'graphite' ),
		'id' => 'wdl_top_sidebar',
		'description' => __( 'This section for suitable widgets like WBR: Rooms widget.', 'graphite' ),
		'before_widget' => '<div id="%1$s" class="widget col-md-'.$room_layout.' %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="0ms">',
		'after_title' => '</h3>'
	));
	
	// business content middle sidebar
	register_sidebar( array(
		'name' => __( 'Homepage testimonial section sidebar','graphite' ),
		'id' => 'wdl_middle_sidebar',
		'description' => __( 'Homepage testimonial section sidebar','graphite' ),
		'before_widget' => '<div id="%1$s" class="widget col-md-'.$testimonial_column_layout.' %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title white wow fadeInUp animated" data-wow-delay="0ms" data-wow-duration="500ms">',
		'after_title' => '</h3>',
	));
	
	// business content bottom left sidebar
	register_sidebar( array(
		'name' => __( 'Homepage latest news sidebar', 'graphite' ),
		'id' => 'wdl_bottom_left_sidebar',
		'description' => __( 'Homepage latest news sidebar', 'graphite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s wow animated fadeInLeft animated" data-wow-delay="0.2s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
	
	
	
	
	// business content bottom center sidebar
	register_sidebar( array(
		'name' => __( 'Homepage about info sidebar', 'graphite' ),
		'id' => 'wdl_bottom_center_sidebar',
		'description' => __( 'Info section from About page', 'graphite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInDown animated animated" data-wow-delay="0.4s" >',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
	
	// business content bottom right sidebar
	register_sidebar( array(
		'name' => __( 'Homepage Google map sidebar', 'graphite' ),
		'id' => 'wdl_bottom_right_sidebar',
		'description' => __( 'Homepage Google map sidebar', 'graphite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s wow animated fadeInRight animated" data-wow-delay="0.2s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
	
	
	// Home Page gallery Sidebar
	register_sidebar( array(
		'name' => __( 'Homepage gallery section', 'graphite' ),
		'id' => 'wdl_gallery_section',
		'description' => __('To show gallery on homepage drag text widget and enter gallery shortcode', 'graphite' ),
		'before_widget' => '<div id="%1$s" class="padding0 widget col-md-12 %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="0ms">',
		'after_title' => '</h3>'
	) );
	
	// Home Page Additional Sidebar
	register_sidebar( array(
		'name' => __( 'Homepage additional section', 'graphite' ),
		'id' => 'wdl_additional_sidebar',
		'before_widget' => '<div id="%1$s" class="widget col-md-'.$additional_layout.' %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="0ms">',
		'after_title' => '</h3>'
	) );
	
	
	// business content bottom footer sidebar
	register_sidebar( array(
		'name' => __( 'Homepage call to action sidebar', 'graphite' ),
		'id' => 'wdl_bottom_footer_sidebar',
		'description' => __( 'Call to action widget', 'graphite' ),
		'before_widget' => '<div id="%1$s" class="col-md-12 widget %2$s ">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title wow bounceInLeft animated animated" ata-wow-delay="0.4s" style="visibility: visible; animation-name: bounceInLeft;">',
		'after_title' => '</h3>',
	));
	
	// Client Sidebar
	register_sidebar( array(
		'name' => __( 'Homepage client sidebar', 'graphite' ),
		'id' => 'wdl_client_sidebar',
		'description' => __( 'Homepage client sidebar', 'graphite' ),
		'before_widget' => '<div id="%1$s" class="widget col-md-'.$client_layout.' %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title wow bounceInLeft animated animated" ata-wow-delay="0.4s" style="visibility: visible; animation-name: bounceInLeft;">',
		'after_title' => '</h3>',
	));
	
	
	register_sidebar( array(
		'name' => __( 'Footer widget area left', 'graphite' ),
		'id' => 'footer_widget_area_left',
		'description' => __( 'Footer widget area left', 'graphite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInDown animated" data-wow-delay="0.4s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="section-title"><h3 class="widget-title">',
		'after_title' => '</h3><span></span></div>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer widget area center', 'graphite' ),
		'id' => 'footer_widget_area_center',
		'description' => __( 'Footer widget area center', 'graphite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInDown animated" data-wow-delay="0.4s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="section-title"><h3 class="widget-title">',
		'after_title' => '</h3><span></span></div>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer widget area Right', 'graphite' ),
		'id' => 'footer_widget_area_right',
		'description' => __( 'Footer widget area Right', 'graphite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInDown animated" data-wow-delay="0.4s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="section-title"><h3 class="widget-title">',
		'after_title' => '</h3><span></span></div>',
	) );
	
	
	register_sidebar( array(
		'name' => __( 'About page amenities sidebar', 'graphite' ),
		'id' => 'about_page_amenities',
		'description' => __( 'About page amenities sidebar', 'graphite' ),
		'before_widget' => '<div id="%1$s" class="col-md-'.$amanities_layout.' col-sm-6 col-xs-6 widget %2$s wow animated fadeInLeft animated" data-wow-duration="700ms" data-wow-delay="100ms">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title white">',
		'after_title' => '</h3>',
	) );
	
	register_sidebar( array(
		'name' => __( 'About page team sidebar', 'graphite' ),
		'id' => 'about_team_staff',
		'description' => __( 'About page team sidebar', 'graphite' ),
		'before_widget' => '<div id="%1$s" class="col-md-'.$staff_layout.' widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	
	
	
	
	// about page bottom footer sidebar
	register_sidebar( array(
		'name' => __( 'About page callout sidebar', 'graphite' ),
		'id' => 'wdl_about_page_bottom_footer_sidebar',
		'description' => __( 'About page callout sidebar', 'graphite' ),
		'before_widget' => '<div id="%1$s" class="col-md-12 widget %2$s ">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title wow bounceInLeft animated animated" ata-wow-delay="0.4s" style="visibility: visible; animation-name: bounceInLeft;">',
		'after_title' => '</h3>',
	));
	
	
	// Rooms template page sidebar
	register_sidebar( array(
		'name' => __( 'Room template callout sidebar','graphite'),
		'id' => 'wdl_room_page_footer_sidebar',
		'description' => __( 'Room template callout sidebar', 'graphite' ),
		'before_widget' => '<div id="%1$s" class="col-md-12 widget %2$s ">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="wow bounceInLeft animated animated" ata-wow-delay="0.4s" style="visibility: visible; animation-name: bounceInLeft;">',
		'after_title' => '</h3>',
	));
	
	// contact template page sidebar
	register_sidebar( array(
		'name' => __( 'Contact template sidebar', 'graphite' ),
		'id' => 'wdl_contact_page_sidebar',
		'description' => __('Contact template sidebar', 'graphite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInDown animated" data-wow-delay="0.4s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="section-title wow fadeInDown animated" data-wow-delay="0.4s"><h3 class="widget-title">',
		'after_title' => '</h3></div>',
	));
	
	register_sidebar( array(
	'name' => __('Woocommerce sidebar widget area', 'graphite' ),
	'id' => 'woocommerce',
	'description' => __( 'Woocommerce sidebar widget area', 'graphite' ),
	'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInDown animated" data-wow-delay="0.4s">',
	'after_widget' => '</div>',
	'before_title' => '<div class="section-title wow fadeInDown animated" data-wow-delay="0.4s"><h3 class="widget-title">',
	'after_title' => '</h3></div>',
	) );
}	                     
?>
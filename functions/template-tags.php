<?php
if ( ! function_exists( 'graphite_blog_meta_content' ) ) :
function graphite_blog_meta_content()
{ 
	$blog_meta_section_enable = get_theme_mod('blog_meta_section_enable',false);
	
	if( $blog_meta_section_enable == false ) { ?>
	<div class="entry-meta">
		<span class="author"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) );?>"><?php _e('Articles posted by ','graphite'); echo get_the_author();?></a></span>
		<span class="entry-date"><a href="<?php echo get_month_link(get_post_time('Y'),get_post_time('m')); ?>"><time datetime="" class="entry-date"><?php _e('Posted on ','graphite'); echo get_the_date('M j, Y'); ?></time></a></span>
		<?php the_tags( '<span class="tag-links">', ', ', '</span>' ); ?>				
	</div>
<?php } 
} 
endif;

// rooms with page
function feature_page_room($id,$count,$column,$image_before_title,$more_btn_text,$image_show){
	
	$args = array ( 'post_type' => 'page','page_id'=>$id);
	$loop = new WP_query( $args );
	$count = $count;
					if($loop->have_posts()):
					$loop->the_post();
					
					$temp_col = 12 / $column;
					
					?>
					<div class="col-md-<?php echo $temp_col; ?> col-sm-6 col-xs-12 pull-left">
							<div class="portfolio-content-area">
								
								<?php if( $image_before_title == false ){ ?>
									<h3 style="text-align:left;"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								<?php } ?>
								
								<div class="portfolio-img">
									<?php if(get_the_post_thumbnail_caption()) {?>
									<span class="wp-caption">
									<?php echo get_the_post_thumbnail_caption(); ?>
									</span>
									<?php } ?>
									<?php $defalt_arg =array('class' => "img-responsive"); ?>
									<?php if(has_post_thumbnail()): ?>
									<?php the_post_thumbnail('', $defalt_arg); ?>
									<?php endif; endif; ?>
								</div>
								<div class="portfolio-content">
								
									<?php if( $image_before_title == true ){ ?>
										<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<?php } ?>
									
									<?php 
										if($more_btn_text!=null){
											the_content(__('$more_btn_text','graphite'));
										}
										else{
											the_content(__('Read More','graphite'));
										}
									?>
								</div>
							</div>
					</div>
			<?php
			if($count%$column==0){
				echo "<div class='clearfix'></div>"; 
			} 
}

// rooms with posts
function  graphite_rooms($id,$count,$carousel_effect,$column)
{
                    $args = array ( 'post_type' => 'page','page_id'=>$id);
					$loop = new WP_query( $args );
					$count = $count;
					if($loop->have_posts()):
					$loop->the_post();
					
					$temp_col = 12 / $column;
					
					if($carousel_effect != true) { ?>
					<div class="item <?php echo ( $count==1 ? 'active' : ''); ?>">						
						<div class="col-md-<?php echo $temp_col; ?> col-sm-6 col-xs-12 pull-left">
							<div class="portfolio-content-area">
								<div class="portfolio-img">
									
									<span class="wp-caption">
									<?php echo get_the_post_thumbnail_caption(); ?>
									</span>
									<?php $defalt_arg =array('class' => "img-responsive"); ?>
									<?php if(has_post_thumbnail()): ?>
									<?php the_post_thumbnail('', $defalt_arg); ?>
									<?php endif; ?>
								</div>
								<div class="portfolio-content">
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<?php  the_content(__('Read More','graphite')); ?>
								</div>
							</div>
						</div>
					</div>
					<?php } else { ?>
					<div class="col-md-<?php echo $temp_col; ?> col-sm-6 col-xs-12 pull-left">
						<div class="portfolio-content-area">
							<div class="portfolio-img">
								
								<span class="wp-caption">
								<?php echo get_the_post_thumbnail_caption(); ?>
								</span>
								
								<?php $defalt_arg =array('class' => "img-responsive"); ?>
								<?php if(has_post_thumbnail()): ?>
								<?php the_post_thumbnail('', $defalt_arg); ?>
								<?php endif; ?>
							</div>
							<div class="portfolio-content">
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								<?php  the_content(__('Read More','graphite')); ?>
							</div>                                
						</div>
					</div>
						<?php
						if($count%$column==0){
							echo "<div class='clearfix'></div>"; 
						}  
					} 		
	endif; 
}

// avator class
function graphite_gravatar_class($class) {
    $class = str_replace("class='avatar", "class='img-responsive img-circle", $class);
    return $class;
}
add_filter('get_avatar','graphite_gravatar_class');


// webriti author meta
function graphite_author_meta()
{ ?>
<article class="blog-author wow fadeInDown animated" data-wow-delay="0.4s">
	<div class="media">
		<div class="pull-left">
			<?php echo get_avatar( get_the_author_meta('ID'), 200); ?>
		</div>
		<div class="media-body">
			<h6><?php the_author_link(); ?></h6>
			<p><?php the_author_meta( 'description' ); ?></p>
			<ul class="blog-author-social">
			    <?php $facebook_profile = get_the_author_meta( 'facebook_profile' ); if ( $facebook_profile && $facebook_profile != '' ): ?>
				<li class="facebook"><a href="<?php echo esc_url($facebook_profile); ?>"><i class="fa fa-facebook"></i></a></li>
				<?php endif; ?>
				<?php $linkedin_profile = get_the_author_meta( 'linkedin_profile' ); if ( $linkedin_profile && $linkedin_profile != '' ): ?>
				<li class="linkedin"><a href="<?php echo esc_url($linkedin_profile); ?>"><i class="fa fa-linkedin"></i></a></li>
				<?php endif; ?>
				<?php $twitter_profile = get_the_author_meta( 'twitter_profile' ); if ( $twitter_profile && $twitter_profile != '' ): ?>
				<li class="twitter"><a href="<?php echo esc_url($twitter_profile); ?>"><i class="fa fa-twitter"></i></a></li>
				<?php endif; ?>
				<?php $google_profile = get_the_author_meta( 'google_profile' ); if ( $google_profile && $google_profile != '' ): ?>
				<li class="googleplus"><a href="<?php echo esc_url($google_profile); ?>"><i class="fa fa-google-plus"></i></a></li>
				<?php endif; ?>
		   </ul>
		</div>
	</div>	
</article>
<?php }

// author profile data
function graphite_author_social_icons( $contactmethods ) {
		$contactmethods['facebook_profile'] = 'Facebook Profile URL';
		$contactmethods['twitter_profile'] = 'Twitter Profile URL';
		$contactmethods['linkedin_profile'] = 'Linkedin Profile URL';
		$contactmethods['google_profile'] = 'Google Profile URL';
		return $contactmethods;
	}
add_filter( 'user_contactmethods', 'graphite_author_social_icons', 10, 1);

// blogs,pages and archive page title
function graphite_archive_page_title(){
	if( is_archive() )
	{
		$archive_text = get_theme_mod('archive_prefix','Archives for');
		
		echo '<div class="page-title wow bounceInLeft animated" ata-wow-delay="0.4s"><h1>';
		
		if ( is_day() ) :
		
		  printf( __( '%1$s %2$s', 'graphite' ), $archive_text, get_the_date() );
		  
        elseif ( is_month() ) :
		
		  printf( __( '%1$s %2$s', 'graphite' ), $archive_text, get_the_date( 'F Y' ) );
		  
        elseif ( is_year() ) :
		
		  printf( __( '%1$s %2$s', 'graphite' ), $archive_text, get_the_date( 'Y' ) );
		  
        elseif( is_category() ):
		
			$category_text = get_theme_mod('category_prefix',__('Category Archive','graphite'));
			
			printf( __( '%1$s %2$s', 'graphite' ), $category_text, single_cat_title( '', false ) );
			
		elseif( is_author() ):
			
			$author_text = get_theme_mod('author_prefix',__('All posts by','graphite'));
		
			printf( __( '%1$s %2$s', 'graphite' ), $author_text, get_the_author() );
			
		elseif( is_tag() ):
			
			$tag_text = get_theme_mod('tag_prefix',__('Tag Archive','graphite'));
			
			printf( __( '%1$s %2$s', 'graphite' ), $tag_text, single_tag_title( '', false ) );
			
		elseif( is_shop() ):
			
			$shop_text = get_theme_mod('shop_prefix',__('Shop','graphite'));
			
			printf( __( '%1$s %2$s', 'graphite' ), $shop_text, single_tag_title( '', false ) );
			
        endif;

		echo '</h1></div>';
	}
	elseif( is_search() )
	{
		$search_text = get_theme_mod('search_prefix',__('Search results for','graphite'));
		
		echo '<div class="page-title wow bounceInLeft animated" ata-wow-delay="0.4s"><h1>';
		
		printf( __( '%1$s %2$s', 'graphite' ), $search_text, get_search_query() );
		
		echo '</h1></div>';
	}
	elseif( is_404() )
	{
		$breadcrumbs_text = get_theme_mod('404_prefix',__('Error 404 : Page Not Found','graphite'));
		
		echo '<div class="page-title wow bounceInLeft animated" ata-wow-delay="0.4s"><h1>';
		
		printf( __( '%1$s ', 'graphite' ) , $breadcrumbs_text );
		
		echo '</h1></div>';
	}
	else
	{
		echo '<div class="page-title wow bounceInLeft animated" ata-wow-delay="0.4s"><h1>'.get_the_title().'</h1></div>';
	}
}


function graphite_get_child_pages($parent_page_id,$class,$column = 3){
query_posts(array('post_parent' => $parent_page_id , 'post_type' => 'page'));
while (have_posts()) { the_post(); ?>
	<div class="<?php echo $class; ?>">
		<div class="portfolio-content-area">
			<div class="portfolio-img">
				<?php if(get_the_post_thumbnail_caption()) {?>
				<span class="wp-caption">
				<?php echo get_the_post_thumbnail_caption(); ?>
				</span>
				<?php } ?>
				<?php $defalt_arg =array('class' => "img-responsive"); ?>
				<?php if(has_post_thumbnail()): ?>
				<?php the_post_thumbnail('', $defalt_arg); ?>
				<?php endif; ?>
			</div>
			<div class="portfolio-content">
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>		
				<?php the_content(__('Read More','graphite')); ?>
			</div>
		</div>
	</div>
<?php
	}
}



function new_excerpt_more( $more ) {
	return '</div><div class="blog-btn"><a href="' . get_permalink() . '" class="home-blog-btn">'.__('Read More','graphite').'</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

// graphite post navigation
function graphite_post_nav()
{
	global $post;
	echo '<div style="text-align:center;">';
	posts_nav_link( ' &#183; ', __('previous page','graphite'), __('next page','graphite') );
	echo '</div>';
}

// Custom header function
if ( ! function_exists( 'graphite_header_style' ) ) :

function graphite_header_style() {
	$text_color = get_header_textcolor();

	// If no custom color for text is set, let's bail.
	if ( display_header_text() && $text_color === get_theme_support( 'custom-header', 'default-text-color' ) )
		return;
	?>
	<style type="text/css" id="graphite-header-css">
		<?php
		// Has the text been hidden?
		if ( ! display_header_text() ) :
	?>
		.site-title,
		.site-description {
			clip: rect(1px 1px 1px 1px); /* IE7 */
			clip: rect(1px, 1px, 1px, 1px);
			position: absolute;
		}
	<?php
		// If the user has set a custom color for the text, use that.
		elseif ( $text_color != get_theme_support( 'custom-header', 'default-text-color' ) ) :
	?>
		.site-title a {
			color: #<?php echo esc_attr( $text_color ); ?>;
		}
	<?php endif; ?>
	</style>
	<?php
}
endif;

add_filter('post_gallery', 'my_post_gallery', 10, 2);
function my_post_gallery($output, $attr) {
$gallery_enable_overlay = get_theme_mod('gallery_enable_overlay',true);
$gallery_enable_carousel = get_theme_mod('gallery_enable_carousel',true);
$exclude_id = get_theme_mod('gallery_exclude','');
$exclude_id = explode(',',$exclude_id);

global $post;

if(in_array(get_the_ID(),$exclude_id))
{
	
	return;
}

$galleryid = rand();

wp_enqueue_script( 'jquery' );

if (isset($attr['orderby'])) {
    $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
    if (!$attr['orderby'])
        unset($attr['orderby']);
}

extract(shortcode_atts(array(
    'order' => 'ASC',
    'orderby' => 'menu_order ID',
    'id' => $post->ID,
    'itemtag' => 'dl',
    'icontag' => 'dt',
    'captiontag' => 'dd',
    'columns' => isset($attr['columns'])?$attr['columns']:4,
    'size' => 'thumbnail',
    'exclude' => ''
), $attr));

if($columns == 1){
	$col = 12 / $columns;
	$col = 'col-md-' . $col . ' col-sm-12 col-xs-12';
}
elseif($columns == 5){
	$col = 'col-5 col-sm-6 col-xs-12';
}else{
	$col = 12 / $columns;
	$col = 'col-md-' . $col . ' col-sm-6 col-xs-12';
}

if($columns == 6)
{
	$clone=4;
}
elseif($columns == 5)
{
	$clone=3;
}
elseif($columns == 4)
{
	$clone=2;
	
}	
elseif($columns == 3)
{
	$clone=1;
	
}
elseif($columns == 2)
{
	$clone=0;
	
}

else
{
$clone='';
}
 
$id = intval($id);
if ('RAND' == $order) $orderby = 'none';

if (!empty($include)) {
    $include = preg_replace('/[^0-9,]+/', '', $include);
    $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

    $attachments = array();
    foreach ($_attachments as $key => $val) {
        $attachments[$val->ID] = $_attachments[$key];
    }
}

if (empty($attachments)) return '';

// Here's your actual output, you may customize it to your need

if($gallery_enable_carousel == true){

$output = "<div class=\"carousel slide padding0\" data-ride=\"carousel\" data-type=\"multi\" data-interval=\"3000\" id=\"{$galleryid}\">\n";
$output .= "<div class=\"carousel-inner\">\n";
}

// Now you loop through each attachment
$i=1;
foreach ($attachments as $id => $attachment) {
    // Fetch all data related to attachment 
    $img = wp_prepare_attachment_for_js($id);
	

	
	// If you want a different size change 'large' to eg. 'medium'
    $url = $img['sizes']['full']['url'];
    $height = $img['sizes']['full']['height'];
    $width = $img['sizes']['full']['width'];
    $alt = $img['alt'];
	$title = $img['title'];
	$desc = $img['description'];
	$link = $img['link'];
	

    // Store the caption
    $caption = $img['caption'];

    if($gallery_enable_carousel == true){
	$output .= "<div class=\"item ".($i==1?'active':'')."\">\n";
	}	
		$output .= "<div class=\"{$col} padding0 gallery-area\">\n";
		
			$output .= "<div class=\"gallery-image border0\">\n";
	
				$output .= "<img src=\"{$url}\" alt=\"{$alt}\" />\n";
				
				if($gallery_enable_overlay == true)
				{
				$output .= "<div class=\"gallery-showcase-overlay\">\n";
					
					$output .= "<div class=\"gallery-showcase-overlay-inner\">\n";
					
						if ($caption) { 
						$output .= "<h3 class=\"gallery-caption\">{$caption}</h3>\n";
						}
						
						$output .= "<div class=\"gallery-showcase-icons\">\n";
							
							$output .= "<a href=\"{$url}\" title=\"{$title}\" data-lightbox=\"image\" class=\"hover_thumb\"><i class=\"fa fa-search\"></i></a>\n";
							
							$output .= "<a href=\"{$link}\" title=\"{$title}\"><i class=\"fa fa-link\"></i></a>\n";
						
						$output .= "</div>\n";
					
					$output .= "</div>\n";
					
				$output .= "</div>\n";
				}

			$output .= "</div>\n";
				
	
		$output .= "</div>\n";
	
	if($gallery_enable_carousel == true){
	$output .= "</div>\n";
	}
	
	
	$i++;
	
}
if($gallery_enable_carousel == true){
$output .= "</div>\n";
$output .= "</div>\n";
			
				$output .= "<div class=\"col-md-12\">\n";
					$output .= "<ul class=\"scroll-btn\">\n";
						$output .= "<li><a class=\"btn-prev\" href=\"#{$galleryid}\" data-slide=\"prev\"></a></li>\n";
						$output .= "<li><a class=\"btn-next\" href=\"#{$galleryid}\" data-slide=\"next\"></a></li>\n";    
					$output .= "</ul>\n";
				$output .= "</div>\n";
			

$output .= "<script>jQuery(function() {
	
		
	
		jQuery('#{$galleryid} .item').each(function(){
				
		  var next = jQuery(this).next();
		  if (!next.length) {
			next = jQuery(this).siblings(':first');
		  }
		  next.children(':first-child').clone().appendTo(jQuery(this));
		 

		 
		  for (var i=0;i<{$clone};i++) {
			next=next.next();
			if (!next.length) {
				next = jQuery(this).siblings(':first');
			}
			
			next.children(':first-child').clone().appendTo(jQuery(this));
		  }
		});		
		
});
</script>";
}

return $output;
}

add_filter( 'widget_text', 'do_shortcode' );

//Remove Gallery Filter
$gallery_filter_enable = get_theme_mod('gallery_filter_enable',false);
if($gallery_filter_enable)
{
remove_filter( 'post_gallery', 'my_post_gallery');	
}

//Hide Title of woocommerce shop page
add_filter( 'woocommerce_show_page_title' , 'woo_hide_page_title' );

function woo_hide_page_title() {
	
	return false;
	
}
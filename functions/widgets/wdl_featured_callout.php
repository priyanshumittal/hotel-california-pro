<?php

add_action( 'widgets_init','graphite_featured_callout'); 
function graphite_featured_callout() 
{ 
	return   register_widget( 'graphite_featured_callout' );
}

class graphite_featured_callout extends WP_Widget {

	function __construct() {
		parent::__construct(
			'graphite_featured_callout', // Base ID
			__('WBR : Call To Action Widget', 'graphite'), // Name
			array( 'description' => __( 'Footer callout widget section', 'graphite' ), ) // Args
		);
	}

	public function widget( $args , $instance ) {
			$instance['title'] = (isset($instance['title'])?$instance['title']:'');
			$instance['btn_label'] = (isset($instance['btn_label'])?$instance['btn_label']:'');
			$instance['btn_link'] = (isset($instance['btn_link'])?$instance['btn_link']:'');
			$instance['btn_open'] = (isset($instance['btn_open'])?$instance['btn_open']:false);
			
		echo $args['before_widget'];
	?>
	<div class="sm-callout animatedParent">
		<?php //echo $args['before_title'] . $instance['title'] . $args['after_title']; ?>
		<?php 
		if($instance['title'] != null)
		{ ?>
		<h4 class="wow bounceInLeft animated animated" ata-wow-delay="0.4s" style="visibility: visible; animation-name: bounceInLeft;"><?php echo $instance['title'];  ?></abbr></h4>
		<?php } if($instance['btn_label'] != null) { ?>
		<div class="sm-callout-btn wow bounceInRight animated" ata-wow-delay="0.4s">
		<a <?php if($instance['btn_link'] != null) { ?> href="<?php echo  $instance['btn_link'];  ?>" <?php } if(  $instance['btn_open']== true ) { echo "target='_blank'"; } ?>><?php echo $instance['btn_label']; ?></a>
		</div>
		<?php } ?>
	</div>
	<div class="sm-seperate"></div>	
		
	<?php
		echo $args['after_widget']; 	
	}

	public function form( $instance ) {
			
			$instance['title'] = (isset($instance['title'])?$instance['title']:'');
			$instance['btn_label'] = (isset($instance['btn_label'])?$instance['btn_label']:'');
			$instance['btn_link'] = (isset($instance['btn_link'])?$instance['btn_link']:'');
			$instance['btn_open'] = (isset($instance['btn_open'])?$instance['btn_open']:false);
		?>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title','graphite' ); ?></label> 
		<textarea rows="5" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" ><?php echo esc_attr( $instance['title'] ); ?></textarea>
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'btn_label' ); ?>"><?php _e( 'Button Text','graphite' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'btn_label' ); ?>" name="<?php echo $this->get_field_name( 'btn_label' ); ?>" type="text" value="<?php echo esc_attr( $instance['btn_label'] ); ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'btn_link' ); ?>"><?php _e( 'Button Link','graphite' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'btn_link' ); ?>" name="<?php echo $this->get_field_name( 'btn_link' ); ?>" type="text" value="<?php echo esc_attr( $instance['btn_link'] ); ?>" />
		</p>
		
		<p>
		<input type="checkbox" class="widefat" id="<?php echo $this->get_field_id( 'btn_open' ); ?>" name="<?php echo $this->get_field_name( 'btn_open' ); ?>" value="1" <?php if($instance['btn_open']==true){ echo 'checked'; } ?> />
		<label for="<?php echo $this->get_field_id( 'btn_open' ); ?>"><?php _e( 'Open link in new tab','graphite' ); ?></label><br/> 
		</p>
		
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? $new_instance['title'] : '';
		
		$instance['btn_label'] = ( ! empty( $new_instance['btn_label'] ) ) ? strip_tags( $new_instance['btn_label'] ) : '';
		
		$instance['btn_link'] = ( ! empty( $new_instance['btn_link'] ) ) ? strip_tags( $new_instance['btn_link'] ) : '';
		
		$instance['btn_open'] = ( ! empty( $new_instance['btn_open'] ) ) ? strip_tags( $new_instance['btn_open'] ) : '';
		
		return $instance;
	}

} // class
?>
<?php 
	if(get_theme_mod('room_section_enable') == false) { 
	
	if( 
		get_theme_mod('home_room_section_title') || 
		
		get_theme_mod('home_room_section_discription')!='' || 
		
		is_active_sidebar( 'graphite_top_sidebar' )
		
		) :
?>
<!-- Rooms Section -->
<section class="wbr-section" id="portfolio">
	<div class="container">
	<?php if(get_theme_mod('home_room_section_title') || get_theme_mod('home_room_section_discription')!='') : ?>
		<!-- Section Title -->
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
					<h1 class="wow fadeInUp animated animated" data-wow-duration="500ms" data-wow-delay="0ms">
					<?php echo get_theme_mod('home_room_section_title'); ?>
					</h1>
					<div class="separator"><span></span></div>
					<p class="wow fadeInDown animated">
					<?php echo get_theme_mod('home_room_section_discription'); ?>
					</p>
				</div>
			</div>
		</div>
		<!-- /Section Title -->
		<?php endif; ?>
		
		<?php if ( is_active_sidebar( 'graphite_top_sidebar' ) ) : ?>
		<div class="row" >
			<?php
				dynamic_sidebar( 'graphite_top_sidebar' );
			?>
		</div>
		<?php endif; ?>
		
	</div>
</section>
<!-- /Rooms Section -->
<div class="clearfix"></div>
<?php 

	endif;
	
} 
?>
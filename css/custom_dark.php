<?php
function custom_dark()
{ 
	$custom_color_enable = get_theme_mod('custom_color_enable',false);
	
	if( $custom_color_enable == true ){
		$link_color = get_theme_mod('link_color','#e32235');
	}
	else
	{
		$link_color = get_theme_mod('theme_color','#e32235');
	}
	
	list($r, $g, $b) = sscanf($link_color, "#%02x%02x%02x");
	
	$r = $r - 50;
	$g = $g - 25;
	$b = $b - 40;
?>
	<style type="text/css"> 
	/* custom light */
	.navbar-default .navbar-nav li.current-menu-item > a,
	.navbar-default .navbar-nav li.current-menu-item > a:hover,
	.navbar-default .navbar-nav li.current-menu-item > a:focus,
	.navbar-default .navbar-nav li.active > a,
	.navbar-default .navbar-nav li.active > a:hover
	{
		background-color:<?php echo $link_color; ?>;
	}
	
	.navbar-default .navbar-nav li > a:hover,
	.navbar-default .navbar-nav li > a:focus,
	.navbar-default .navbar-nav li.menu-item-has-children > a:hover,
	.navbar-default .navbar-nav li.menu-item-has-children > a:focus,
	.navbar-default .navbar-nav li.dropdown:hover > a,
	.navbar-default .navbar-nav li.dropdown:focus > a, 
	.format-quote1 p:before, #footer .widget li a:hover
	{
		color:<?php echo $link_color; ?>;
	}
	
	@media only screen and (min-width: 480px) and (max-width: 767px) {
	.navbar-default .navbar-nav .open .dropdown-menu > li > a:hover { color: <?php echo $link_color; ?>; }
	}
	
	@media only screen and (min-width: 200px) and (max-width: 480px) { 
	.navbar-default .navbar-nav .open .dropdown-menu > li > a:hover { color: <?php echo $link_color; ?>; }
	}
	
	.more, .more:hover, .more:focus {
		color:<?php echo $link_color; ?> !important;
	}
	.navbar-default .navbar-nav >  .current-menu-item > a:hover,
	.navbar-default .navbar-nav >  .current-menu-item > a:focus{ 
		-moz-box-shadow: inset 0 0 5px <?php echo $link_color; ?>;
		-webkit-box-shadow: inset 0 0 5px <?php echo $link_color; ?>;
		box-shadow: inset 0 0 5px <?php echo $link_color; ?>;
	 }

	.dropdown-menu { 
		border-top: 2px solid <?php echo $link_color; ?>;
		border-bottom: 2px solid <?php echo $link_color; ?>;
	}
	
	.comment-date, 
	.testmonial-area span, .about-btn a:hover, 
	/* .page-breadcrumb > li.active a, */
	.author-description:before, 
	.author-description p:before, 	
	.about-section h2 > span, 
	.link-content p,
	.comment-date, 
	.link-content p > a, 
	.site-info p a:hover, 
	.edit-link a, 
	.menu-long-menu-container,
	.about-btn a:hover, 
	ul.post-content li:hover a, 
	ul.post-content > li > a:before, 
	.error_404 h1, .error_404 p > a, 
	a, a:hover, a:focus
	{ 
		color: <?php echo $link_color; ?>;
	}
	
	.format-status-btn-sm,  
	.format-video-btn-sm, 
	.home-room-btn, input[type="reset"] 
	{
		background-color: <?php echo $link_color; ?>;
		color: #ffffff !important;
		box-shadow: 0 3px 0 0 #9a0e1c;
	}
	
	.sm-callout { 
		border-top: 2px solid <?php echo $link_color; ?>;
	}
	
	#footer
	{ 
		border-top: 3px solid <?php echo $link_color; ?>;
		border-bottom: 3px solid <?php echo $link_color; ?>;
	}
	
	.format-status-btn-sm, 
	.format-video-btn-sm,  
	.sm-callout-btn a, 
	.reply a, 
	.cont-btn a, 
	.cont-btn  input[type="submit"]:before, 
	.hc_scrollup, 
	.cont-btn  input[type="submit"]:hover, 
	input[type="submit"]::before, 
	.cont-btn  input[type="submit"]::before, 
	.reply a,  
	.cont-btn a, 
	.sm-callout-btn a, 
	.team-image .team-showcase-icons a:hover, 
	.title-separator span, 
	.scroll-btn li:hover, 
	.slide-btn-area-sm a.slide-btn-sm::before, 
	.home-room-btn a:hover, 
	.cont-btn  input[type="submit"], 
	.more-link, 
	.top-header-detail .section-title span, 
	.footer-sidebar .section-title span,  
	.separator span, 
	.widget #wp-calendar caption, 	
	.widget #wp-calendar tbody a, 
	.widget #wp-calendar tbody a:hover, .widget #wp-calendar tbody a:focus, 
	.widget form.search-form input.search-submit, 
	input[type="submit"], .page-links a, .widget .tagcloud a:hover,
	mark, ins, .page-title-section, .section-separator span, 
	.wbr-typography#testimonial .widget .tagcloud a:hover, 
	.wbr-typography#amenities .widget .tagcloud a:hover { 
		background-color: <?php echo $link_color; ?>;
	}

	.format-status-btn-sm, 	.format-video-btn-sm, 
	.home-room-btn a, .sm-callout-btn a, 
	.reply a, .cont-btn a, 
	.cont-btn input[type="submit"],  
	.sm-callout-btn a, 
	.cont-btn  input[type="submit"]:hover, 
	.cont-btn input[type="submit"], 
	a.more-link, input[type="submit"],
	.page-links a, .widget form.search-form input.search-submit
	{  
		box-shadow: 0 3px 0 0 rgb(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b;?>);
	}
	
	/* 404 */
	.error_404 h1, 
	.error_404 p > a,
	.portfolio-content-area:hover .portfolio-content h3 > a, 
	.portfolio-content-area:hover h3 > a, 
	.post-content-area:hover h3.entry-title a, 
	.widget li a::before, 
	.widget li a:hover, 	
	.widget #wp-calendar a:hover, 
	.widget #wp-calendar #next a:hover, 
	.widget #wp-calendar #prev a:hover, 
	.comment-form-section .blog-post-info-detail a, 
	.comment-form-section .blog-post-info-detail a:hover, 
	.comment-form-section .blog-post-info-detail a:focus,  
	.blog-section .post-content-area .entry-content a[target=_blank], 
	.blog-section .post-content-area .page-links a, 
	.blog-section .post-content-area .entry-meta a:hover, 
	.blog-section .post-content-area:hover .entry-header h2.entry-title > a, 
	.blog-section .post-content-area .entry-content a, h1.site-title, .portfolio-icon i	
	{ 
		color: <?php echo $link_color; ?>;
	}
	
	.sidebar .section-title,  blockquote
	{
		border-left: 5px solid <?php echo $link_color; ?>;
	}
	
	.widget .tagcloud a:hover, 
	.wbr-typography#testimonial .widget .tagcloud a:hover, 
	.wbr-typography#amenities .widget .tagcloud a:hover 
	{
		border: 1px solid <?php echo $link_color; ?>;
	}
	
	/* Woocommerce Colors-------------------------------------------------------------------------------------------- */

	.woocommerce .star-rating span, .woocommerce-error:before, .woocommerce-info:before, .woocommerce-message:before, 
	.woocommerce #review_form #respond p label .required { color: <?php echo $link_color; ?> !important; }
	.woocommerce-error, .woocommerce-info, .woocommerce-message { border-top-color: <?php echo $link_color; ?> !important; }
	.sidebar-widget .widget-title { border-bottom: 2px solid <?php echo $link_color; ?> !important; } 
	.woocommerce ul.products li.product:hover .onsale { border: 2px solid <?php echo $link_color; ?>; }
	.woocommerce ul.products li.product:hover .onsale, 
	.woocommerce ul.products li.product:hover .button, 
	.woocommerce ul.products li.product:focus .button, 
	.woocommerce div.product form.cart .button:hover, 
	.woocommerce div.product form.cart .button:focus, 
	.woocommerce div.product form.cart .button, 
	.woocommerce a.button, 
	.woocommerce a.button:hover, 
	.woocommerce .cart input.button, 
	.woocommerce input.button.alt, 
	.woocommerce input.button, 
	.woocommerce button.button, 
	.woocommerce #respond input#submit, 
	.woocommerce .cart input.button:hover, 
	.woocommerce .cart input.button:focus, 
	.woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover, 
	.woocommerce-cart .wc-proceed-to-checkout a.checkout-button:focus, 
	.woocommerce input.button.alt:hover, 
	.woocommerce input.button.alt:focus, 
	.woocommerce input.button:hover, 
	.woocommerce input.button:focus, 
	.woocommerce button.button, 
	.woocommerce button.button:hover, 
	.woocommerce button.button:focus, 
	.woocommerce #respond input#submit:hover, 
	.woocommerce #respond input#submit:focus,  
	.ui-slider-horizontal .ui-slider-range, 
	.woocommerce-product-search input[type="submit"], 
	.woocommerce .woocommerce-message a.button, 
	.woocommerce .woocommerce-message a.button:hover,  
	.woocommerce button.button, 
	.sidebar-widget .woocommerce a.button, 
	.woocommerce #respond input#submit, 
	.widget_shopping_cart_content .button .button, 
	 .woocommerce a.button.alt, .owl-item .item .cart .add_to_cart_button:hover {
		background: <?php echo $link_color; ?> !important;
	}
	
	</style>
<?php 
} 
<?php 
/**
 * Template Name: Contact Page
 */
get_header(); 
graphite_breadcrumbs(); ?>

<!-- Contact Section -->
<section class="cont-section">
	<div class="container">
	
		<?php if( has_post_thumbnail() ): ?>
		<div class="row">
			<div class="col-md-12">
				<div class="post-featured-area">
					<div class="blog-featured-img">
					<?php $arg = array( 'class'=>'img-responsive' ); the_post_thumbnail( '',$arg ); ?>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<div class="row">	
			<!--Contact Form Section-->
			<?php if( get_theme_mod('contact_form_enable',true) == true ): ?>
			<div class="col-md-<?php echo ( is_active_sidebar( 'graphite_contact_page_sidebar' ) ? '8' :'12' ); ?> col-xs-12">
					<?php 
					// Start the Loop.
					while ( have_posts() ) : the_post();
						// Include the page
						get_template_part( 'content','contact' );
						
						comments_template( '', true ); // show comments 
						
					endwhile;
					?>	
			</div>
			<?php endif; ?>
			<!--/Contact Form Section-->
			
			<!--Contact Info-->
			<?php if( get_theme_mod('contact_info_enable',true) == true ):?>
			<div class="col-md-<?php echo ( is_active_sidebar( 'graphite_contact_page_sidebar' ) ? '4' :'12' ); ?> col-xs-12">
					
				<?php 
				if( is_active_sidebar('graphite_contact_page_sidebar') ) :
					echo '<div class="sidebar">';
					dynamic_sidebar( 'graphite_contact_page_sidebar' ); 
					echo '</div>';
				endif;
				?>
				
			</div>
			<?php endif; ?>
			<!--Contact Info-->	                     
		</div>
		
		<!--Google Map-->
		<?php if( get_theme_mod('google_map_enable',true) == true ):?>
		<div class="row">
			<div class="col-md-12 col-xs-12 map-section wow flipInX animated animated" data-wow-delay=".5s">
				<?php if(get_theme_mod('google_map_title',__('View us on the map','graphite'))!='') { ?>
				<div class="sidebar">
					<div class="section-title wow fadeInDown animated" style="margin: 0;" data-wow-delay="0.4s">
						<h3 class="widget-title"><?php echo get_theme_mod('google_map_title'); ?></h3>
					</div>
				</div>
				<?php }
				if( get_theme_mod('contact_google_map_shortcode'))
				{
				?>

				<div class="cont-google-map">			
					<?php
					if( get_theme_mod('contact_google_map_shortcode') !='' ) {
					echo  do_shortcode(get_theme_mod('contact_google_map_shortcode')); 
					}
					?>	
					
				</div>
				<?php } ?>
			</div>
		</div>
		<?php endif; ?>
		<!--/Google Map-->
		
	</div>
</section>
<!-- /Contact Section -->

<?php get_footer(); ?>
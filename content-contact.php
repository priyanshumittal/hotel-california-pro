<?php
/**
 * The default template for displaying content
 */
?>

<?php if(get_theme_mod('contact_form_title')!=null): ?>
<div class="sidebar">
	<div class="section-title wow fadeInDown animated" style="margin: 0;" data-wow-delay="0.4s">
		<h3 class="widget-title"><?php echo get_theme_mod('contact_form_title',__('Get in touch with us','graphite')); ?></h3>
	</div>
</div>
<?php endif; 
if ( $post->post_content!=="" ) {

?>
<div class="cont-form-section wow fadeInDown animated" data-wow-delay="0.4s">
			
	<div class="cont-description">
		<?php the_content( __('Read More','graphite') ); ?>
	</div>
	
</div>
<?php } ?>

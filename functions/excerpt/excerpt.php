<?php
// new and event
function graphite_get_news_event_excerpt()
{
		global $post;
		$excerpt = get_the_content();
		$excerpt = strip_tags(preg_replace(" (\[.*?\])",'',$excerpt));
		$excerpt = strip_shortcodes($excerpt);
		$original_len = strlen($excerpt);
		$excerpt = substr($excerpt, 0,80);
		$len=strlen($excerpt);
		if($original_len>25) {
		$excerpt = $excerpt;
		return '<p>'.$excerpt . '...</p><div class="about-btn"><a href="' . get_permalink() . '">'.__('Read More','graphite').'</a></div>';
		}
		else
		{ 
			return '<p>'.$excerpt.'</p>'; 
		}
}

// blog page excerpt
function get_slider_excerpt()
{
		global $post;
		
		$more_enable = sanitize_text_field( get_post_meta( get_the_ID(), 'more_enable', true ));
		$btn_text = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_text', true ));
		$btn_link = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_link', true ));
		$btn_target = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_target', true ));
		
		
		$excerpt = get_the_excerpt();
		$excerpt = strip_tags(preg_replace(" (\[.*?\])",'',$excerpt));
		$excerpt = strip_shortcodes($excerpt);
			
			if($more_enable==true){

						return '<p>'.$excerpt . '</p></div><div class="slide-btn-area-sm"><a href="' . $btn_link . '" class="slide-btn-sm" '.($btn_target==true?'target="_blank"':'').'><span>'.$btn_text.'</span></a></div>';
						
					}
					else{
						return '<p>'.$excerpt . '</p></div><div class="slide-btn-area-sm"></div>';
					}
}


function get_slider_quote_excerpt()
{
		global $post;
		$excerpt = get_the_excerpt();
		$excerpt = strip_tags(preg_replace(" (\[.*?\])",'',$excerpt));
		$excerpt = strip_shortcodes($excerpt);
		$original_len = strlen($excerpt);
		$get_excerpt_length = get_theme_mod('slider_excerpt_lenght',250);
		$get_excerpt_length = ($get_excerpt_length!=null?$get_excerpt_length:250);
		$excerpt = substr($excerpt, 0, $get_excerpt_length);
		$len=strlen($excerpt);
		if($original_len>$get_excerpt_length) {
		$excerpt = $excerpt;
		return '<p>'.$excerpt . '</p>';
		}
		else
		{ 
			return '<p>'.$excerpt.'</p>'; 
		}
		
}


// slider vedio excerpt
function get_slider_video_excerpt()
{
		
		global $post;
		
		$more_enable = sanitize_text_field( get_post_meta( get_the_ID(), 'more_enable', true ));
		$btn_text = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_text', true ));
		$btn_link = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_link', true ));
		$btn_target = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_target', true ));
		
		
		$excerpt = get_the_excerpt();
		$excerpt = strip_tags(preg_replace(" (\[.*?\])",'',$excerpt));
		$excerpt = strip_shortcodes($excerpt);
			
					if($more_enable==true){

					return '<p>'.$excerpt . '</p><a href="' . $btn_link . '" class="format-video-btn-sm" '.($btn_target==true?'target="_blank"':'').'>'.$btn_text.'</a>';
						
					}
					else
					{
						return '<p>'.$excerpt . '</p>';
					}
}
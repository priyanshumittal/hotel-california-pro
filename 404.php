<?php 
/**
 * The main template file
 */
get_header();
graphite_breadcrumbs(); ?>
<!-- Blog & Sidebar Section -->
<!-- 404 Error Section -->
<section class="404-section">		
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="error_404 wow flipInX animated" data-wow-delay=".5s">
					<h4> <?php _e('Oops! Page not found','graphite'); ?></h4>
					<h1><?php echo '4'; ?><i class="fa fa-frown-o"></i><?php echo '4'; ?> </h1>
					<p><?php _e ('We are sorry, but the page you are looking for does not exist. ','graphite'); ?> <a href="<?php echo esc_html(site_url());?>"><?php _e('Go back','graphite'); ?></a></p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /404 Error Section -->
<!-- /Blog & Sidebar Section -->

<?php get_footer(); ?>
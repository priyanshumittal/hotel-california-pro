<?php

// Webriti scripts
if( !function_exists('graphite_scripts_function'))
{
	function graphite_scripts_function(){
		// css
		wp_enqueue_style('style', get_stylesheet_uri() );
		wp_enqueue_style('bootstrap', WEBRITI_TEMPLATE_DIR_URI . '/css/bootstrap.css');
		$class = get_theme_mod('theme_style_type','default.css');
		wp_enqueue_style('default', WEBRITI_TEMPLATE_DIR_URI . '/css/'.$class);
		wp_enqueue_style('graphite_theme-menu_css', WEBRITI_TEMPLATE_DIR_URI . '/css/theme-menu.css');
		$remove_wow_animation = get_theme_mod('remove_wow_animation',false);
		if($remove_wow_animation !=true)
		{
		wp_enqueue_style('graphite_animate.min_css', WEBRITI_TEMPLATE_DIR_URI . '/css/animate.min.css');
		}
		wp_enqueue_style('graphite_switcher_css', WEBRITI_TEMPLATE_DIR_URI . '/css/switcher/switcher.css');
		wp_enqueue_style('graphite_light_css', WEBRITI_TEMPLATE_DIR_URI . '/css/lightbox.css');
		wp_enqueue_style('graphite_font-awesome.min_css', WEBRITI_TEMPLATE_DIR_URI . '/css/font-awesome/css/font-awesome.min.css');
		wp_enqueue_style('graphite_media-responsive_css', WEBRITI_TEMPLATE_DIR_URI . '/css/media-responsive.css');
		wp_enqueue_style('graphite_woocommerce_css', WEBRITI_TEMPLATE_DIR_URI . '/css/woo-commerce.css');
		wp_enqueue_style('graphite_element_css', WEBRITI_TEMPLATE_DIR_URI . '/css/element.css');
		
		wp_enqueue_script( 'jquery' );
		
		// Menu & page scroll js
		wp_enqueue_script('graphite_menu_js', WEBRITI_TEMPLATE_DIR_URI . '/js/menu/menu.js');
		wp_enqueue_script('graphite_page-scroll_js', WEBRITI_TEMPLATE_DIR_URI . '/js/page-scroll.js');
		wp_enqueue_script('graphite_light_js', WEBRITI_TEMPLATE_DIR_URI . '/js/lightbox/lightbox.js');
		
		require_once('custom_style.php');
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
		}
	}
}
add_action('wp_enqueue_scripts','graphite_scripts_function');

// webriti footer enquque scripts
if(!function_exists('graphite_footer_enquque_scripts'))
{
	function graphite_footer_enquque_scripts()
	{
		wp_enqueue_script('graphite_bootstrap_js', WEBRITI_TEMPLATE_DIR_URI . '/js/bootstrap.min.js');
		$remove_wow_animation = get_theme_mod('remove_wow_animation',false);
		if($remove_wow_animation !=true)
		{
		wp_enqueue_script('graphite_animate_js', WEBRITI_TEMPLATE_DIR_URI . '/js/animation/animate.js');
		wp_enqueue_script('graphite_wow_js', WEBRITI_TEMPLATE_DIR_URI . '/js/animation/wow.min.js');
		}
	}
}
add_action('wp_footer','graphite_footer_enquque_scripts');

// custom js enquque scripts
if(!function_exists('graphite_head_scripts'))
{
	function graphite_head_scripts()
	{ ?>
	<script>
		function changeCSS(cssFile, cssLinkIndex) {
			var link_index = document.getElementById("webriti_theme_default_css-css");
			link_index.setAttribute("href", "<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/css/"+cssFile);
		}
	</script>
	<?php }
}
add_action('wp_head','graphite_head_scripts');

function graphite_enqueue_scripts(){
	wp_enqueue_style('drag-drop-css', WEBRITI_TEMPLATE_DIR_URI . '/css/drag-drop.css');
}
add_action( 'admin_enqueue_scripts', 'graphite_enqueue_scripts' );

// custom css
function graphite_custom_css()
{
?>
	<style>
	<?php echo get_theme_mod('graphite_custom_css',''); ?>
	</style>
<?php }
add_action('wp_head','graphite_custom_css');

// footer custom script
function graphite_footer_custom_script()
{
	if(get_theme_mod('theme_style_type','default.css')=='dark.css')
	{
	   custom_dark();
	}
	else
	{
	  custom_light();
	}
}
add_action('wp_head','graphite_footer_custom_script');

function graphite_cta_color()
{
$cta_color = get_theme_mod('cta_color','#21202e');
?>
	<style type="text/css">
	.cta-section {
	background:<?php echo $cta_color; ?>;
	}
	</style>
<?php
}
add_action('wp_head','graphite_cta_color');

// media upload
function graphite_upload_scripts()
 {
	// And let's not forget the script we wrote earlier
	 wp_enqueue_media();	
	 wp_enqueue_script('graphite-media-upload');
     wp_enqueue_script('graphite-thickbox');
     wp_enqueue_script('upload_media_widget', WEBRITI_TEMPLATE_DIR_URI . '/js/upload-media.js', array('jquery'));
	 wp_enqueue_style('thickbox');
}
add_action("admin_enqueue_scripts","graphite_upload_scripts");

function graphite_customize_preview_js() {
	wp_enqueue_script( 'graphite_customizer_js', WEBRITI_TEMPLATE_DIR_URI . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'graphite_customize_preview_js' );

add_action('wp_head', 'graphite_mobile_custom_styles', 100);

function graphite_mobile_custom_styles()
{

$slider_slide_height = get_theme_mod('slider_slide_height',200);
if($slider_slide_height)
{ ?>
<style>
@media only screen and (max-width: 480px) and (min-width: 200px)
{
.homepage-mycarousel .carousel-inner > .item > img {
    height: <?php echo $slider_slide_height; ?>px !important;
}
}
</style>
<?php
} } ?>
<!-- Additional Section -->
<?php 
if( get_theme_mod('additional_enable') == false ) {
	
	if( 
		
		get_theme_mod('additional_title') || 
		
		get_theme_mod('additional_description')!='' ||
		
		is_active_sidebar( 'graphite_additional_sidebar' )
		
		):
		
?>
<section class="wbr-section" id="additional">
	<div class="container">
	
		<?php if( get_theme_mod('additional_title') != '' || get_theme_mod('additional_description') != '' ) { ?>
		<!-- Section Title -->
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
				
					<?php if( get_theme_mod('additional_title') != '' ): ?>
					<h1 class="wow fadeInUp animated animated" data-wow-duration="500ms" data-wow-delay="0ms"><?php echo get_theme_mod('additional_title');?></h1>
					<?php endif; ?>
					
					<?php if( get_theme_mod('additional_title') != '' && get_theme_mod('additional_description') != '' ): ?>
					<div class="separator"><span></span></div>
					<?php endif; ?>
					
					<?php if( get_theme_mod('additional_description') != '' ): ?>
					<p class="wow fadeInDown animated"><?php echo get_theme_mod('additional_description'); ?></p>
					<?php endif; ?>
					
				</div>
			</div>
		</div>
		<!-- /Section Title -->
		<?php } ?>
		
		<?php	if ( is_active_sidebar( 'graphite_additional_sidebar' ) ) : ?>
		<div class="row">
			<?php dynamic_sidebar( 'graphite_additional_sidebar' ); ?>
		</div>
		<?php endif; ?>
		
	</div>
</section>
<!-- /Additional Section -->
<?php 
	endif;

} 
?>
<?php
/**
 * Feature Page Widget
 *
 */
add_action('widgets_init','graphite_feature_page_widget');

function graphite_feature_page_widget(){
	
	return register_widget('graphite_feature_page_widget');
}


class graphite_feature_page_widget extends WP_Widget{
	
	function __construct() {
		
		parent::__construct(
		
			'graphite_feature_page_widget', // Base ID
			
			__('WBR : Page / Service Widget', 'graphite'), // Name
			
			array( 'description' => __( 'Feature page item widget ', 'graphite'), ) // Args
			
		);
	}
	
	
	public function widget( $args , $instance ) {
		
		$instance['selected_page'] = (isset($instance['selected_page'])?$instance['selected_page']:'');
		
		$instance['hide_image'] = (isset($instance['hide_image'])?$instance['hide_image']:'');
		
		$instance['below_title'] = (isset($instance['below_title'])?$instance['below_title']:'');
		
		$instance['buttontext'] = (isset($instance['buttontext'])?$instance['buttontext']:'');
		
		$instance['buttonlink'] = (isset($instance['buttonlink'])?$instance['buttonlink']:'');
		
		$instance['background'] = (isset($instance['background'])?$instance['background']:'');
		
		$instance['icon'] = (isset($instance['icon'])?$instance['icon']:'');
		
		$instance['target'] = (isset($instance['target'])?$instance['target']:'');
		
		$selected_page_class=(isset($instance['selected_page_class'])?$instance['selected_page_class']:'');
		
		if($selected_page_class !='')
		{
			$args['before_widget'] = str_replace('class="', 'class="'. $selected_page_class . ' ',$args['before_widget']);
				
		}
		
		// fetch page object
		
		if( $instance['selected_page'] != '' ){
				
			$page  = get_post( $instance['selected_page'] ); 
			
			$title = $page->post_title;
		
		echo $args['before_widget'];
			
			echo '<div class="post text-center wow flipInX animated" data-wow-delay=".5s">';
			
			
			
				
				if( $title != '' && $instance['below_title'] == true ):
				
					echo '<div class="entry-header">';
						
						
					
						echo '<h3 class="entry-title">';
							
							if( ( $instance['buttonlink'] ) != null ):
							
							echo '<a href="'.$instance['buttonlink'].'" '.($instance['target']==true?'target="_blank"':'').' title="'.$title.'">';
							
							endif;
							
								echo $title;
								
							if( ( $instance['buttonlink'] ) != null ):
							
							echo '</a>';
							
							endif;
							
						echo '</h3>';
						
					echo '</div>';
					
				endif;
			
			
			
			
			
				
					
					
						
						if( $instance['icon'] != '' ):
						
							echo '<figure class="post-thumbnail">';
							
							if( ( $instance['buttonlink'] ) != null ):
					
								echo '<a href="'.$instance['buttonlink'].'" '.($instance['target']==true?'target="_blank"':'').' title="'.$title.'">';
						
					        endif; 
							
							echo '<i class="fa '.$instance['icon'].'" style="color:'.$instance['background'].';"></i>';
							
							if( ( $instance['buttonlink'] ) != null ):
					
								echo '</a>';
						
					        endif;	
							
							echo '</figure>';
						
						endif;
					
						

						if( $instance['hide_image'] != true ):
						
						if(get_the_post_thumbnail($page->ID) != null){
						
						echo '<figure class="post-thumbnail">';
						
						    if( ( $instance['buttonlink'] ) != null ):
					
								echo '<a href="'.$instance['buttonlink'].'" '.($instance['target']==true?'target="_blank"':'').' title="'.$title.'">';
						
					        endif; 
						    	
						
							$attr= array('class' => 'img-responsive');
							echo get_the_post_thumbnail($page->ID, 'large', $attr);
							
							if( ( $instance['buttonlink'] ) != null ):
					
								echo '</a>';
						
					        endif;	
						
						echo '</figure>';
						
						}
						
						endif;
					
					
				if( $title != '' && $instance['below_title']!=true ):
				
					echo '<div class="entry-header">'; 
						
					
					echo '<h3 class="entry-title">';
							
							if( ( $instance['buttonlink'] ) != null ):
							
							echo '<a href="'.$instance['buttonlink'].'" '.($instance['target']==true?'target="_blank"':'').' title="'.$title.'">';
							
							endif;
							
								echo $title;
								
							if( ( $instance['buttonlink'] ) != null ):
							
							echo '</a>';
							
							endif;
							
						echo '</h3>';
					echo '</div>';
					echo '<div class="entry-content">';
					
						
						
						if( $page->post_content ) :

						 echo '<p>'.$page->post_content.'</p>';

					endif; 
					
					if( ( $instance['buttonlink'] )!= null )echo '<p><a class="more" href="'.$instance['buttonlink'].'" '.($instance['target']==true?'target="_blank"':'').' >'.$instance['buttontext'].'</a></p>';
						
					echo '</div>';	
						
				endif;
					
				if( $title != '' && $instance['below_title'] == true ):
				
				echo '<div class="entry-content">';
				
					if( $page->post_content ) :

						 echo '<p>'.$page->post_content.'</p>';

					endif; 
					
					if( ( $instance['buttonlink'] )!= null )echo '<p><a class="more" href="'.$instance['buttonlink'].'" '.($instance['target']==true?'target="_blank"':'').' >'.$instance['buttontext'].'</a></p>';
					
					echo '</div>';	
					
				endif;	
				
			
				
				
				
			echo '</div>';
				
		echo $args['after_widget']; 	
		
		}
	}
	
	
	
	
	public function form( $instance ) {
		
		$instance['selected_page'] = isset($instance['selected_page']) ? $instance['selected_page'] : '';
		
		$instance['hide_image'] = isset($instance['hide_image']) ? $instance['hide_image'] : '';
		
		$instance['below_title'] = isset($instance['below_title']) ? $instance['below_title'] : '';
		
		$instance['buttontext'] = isset($instance['buttontext']) ? $instance['buttontext'] : '';
		
		$instance['buttonlink'] = isset($instance['buttonlink']) ? $instance['buttonlink'] : '';
		
		$instance['icon'] = isset($instance['icon']) ? $instance['icon'] : '';
		
		$instance['target'] = isset($instance['target']) ? $instance['target'] : '';
		
		$instance['background'] = isset($instance['background']) ? $instance['background'] : '';
		
		$instance['selected_page_class'] = (isset($instance['selected_page_class'])? $instance['selected_page_class']: '');
		
		?>
		<script type="text/javascript">
			//<![CDATA[
				jQuery(document).ready(function()
				{
					// colorpicker field
					jQuery('.cw-color-picker').each(function(){
						var $this = jQuery(this),
							id = $this.attr('rel');

						$this.farbtastic('#' + id);
					});
				});
			//]]>   
		  </script>	
		
		
	    <p>
		
		<label for="<?php echo $this->get_field_id( 'icon' ); ?>"><?php _e( 'Specify font awesome icon class ( like: fa-cloud ). <a href="//fontawesome.io/icons/" target="blank" >Browse all icons </a>','graphite' ); ?></label> 
		
		<input class="widefat" id="<?php echo $this->get_field_id( 'icon' ); ?>" name="<?php echo $this->get_field_name( 'icon' ); ?>" type="text" value="<?php echo esc_attr( $instance['icon'] ); ?>" />
		
		</p>
		
		<p>
	  <label for="<?php echo $this->get_field_id('background'); ?>"><?php _e('Icon Color','graphite'); ?></label> 
	  <input class="widefat" id="<?php echo $this->get_field_id('background'); ?>" name="<?php echo $this->get_field_name('background'); ?>" type="text" value="<?php if($instance['background']) { echo $instance['background']; } else { echo '#21202e'; } ?>" />
		<div class="cw-color-picker" rel="<?php echo $this->get_field_id('background'); ?>"></div>
	</p>

		
		
		<p>
			<label for="<?php echo $this->get_field_id( 'selected_page' ); ?>"><?php _e( 'Select pages','graphite' ); ?></label> 
			
			<select class="widefat" id="<?php echo $this->get_field_id( 'selected_page' ); ?>" name="<?php echo $this->get_field_name( 'selected_page' ); ?>">
				
				<option value>--<?php _e('Select','graphite');?>--</option>
				
				<?php
				
					$selected_page = $instance['selected_page'];
					
					$pages = get_pages($selected_page); 
					
					foreach ( $pages as $page ) {
						
						$option = '<option value="' . $page->ID . '" ';
						
						$option .= ( $page->ID == $selected_page ) ? 'selected="selected"' : '';
						
						$option .= '>';
						
						$option .= $page->post_title;
						
						$option .= '</option>';
						
						echo $option;
						
					}
					
				?>
						
			</select>
			
		</p>
		
		
		
		<p>
		
		<input class="checkbox" type="checkbox" <?php if($instance['hide_image']==true){ echo 'checked'; } ?> id="<?php echo $this->get_field_id( 'hide_image' ); ?>" name="<?php echo $this->get_field_name( 'hide_image' ); ?>" /> 
		
		<label for="<?php echo $this->get_field_id( 'hide_image' ); ?>"><?php _e( 'Hide featured image','graphite' ); ?></label>
		
		</p>
		
		
		
		<p>
		
		<input class="checkbox" type="checkbox" <?php if($instance['below_title']==true){ echo 'checked'; } ?> id="<?php echo $this->get_field_id( 'below_title' ); ?>" name="<?php echo $this->get_field_name( 'below_title' ); ?>" /> 
		
		<label for="<?php echo $this->get_field_id( 'below_title' ); ?>"><?php _e( 'Display image / icon below title','graphite' ); ?></label>
		
		</p>
		
		
		
		<p>
		
		<label for="<?php echo $this->get_field_id( 'buttontext' ); ?>"><?php _e( 'Button Text','graphite' ); ?></label> 
		
		<input class="widefat" id="<?php echo $this->get_field_id( 'buttontext' ); ?>" name="<?php echo $this->get_field_name( 'buttontext' ); ?>" type="text" value="<?php echo esc_attr( $instance['buttontext'] ); ?>" />
		
		</p>
		
		
		
		<p>
		
		<label for="<?php echo $this->get_field_id( 'buttonlink' ); ?>"><?php _e( 'Button Link','graphite' ); ?></label> 
		
		<input class="widefat" id="<?php echo $this->get_field_id( 'buttonlink' ); ?>" name="<?php echo $this->get_field_name( 'buttonlink' ); ?>" type="text" value="<?php echo esc_attr( $instance['buttonlink'] ); ?>" />
		
		</p>
		
		
		
		<p>
		
		<input class="checkbox" type="checkbox" <?php if($instance['target']==true){ echo 'checked'; } ?> id="<?php echo $this->get_field_id( 'target' ); ?>" name="<?php echo $this->get_field_name( 'target' ); ?>" /> 
		
		<label for="<?php echo $this->get_field_id( 'target' ); ?>"><?php _e( 'Open link in new tab','graphite' ); ?></label>
		
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'selected_page_class' ); ?>"><?php _e('CSS Classes (optional)','graphite' ); ?></label> 
		</p>
		<input class="widefat" id="<?php echo $this->get_field_id( 'selected_page_class' ); ?>" name="<?php echo $this->get_field_name( 'selected_page_class' ); ?>" type="text" value="<?php if($instance[ 'selected_page_class' ]) echo esc_attr($instance[ 'selected_page_class' ]);?>" />
		
		
		<?php 
	}
	
	
	
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		
		$instance['selected_page'] = ( ! empty( $new_instance['selected_page'] ) ) ? $new_instance['selected_page'] : '';
		
		$instance['hide_image'] = ( ! empty( $new_instance['hide_image'] ) ) ? $new_instance['hide_image'] : '';
		
		$instance['below_title'] = ( ! empty( $new_instance['below_title'] ) ) ? $new_instance['below_title'] : '';
		
		$instance['buttontext'] = ( ! empty( $new_instance['buttontext'] ) ) ? $new_instance['buttontext'] : '';
		
		$instance['buttonlink'] = ( ! empty( $new_instance['buttonlink'] ) ) ? $new_instance['buttonlink'] : '';
		
		$instance['icon'] = ( ! empty( $new_instance['icon'] ) ) ? $new_instance['icon'] : '';
		
		$instance['target'] = ( ! empty( $new_instance['target'] ) ) ? $new_instance['target'] : '';
		
		$instance['background'] = strip_tags($new_instance['background']);
		
		$instance['selected_page_class'] = ( ! empty( $new_instance['selected_page_class'] ) ) ? $new_instance['selected_page_class'] : '';
		
		
		
		return $instance;
	}
	
}

function sample_load_color_picker_script() {
	wp_enqueue_script('farbtastic');
}
function sample_load_color_picker_style() {
	wp_enqueue_style('farbtastic');	
}
add_action('admin_print_scripts-widgets.php', 'sample_load_color_picker_script');
add_action('admin_print_styles-widgets.php', 'sample_load_color_picker_style');
?>
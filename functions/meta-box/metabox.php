<?php
//code for the meta data...

add_action('admin_init','my_meta_init');

function my_meta_init()
{
	foreach ( array( 'post' , 'page' ) as $type) 
	{
		add_meta_box('my_all_meta', 'my_meta_banner', $type, 'normal', 'high');
	}

	add_meta_box('my_all_meta2', __('Customize Read More Button in slider','graphite'), 'my_meta_slider','post', 'normal', 'high');
	add_action('save_post','my_meta_save');
	
	add_meta_box('slider_page',__('Show slider on this page','graphite'),'graphite_slider_page', 'page','normal', 'high');
	add_meta_box('slider_post',__('Show slider on this post','graphite'),'graphite_slider_post', 'post', 'normal', 'high');
}

function graphite_slider_page()
	{
		global $post ;
		$slider_enable_page = sanitize_text_field( get_post_meta( get_the_ID(), 'slider_enable_page', true ));
		?>
		<p> <strong><?php _e('The settings mentioned Slider on all pages and post.','graphite');?></strong>
		<p><input type="checkbox" name="slider_enable_page" id="slider_enable_page" <?php if($slider_enable_page){echo "checked";}?> />
		<?php _e('Show slider on page and post','graphite'); ?></p>
		<?php
	}
	
	function graphite_slider_post()
	{
		global $post ;
		$slider_enable_post = sanitize_text_field( get_post_meta( get_the_ID(), 'slider_enable_post', true ));
		?>
		<p> <strong><?php _e('The settings mentioned Slider on all pages and post.','graphite');?></strong>
		<p><input type="checkbox" name="slider_enable_post" id="slider_enable_post" <?php if($slider_enable_post){echo "checked";}?> />
		<?php _e('Show slider on page and post','graphite'); ?></p>
		<?php
	}


function my_meta_slider(){
	
	global $post;
	
	$more_enable = sanitize_text_field( get_post_meta( get_the_ID(), 'more_enable', true ));
	$btn_text = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_text', true ));
	$btn_link = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_link', true ));
	$btn_target = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_target', true )); 
	
	$btn_target = ( $btn_target ? true : 0 );
	?>
	<p><input type="checkbox" style="padding: 10px;" name="more_enable" value="1" <?php if( isset( $more_enable ) && $more_enable == true ) echo 'checked'; ?> />
	<label><?php _e('Show Read More Button','graphite');?></label></p>
	
	
	<p>
		<h4 class="heading"><?php _e('Button Text','graphite'); ?></h4>
		<input type="text" name="btn_text" id="btn_text" value="<?php if($btn_text){ echo $btn_text; } ?>">
	</p>
	
	<p>
		<h4 class="heading"><?php _e('Button Link','graphite'); ?></h4>
		<input type="text" name="btn_link" id="btn_link" value="<?php if($btn_link){ echo $btn_link; } ?>">
	</p>
	
	<p>
		<h4 class="heading"><?php _e('Open link in new tab','graphite'); ?></h4>
		<input type="checkbox" name="btn_target" id="btn_target" value="1" <?php if($btn_target) echo 'checked'; ?>>
	</p>
	<?php
}
			
// code for banner description
function my_meta_banner()
{
	global $post;
	$meta = get_post_meta($post->ID,'_my_meta',TRUE); 
	?>	
	
<?php
}
//end of banne description
function my_meta_save($post_id) 
{
	if((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']))
        return;
	
	if ( ! current_user_can( 'edit_page', $post_id ) )
	{     return ;	} 

	if(isset($_POST['post_ID']))
	{
		$post_ID   = $_POST['post_ID'];				
		$post_type = get_post_type($post_ID);
		
		if($post_type=='post'){
			$more_enable = isset($_POST['more_enable']) && $_POST['more_enable'] ? 1 : 0 ;
			update_post_meta($post_ID, 'more_enable', sanitize_text_field($more_enable));
			update_post_meta($post_ID, 'btn_target', sanitize_text_field( $_POST['btn_target'] ));
			update_post_meta($post_ID, 'btn_text', sanitize_text_field($_POST['btn_text']));
			update_post_meta($post_ID, 'btn_link', sanitize_text_field($_POST['btn_link']));
			update_post_meta($post_ID,'_my_meta',$_POST['_my_meta']);
			
			//For Slider on post
			$slider_post = isset($_POST['slider_enable_post']) && $_POST['slider_enable_post'] ? 1 : 0 ;
			update_post_meta($post_ID, 'slider_enable_post', $slider_post);
			
		}
		
		//For slider on page
		elseif($post_type=='page'){
			
			$slider_page = isset($_POST['slider_enable_page']) && $_POST['slider_enable_page'] ? 1 : 0 ;
			update_post_meta($post_ID, 'slider_enable_page', $slider_page);

		}
		
		else{
			update_post_meta($post_ID,'_my_meta',$_POST['_my_meta']);
		}
		
	}
}
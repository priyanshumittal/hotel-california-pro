<?php 
	if( 
		is_active_sidebar('graphite_bottom_left_sidebar') || 
		
		is_active_sidebar('graphite_bottom_center_sidebar') ||
		
		is_active_sidebar('graphite_bottom_right_sidebar')
	) : 
 ?>
<!-- Blog, About and Googlemap Section -->
<section class="wbr-section" id="footer-widget">
	<div class="container">
		<div class="row">
			 
			<!--Homepage News & Blog Area-->
				
				<?php
				$latest_news_section_enable = get_theme_mod('latest_news_section_enable',false);
				if($latest_news_section_enable !=true)
				{
					if( is_active_sidebar('graphite_bottom_left_sidebar') ) :
						echo '<div class="col-md-4">';	
							dynamic_sidebar( 'graphite_bottom_left_sidebar' ); 
						echo"</div>";
					endif;
				}
				?>
			<!--/Homepage News & Blog Area-->		
			
			<!--Homepage About us Area-->
				<?php
				$about_info_section_enable = get_theme_mod('about_info_section_enable',false);
				if( $about_info_section_enable !=true )
				{
				
					if( is_active_sidebar('graphite_bottom_center_sidebar') ):
						echo '<div class="col-md-4">';
						dynamic_sidebar( 'graphite_bottom_center_sidebar' );
						echo '</div>';
					endif;
				
				}
				?>
			<!--/Homepage About us Area-->		
			
			<!--Homepage Google Map-->
			<?php
				$google_map_section_enable = get_theme_mod('google_map_section_enable',false);
				if($google_map_section_enable !=true)
				{
					if( is_active_sidebar('graphite_bottom_right_sidebar') ) :
						echo '<div class="col-md-4">';
						dynamic_sidebar( 'graphite_bottom_right_sidebar' );
						echo"</div>";
					endif;
				}
			?>
			<!--/Homepage Google Map-->
			
		</div>
	</div>
</section>
<!-- /Blog, About and Googlemap Section -->

<div class="clearfix"></div>

<?php endif; ?>
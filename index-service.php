<?php if(get_theme_mod('service_section_enable') == false) {
if(get_theme_mod('home_service_section_title') || get_theme_mod('home_service_section_discription')!='' || is_active_sidebar( 'graphite_service_sidebar' ) ) { ?>
<!-- Rooms Section -->
<section class="wbr-section" id="service">
	<div class="container">
		
		<?php  if( get_theme_mod('home_service_section_title') || get_theme_mod('home_service_section_discription')!='' ) { ?>
	    <!-- Section Title -->
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
					<?php if(get_theme_mod('home_service_section_title')) {?>
					<h1 class="widget-title wow fadeInUp animated animated" data-wow-duration="500ms" data-wow-delay="0ms">
					<?php echo get_theme_mod('home_service_section_title'); ?>
					</h1>
					<?php } ?>
					
					<?php if(get_theme_mod('home_service_section_discription')) {?>
					<div class="separator"><span></span></div>
					<p class="wow fadeInDown animated">
					<?php echo get_theme_mod('home_service_section_discription'); ?>
					</p>
					<?php } ?>
				</div>
			</div>
		</div>
		<!-- /Section Title -->
		<?php } ?>
		
		<?php if ( is_active_sidebar( 'graphite_service_sidebar' ) ) : ?>
		<div class="row">
			<?php dynamic_sidebar( 'graphite_service_sidebar' ); ?>
		</div>
		<?php endif; ?>
	</div>
</section>
<!-- /Rooms Section -->
<div class="clearfix"></div>
<?php } 
}?>
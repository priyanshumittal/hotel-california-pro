<!-- Slider Section -->	
<?php 
$slider_enabled = get_theme_mod('slider_enabled',true);
$slider_animation = get_theme_mod('slider_options','slide');
$slider_transition = get_theme_mod('slider_transition_delay','2000');
if($slider_enabled == true) {
?>
<section class="homepage-mycarousel">
<div id="carousel-example-generic" class="carousel slide <?php echo $slider_animation; ?>" data-ride="carousel" data-pause="hover"
	<?php if($slider_transition != '') { ?> data-interval="<?php echo $slider_transition; } ?>" >

	<!-- Indicators -->
		<?php
			$query_args = array();
			
			$slider_select_category = array();
			$slider_select_category = get_theme_mod('slider_select_category');
		
			$query_args = array( 'category__in'  => $slider_select_category,'ignore_sticky_posts' => 1);	
			
			
			$t=true;

			$the_query = new WP_Query($query_args);
			?>
			<div class="carousel-inner" role="listbox">
			<?php
			$i=0;
			if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) 
			{
				$the_query->the_post();
			?>
				<div class="item <?php if($t==true){ echo 'active'; } $t=false; ?>">
				<?php 
					$default_arg =array('class' => "img-responsive"); 
					if( has_post_thumbnail() ){
						the_post_thumbnail('', $default_arg); 
					}
					else
					{
						echo '<img class="img-responsive" src="'.WEBRITI_TEMPLATE_DIR_URI.'/images/slide/no-image.jpg">';
					}
					
				if ( has_post_format( 'aside' )) 
				{ 
						echo '<div class="format-aside1">';
						echo get_slider_quote_excerpt();
						echo '</div>';
				 } 
				else if( has_post_format( 'quote' ))
				{ 
				
					  echo '<div class="format-quote1">';
					  echo get_slider_quote_excerpt();
					  echo '<small><cite title="Source Title">';
					   the_title();
					  echo '</cite></small></div>';
			     }  
				else if ( has_post_format( 'status' )) 
				{ 

					echo '<div class="format-status1">';
					
					echo '<h1>';
				    the_title();
				    echo '</h1>';

					echo get_slider_video_excerpt();
					echo '</div>';
				}  
				else if ( has_post_format( 'video' ))
				{
					echo '<div class="container format-video1">';
					  get_template_part('content-slider','video');
					echo '</div>';
				}
				else if( has_post_format( 'audio' ))
				{
					echo '<div class="container format-standard1"><div class="slide-text-bg1">';
					echo  get_slider_video_excerpt();
					echo '</div></div>';
				}
				else 
				{ 
				
				   echo '<div class="container format-standard1"><div class="slide-text-bg1"><h1>';
				   the_title();
				   echo '</h1>';
					echo get_slider_excerpt();
				    echo '</div>';
				} 
				echo '</div>';
			  } 
				wp_reset_postdata(); 
			}  
			?>  
			</div>
			<?php if( $the_query->post_count > 1 ){ ?>
			<ul class="carou-direction-nav">
			<li><a class="carou-prev" href="#carousel-example-generic" data-slide="prev"></a></li>
			<li><a class="carou-next" href="#carousel-example-generic" data-slide="next"></a></li>
			</ul>
			<?php } ?>
</div>			
</section>
<!-- /Slider Section -->
<div class="clearfix"></div>
<?php } ?>
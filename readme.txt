Graphite

A Free Multi colored Business Blog theme that supports Primary menu's , Primary sidebar,Four widgets area at the footer region,Post Metaetc. 
It has a perfect design that's great for any Business/Firms  Blogs who wants a new look for their site. Three page templates Blog, Full Widht Page and Contact Page. 
Author: Priyanshu Mittal
Theme Homepage Url: http://webriti.com/Graphite

About:
Graphite a theme for business, consultancy firms etc  by Priyanshu Mittal (Author URI: http://www.webriti.com). 

The CSS, XHTML and design is released under GPL:
http://www.opensource.org/licenses/gpl-license.php

Feel free to use as you please. I would be very pleased if you could keep the Auther-link in the footer. Thanks and enjoy.

Graphite supports Custom Menu, Widgets and 
the following extra features:

 - Pre-installed menu and content colors
 - Responsive
 - Custom sidebars
 - Support for post thumbnails
 - Similar posts feature
 - 3 widgetized areas in the footer
 - Slideshow using Post Meta  
 - Custom footer
 - Translation Ready 
 

# Basic Setup of the Theme.
-----------------------------------
Fresh installation!

1. Upload the Graphite Theme folder to your wp-content/themes folder.
2. Activate the theme from the WP Dashboard.
3. Done!
=== Images ===

All images in Graphite are licensed under the terms of the GNU GPL.

# Top Navigation Menu:
- Default the page-links start from the left! Use the Menus function in Dashboard/Appearance to rearrange the buttons and build your own Custom-menu. DO NOT USE LONG PAGE NAMES, Maximum 14 letters/numbers incl. spaces!
- Read more here: http://codex.wordpress.org/WordPress_Menu_User_Guide

=============Page Templates======================
1. Contact  Page Tempalte:- Create a page as you do in WordPress and select the page template with the name 'Contact'


===========Front Page Added with the theme=================
1 It has header(logo + menus),slider, services,recent comments widgets and footer.

==================Featued Slider=============
For adding your custom Images in the slider you just need to add the information to the meta box , placed just below the post editor.

======Site Title and Description=============
Site Title and its description in not shown on home page besides this both are used above each page / post along with the search field.
	
Support
-------

Do you enjoy this theme? Send your ideas - issues - on the theme formn . Thank you!

Graphite WordPress Theme bundles the following third-party resources:

Font Awesome 4.6.3 by @davegandy - http://fontawesome.io - @fontawesome
License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
Source: http://fontawesome.io



# --- EOF --- #
============================Change Log================================
@Version 1.0.2
1. Added Gallry, client widget and woocomerce support.
@Version 1.0.1
1. Added Turkish Locale
@Version 1.0
Released

# --- EOF --- #
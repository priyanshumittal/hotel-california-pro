<?php 
// Adding customizer home page setting
function graphite_theme_style_customizer( $wp_customize ){
$wp_customize->remove_control('header_textcolor');

//Image Background image
class WP_graphite_pre_Customize_Control extends WP_Customize_Control {
public $type = 'new_menu';

       function render_content()
       
	   {
	   echo '<h3>'.__('Predefined colors','graphite').'</h3>';
		  $name = '_customize-image-radio-' . $this->id;
		  $i=1;
		  foreach($this->choices as $key => $value ) {
            ?>
               <label>
				<input type="radio" value="<?php echo $key; ?>" name="<?php echo esc_attr( $name ); ?>" data-customize-setting-link="<?php echo esc_attr( $this->id ); ?>" <?php if($this->value() == $key){ echo 'checked'; } ?>>
				<img <?php if($this->value() == $key){ echo 'class="color_scheem_active"'; } ?> src="<?php echo get_template_directory_uri(); ?>/images/bg-pattern/<?php echo $value; ?>" alt="<?php echo esc_attr( $value ); ?>" />
				</label>
            <?php 
			if($i==4)
			{
			  echo '<p></p>';
			  $i=0;
			}
			$i++;
			
			} ?>
			<h3><?php _e('Background Image','graphite'); ?></h3>
			<p><?php _e('Go to','graphite'); ?> => <?php _e('Appearance','graphite'); ?> => <?php _e('Customize','graphite');?> => <?php _e('Background Image','graphite'); ?></p><br/>
			<h3><?php _e('Background Color','graphite'); ?></h3>
			<p> <?php _e('Go to','graphite'); ?> => <?php _e('Appearance','graphite'); ?> => <?php _e('Customize','graphite');?> => <?php _e('Colors','graphite'); ?> </p>
		  <script>
			jQuery(document).ready(function($) {
				$("#customize-control-predefined_back_image label img").click(function(){
					$("#customize-control-predefined_back_image label img").removeClass("color_scheem_active");
					$(this).addClass("color_scheem_active");
				});
			});
		  </script>
		<?php
       }

}

//Layout Style
class WP_graphite_style_layout_Customize_Control extends WP_Customize_Control {
public $type = 'new_menu';

       function render_content()
       
	   {
	   echo '<h3>',__('Layout style','graphite').'</h3>';
		  $name = '_customize-layout-radio-' . $this->id; 
		  foreach($this->choices as $key => $value ) {
            ?>
               <label>
				<input type="radio" value="<?php echo $key; ?>" name="<?php echo esc_attr( $name ); ?>" data-customize-setting-link="<?php echo esc_attr( $this->id ); ?>" <?php if($this->value() == $key){ echo 'checked'; } ?>>
				<img <?php if($this->value() == $key){ echo 'class="color_scheem_active"'; } ?> src="<?php echo get_template_directory_uri(); ?>/images/bg-pattern/<?php echo $value; ?>" alt="<?php echo esc_attr( $value ); ?>" />
				</label>
				
            <?php
		  } ?>
		  <script>
			jQuery(document).ready(function($) {
				$("#customize-control-graphite_layout_style label img").click(function(){
					$("#customize-control-graphite_layout_style label img").removeClass("color_scheem_active");
					$(this).addClass("color_scheem_active");
				});
			});
		  </script>
		  <?php
       }

}

// Theme color
class WP_graphite_color_Customize_Control extends WP_Customize_Control {
public $type = 'new_menu';

       function render_content()
       
	   {
	   echo '<h3>'.__('Predefined colors','graphite').'</h3>';
		  $name = '_customize-color-radio-' . $this->id; 
		  foreach($this->choices as $key => $value ) {
            ?>
               <label>
				<input type="radio" value="<?php echo $key; ?>" name="<?php echo esc_attr( $name ); ?>" data-customize-setting-link="<?php echo esc_attr( $this->id ); ?>" <?php if($this->value() == $key){ echo 'checked="checked"'; } ?>>
				<img <?php if($this->value() == $key){ echo 'class="color_scheem_active"'; } ?> src="<?php echo get_template_directory_uri(); ?>/images/bg-pattern/<?php echo $value; ?>" alt="<?php echo esc_attr( $value ); ?>" />
				</label>
				
            <?php
		  }
		  ?>
		  <script>
			jQuery(document).ready(function($) {
				$("#customize-control-theme_color label img").click(function(){
					$("#customize-control-theme_color label img").removeClass("color_scheem_active");
					$(this).addClass("color_scheem_active");
				});
			});
		  </script>
		  <?php
       }

}

// Theme style type
class WP_graphite_style_type_Customize_Control extends WP_Customize_Control {
public $type = 'new_menu';

       function render_content()
       
	   {
	   echo '<h3>'.__('Custom pattern type','graphite').'</h3>';
		  $name = '_customize-color-radio-' . $this->id; 
		  foreach($this->choices as $key => $value ) {
            ?>
               <label>
				<input type="radio" value="<?php echo $key; ?>" name="<?php echo esc_attr( $name ); ?>" data-customize-setting-link="<?php echo esc_attr( $this->id ); ?>" <?php if($this->value() == $key){ echo 'checked="checked"'; } ?>>
				<img <?php if($this->value() == $key){ echo 'class="theme_style_active"'; } ?> src="<?php echo get_template_directory_uri(); ?>/images/bg-pattern/<?php echo $value; ?>" alt="<?php echo esc_attr( $value ); ?>" />
				</label>
				
            <?php
		  }
		  ?>
		  <script>
			jQuery(document).ready(function($) {
				$("#customize-control-theme_style_type label img").click(function(){
					$("#customize-control-theme_style_type label img").removeClass("theme_style_active");
					$(this).addClass("theme_style_active");
				});
			});
		  </script>
		  <?php
       }

}
	/* Theme Style settings */
	$wp_customize->add_section( 'theme_style' , array(
		'title'      => __('Theme style setting', 'graphite'),
		'priority'   => 120,
   	) );
	
	// Theme Color Scheme
	$wp_customize->add_setting(
	'theme_color', array(
	'default' => '#e32235',  
	'capability'     => 'edit_theme_options',
    ));
	$wp_customize->add_control( new WP_graphite_color_Customize_Control($wp_customize,'theme_color',
	array(
        'label'   => __('Predefined colors', 'graphite'),
        'section' => 'theme_style',
		'type' => 'radio',
		'choices' => array(
			'#e32235' => 'red.png',
            '#82b440' => 'green.png',
            '#d4ae60' => 'brown.png',
			'#01a4a4' => 'blue.png',
			'#bb8e5c' => 'gold.png',
    ))));
	
	// enable / disable custom color settings 
	$wp_customize->add_setting(
		'custom_color_enable',
		array('capability'  => 'edit_theme_options',
		'default' => false,
		
		));
	$wp_customize->add_control(
		'custom_color_enable',
		array(
			'type' => 'checkbox',
			'label' => __('Enable custom color skin','graphite'),
			'section' => 'theme_style',
		)
	);
	
	// link color settings
	$wp_customize->add_setting(
	'link_color', array(
	'capability'     => 'edit_theme_options',
	'default' => '#e32235'
    ));
	
	$wp_customize->add_control( 
	new WP_Customize_Color_Control( 
	$wp_customize, 
	'link_color', 
	array(
		'label'      => __( 'Skin color', 'graphite' ),
		'section'    => 'theme_style',
		'settings'   => 'link_color',
	) ) );
	
	// Theme style type
	$wp_customize->add_setting(
	'theme_style_type', array(
	'default' => 'default.css',  
	'capability'     => 'edit_theme_options',
    ));
	$wp_customize->add_control( new WP_graphite_style_type_Customize_Control($wp_customize,'theme_style_type',
	array(
        'label'   => __('Predefined colors', 'graphite'),
        'section' => 'theme_style',
		'type' => 'radio',
		'choices' => array(
			'default.css'=>'light.png',
			'dark.css'=>'dark.png',
    ))));
	
	//Theme Layout
	$wp_customize->add_setting(
	'graphite_layout_style', array(
	'default' => 'wide.jpg',  
	'capability'     => 'edit_theme_options',
    ));
	$wp_customize->add_control(new WP_graphite_style_layout_Customize_Control($wp_customize,'graphite_layout_style',
	array(
        'label'   => __('Layout style', 'graphite'),
        'section' => 'theme_style',
		'type' => 'radio',
		'choices' => array(
            'wide' => 'wide.png',
            'boxed' => 'boxed.png',
    )
	
	)));
	
	
	//Predefined Background image
	$wp_customize->add_setting(
	'predefined_back_image', array(
	'default' => 'bg-img1.png',  
	'capability'     => 'edit_theme_options',
    ));
	$wp_customize->add_control(new WP_graphite_pre_Customize_Control($wp_customize,'predefined_back_image',
	array(
        'label'   => __('Predefined pattern', 'graphite'),
        'section' => 'theme_style',
		'type' => 'radio',
		'choices' => array(
			'bg-img0.png' => 'sm0.png',
            'bg-img1.png' => 'sm1.png',
            'bg-img2.png' => 'sm2.png',
			'bg-img3.png' => 'sm3.png',
			'bg-img4.png' => 'sm4.png',
			'bg-img5.png' => 'sm5.png',
			'bg-img6.jpg' => 'sm6.jpg',
            'bg-img7.jpg' => 'sm7.jpg',
			'bg-img8.jpg' => 'sm8.jpg',
			'bg-img9.jpg' => 'sm9.jpg',
			'bg-img10.jpg' => 'sm10.jpg',
    ))));
}
add_action( 'customize_register', 'graphite_theme_style_customizer' );
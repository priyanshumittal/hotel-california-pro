<!-- Callout Section -->
		<?php 
			if( is_active_sidebar('wdl_about_page_bottom_footer_sidebar') ) :
				echo '<section class="wbr-section" id="callout"><div class="container"><div class="row">';
				dynamic_sidebar( 'wdl_about_page_bottom_footer_sidebar' );
				echo '</div></div></section>';
			endif;
		?>
<!-- /Callout Section -->
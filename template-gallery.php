<?php 
/**
 * Template Name: Gallery Template
 */
get_header(); 
graphite_breadcrumbs(); 
$footer_callout = get_theme_mod('footer_callout_enable','');
?>

<section class="blog-section portfolio-section">
	<div class="container">
	
		<div class="row">
		
			<?php 
			the_post(); 
			
			if( has_post_thumbnail() ): ?>
				<div class="col-md-12 col-xs-12">
				<div class="post-featured-area">
					<div class="blog-featured-img">
						<?php $arg = array( 'class'=>'img-responsive' ); the_post_thumbnail( '',$arg ); ?>
					</div>
				</div>
				</div>
			<?php endif; ?>	
			
			<div class="col-md-<?php echo ( !is_active_sidebar( 'graphite_gallery_page_sidebar' ) ? '12' :'8' ); ?> col-xs-12 wow fadeInDown animated animated" data-wow-delay="0.4s">			
				<?php the_content(); ?>
			</div>
			
			<?php if ( is_active_sidebar( 'graphite_gallery_page_sidebar' ) ) : ?>
			<!--Sidebar Section-->
			<div class="col-md-4 col-xs-12">
				<div class="sidebar">
					<?php dynamic_sidebar( 'graphite_gallery_page_sidebar' ); ?>		
				</div>
			</div>	
			<!--Sidebar Section-->
			<?php endif; ?>
			
		</div>

	</div>
</section>

<div class="clearfix"></div>

<?php 

get_template_part('room','footer_callout');

get_footer(); ?>
<?php 
/**
 * Template Name: About Page
 */
get_header(); 
graphite_breadcrumbs(); 
$amanities_section = get_theme_mod('amenities_section_enable',false);
$amenities_title = get_theme_mod('amenities_title',__('Graphite Amenities','graphite'));
$amenities_des = get_theme_mod('amenities_des','Sea summo mazim ex, ea errem eleifend definitionem vim. Ut nec hinc dolor possim mei ludus efficiendi ei sea summo mazim ex.');
$team_section_enable = get_theme_mod('team_section_enable',false);
$team_title = get_theme_mod('team_title',__('Our Graphite Staff','graphite'));
$team_des = get_theme_mod('team_dec','Sea summo mazim ex, ea errem eleifend definitionem vim. Ut nec hinc dolor possim mei ludus efficiendi ei sea summo mazim ex.');
$footer_callout = get_theme_mod('footer_callout_enable',false);

the_Post();
			
		$cc = get_the_content();
			
		if ( has_post_thumbnail() || $cc != '' ) :
?>

<!-- Blog & Sidebar Section -->
<section class="blog-section">
	<div class="container">
		<div class="row">	
			<!--Blog Section-->
			<div class="col-md-12 col-xs-12">
				<?php 
				if ( has_post_thumbnail() ) :
				echo '<div class="post-featured-area"><div class="blog-featured-img"><a class="post-thumbnail" href="'.get_the_permalink().'">';
				the_post_thumbnail( '', array( 'class'=>'img-responsive','alt' => get_the_title() ) );
				echo '</a></div></div>';
				endif;

				the_content( __('Read More','graphite') ); ?>
			</div>	
			<!--/Blog Section-->
		</div>
	</div>
</section>
<!-- /Blog & Sidebar Section -->
<?php endif; ?>

<div class="clearfix"></div>
<?php if($amanities_section == false) { ?>
<!-- Graphite Amenities Section -->
<section class="wbr-section wbr-typography" id="amenities" <?php $image = get_theme_mod('amenities_background_image'); if(!empty($image)){ echo 'style="background:url('.$image.')  repeat fixed 0 0 rgba(0, 0, 0, 0)"'; } ?>>
	<div class="overlay">
		<div class="container">			
			
			<!-- Section Title -->
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h1 class="white wow fadeInUp animated animated" data-wow-duration="500ms" data-wow-delay="0ms"><?php 
						echo $amenities_title; ?></h1>
						<div class="separator"><span></span></div>
						<p class="white wow fadeInDown animated"><?php echo $amenities_des; ?></p>
					</div>
				</div>
			</div>
			<!-- /Section Title -->		
			
			<!-- Amenities Area -->
			<div class="row">
			<?php 
			if( is_active_sidebar('about_page_amenities') ) {
				dynamic_sidebar( 'about_page_amenities' );
			} 
			?>
			</div>
			<!-- /Amenities Area -->	
			
		</div>	
	</div>	
</section>
<!-- /Graphite Amenities Section -->
<?php } ?>

<div class="clearfix"></div>
<?php if($team_section_enable == false) { ?>
<!-- Team Section -->
<section class="wbr-section" id="team">		
	<div class="container">
		
		<!-- Section Title -->
		<div class="row">
			<div class="col-md-12">
				<div class="section-title wow fadeInUp animated animated" data-wow-duration="500ms" data-wow-delay="0ms">
					<h1><?php echo $team_title; ?></h1>
					<div class="separator"><span></span></div>
					<p class="wow fadeInDown animated"><?php echo $team_des; ?></p>
				</div>
			</div>
		</div>
		<!-- /Section Title -->
			
		<!-- Team Area -->
		<div class="row">
			<?php if( is_active_sidebar('about_team_staff') ) {
				dynamic_sidebar( 'about_team_staff' );
				} 
			?>
		</div>
		<!-- /Team Area -->
		
	</div>
</section>
<!-- /Team Section -->
<?php } ?>
<div class="clearfix"></div>
<?php if($footer_callout == false) { ?>
<!-- Callout Section -->

<?php get_template_part('about','footer_callout'); ?>
<!-- /Callout Section -->
<?php } 
get_footer(); ?>
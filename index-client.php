<?php 
$client_section_enable = get_theme_mod('client_section_enable',false);
$home_client_section_title = get_theme_mod('home_client_section_title');
$home_client_section_discription = get_theme_mod('home_client_section_discription');
$client_column_layout = get_theme_mod('client_column_layout',6);
		
if($client_section_enable != true )
{
	if( 
		
		$home_client_section_title != null || 
		
		$home_client_section_discription != null ||
		
		is_active_sidebar( 'graphite_client_sidebar' )
		
		):
?>
<!--Our Clients-->
<section class="wbr-section" id="client">
	<div class="container">
		
		<?php 
		if( $home_client_section_title != null || $home_client_section_discription != null )
		{
		?>
		<!-- Section Title -->
		<div class="row">
			<div class="col-md-12">
				<div class="section-title" data-wow-duration="500ms" data-wow-delay="0ms">
				
					<?php if( $home_client_section_title != null ): ?>
					<h1 class="wow fadeInUp animated animated"><?php echo $home_client_section_title; ?></h1>
					<?php endif; ?>
					
					<?php if( $home_client_section_title != null && $home_client_section_discription != null ): ?>
					<div class="separator"><span></span></div>
					<?php endif; ?>
					
					<?php if( $home_client_section_discription != null ): ?>
					<p class="wow fadeInDown animated"><?php echo $home_client_section_discription; ?></p>
					<?php endif; ?>
					
				</div>
			</div>			
		</div>
		<!-- /Section Title -->
		<?php } ?>
		
		<?php
		if ( is_active_sidebar( 'graphite_client_sidebar' ) ) : ?>
		<div class="row">
		<?php
			dynamic_sidebar( 'graphite_client_sidebar' ); ?>
		</div>
		<?php endif;
		?>
		
	</div>
</section>	
<!--/Our Clients-->
<?php 
	endif;
} 
?>
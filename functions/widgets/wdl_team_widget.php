<?php
	// Register and load the widget
	function graphite_team_widget() {
	    register_widget( 'graphite_team_widget' );
	}
	add_action( 'widgets_init', 'graphite_team_widget' );

// Creating the widget
	class graphite_team_widget extends WP_Widget {
	 
	function __construct() {
		parent::__construct(
			'graphite_team_widget', // Base ID
			__('WBR: Team member widget','graphite'), // Widget Name
			array(
				'classname' => 'graphite_team_widget',
				'description' => __('About page team area','graphite'),
			),
			array(
				'width' => 600,
			)
		);
		
	} 
	 
	public function widget( $args, $instance ) {
	$custom_class=(isset($instance['custom_class'])?$instance['custom_class']:'');

	if($custom_class !='')
	{
		$args['before_widget'] = str_replace('class="', 'class="'. $custom_class . ' ',$args['before_widget']);
				
	}
	echo $args['before_widget']; ?>
	
			<div class="team-area wow fadeInDown animated" data-wow-delay="0.4s">
				<div class="team-image">
					<?php $default_image = get_template_directory_uri() .'/images/team/team1.jpg';?>
					<img class="img-responsive" src="<?php echo(!empty($instance['image']) ? $instance['image']:$default_image); ?>" />
					<div class="team-showcase-overlay">
						<div class="team-showcase-overlay-inner">
							<div class="team-showcase-icons">
								
								<?php if(!empty($instance['facebook_link'])) { ?>
								<a href="<?php echo(!empty($instance['facebook_link']) ? $instance['facebook_link']:"http://www.facebook.com"); ?>" target="_blank" class="hover_thumb">
								<?php if(!empty($instance['fa_facebook_icon'])) { ?>
								<i class="fa <?php echo $instance['fa_facebook_icon']; ?>"></i>
								<?php } ?> </a>
								<?php } ?>
								
								<?php if(!empty($instance['twitter_link'])) { ?>
								<a href="<?php echo(!empty($instance['twitter_link']) ? $instance['twitter_link']:"http://www.twitter.com"); ?>" target="_blank" class="hover_thumb">
								<?php if(!empty($instance['fa_twitter_icon'])) { ?>
								<i class="fa <?php echo $instance['fa_twitter_icon']; ?>"></i>
								<?php } ?>
								</a>
								<?php } ?>
								<?php if(!empty($instance['linkdin_link'])) { ?>
								<a href="<?php echo(!empty($instance['linkdin_link']) ? $instance['linkdin_link']:"http://www.linkdin.com"); ?>" target="_blank" class="hover_thumb">
								<?php if(!empty($instance['fa_linkdin_icon'])) { ?>
								<i class="fa <?php echo $instance['fa_linkdin_icon']; ?>"></i>
								<?php } ?>
								</a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<div class="team-caption">
								<h4><?php if(!empty($instance['name'])) { ?>
								<?php echo $instance['name']; ?>
								<?php } else { ?> 
								<?php echo 'John Doe'; ?>
								<?php } ?>
								</h4>
								<h6><?php if(!empty($instance['post'])) { ?>
								<?php echo $instance['post']; ?>
								<?php } else { ?> 
								<?php _e('Graphite Manager','graphite'); ?>
								<?php } ?>
								</h6>
				</div>
				</div>
	<?php
	echo $args['after_widget'];
	}
	         
	// Widget Backend
	public function form( $instance ) {
	if ( isset( $instance[ 'name' ])){
	$name = $instance[ 'name' ];
	}
	else {
	$name = 'John Doe';
	}
	if ( isset( $instance[ 'post' ])){
	$post = $instance[ 'post' ];
	}
	else {
	$post = __( 'Graphite Manager', 'graphite' );
	}
	if ( isset( $instance[ 'fa_facebook_icon' ])){
	$fa_facebook_icon = $instance[ 'fa_facebook_icon' ];
	}
	else {
	$fa_facebook_icon = 'fa fa-facebook';
	}
	if (isset ($instance ['facebook_link']))
	{
	$facebook_link = $instance[ 'facebook_link' ];
	}
	else
	{
	$facebook_link = '#';
	}
	if ( isset( $instance[ 'fa_twitter_icon' ])){
	$fa_twitter_icon = $instance[ 'fa_twitter_icon' ];
	}
	else {
	$fa_twitter_icon = 'fa fa-twitter';
	}
	if (isset ($instance ['twitter_link']))
	{
	$twitter_link = $instance[ 'twitter_link' ];
	}
	else
	{
	$twitter_link = '#';
	}
	
	if ( isset( $instance[ 'fa_linkdin_icon' ])){
	$fa_linkdin_icon = $instance[ 'fa_linkdin_icon' ];
	}
	else {
	$fa_linkdin_icon =  'fa fa-linkedin';
	}
	if (isset ($instance ['linkdin_link']))
	{
	$linkdin_link = $instance[ 'linkdin_link' ];
	}
	else
	{
	$linkdin_link = '#';
	}
	$image = '';
    if(isset($instance['image']))
    {
    $image = $instance['image'];
    }
	$instance['custom_class'] = (isset($instance['custom_class'])? $instance['custom_class']: '');
	
	
	

	// Widget admin form
	?>
	
	<p>
	<label for="<?php echo $this->get_field_id( 'name' ); ?>"><?php _e( 'Name','graphite' ); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id( 'name' ); ?>" name="<?php echo $this->get_field_name( 'name' ); ?>" type="text" value="<?php if($name) echo esc_attr( $name ); else 'John Doe';?>" />
	</p>
	
	<p>
	<label for="<?php echo $this->get_field_id( 'post' ); ?>"><?php _e('Designation','graphite' ); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id( 'post' ); ?>" name="<?php echo $this->get_field_name( 'post' ); ?>" type="text" value="<?php if($post) echo esc_attr($post); else _e( 'Graphite Manager', 'graphite' );?>" />
	</p>
	
	<p>
	<label for="<?php echo $this->get_field_id('fa_facebook_icon')?> " > <?php _e('Icon','graphite'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id( 'fa_facebook_icon' ); ?>" name="<?php echo $this->get_field_name( 'fa_facebook_icon' ); ?>" type="text" value="<?php if($fa_facebook_icon) echo esc_attr( $fa_facebook_icon ); else echo 'fa fa-facebook';?>" />
	</p>
	
	<p>
	<label for="<?php echo $this->get_field_id( 'facebook_link' ); ?>"><?php _e( 'Link','graphite' ); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id( 'facebook_link' ); ?>" name="<?php echo $this->get_field_name( 'facebook_link' ); ?>" type="text" value="<?php if($facebook_link) echo esc_attr($facebook_link); else echo 'http://www.facebook.com';?>" />
	<span><?php _e('Link to get fa-icon ','graphite'); ?><a href="<?php echo 'http://fortawesome.github.io/Font-Awesome/icons/'; ?>" target="_blank" ><?php _e('fa-icon','graphite'); ?></a></span>
	</p>
	
	
	<p>
	<label for="<?php echo $this->get_field_id('fa_twitter_icon')?> " > <?php _e('Icon','graphite'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id( 'fa_twitter_icon' ); ?>" name="<?php echo $this->get_field_name( 'fa_twitter_icon' ); ?>" type="text" value="<?php if($fa_twitter_icon) echo esc_attr( $fa_twitter_icon ); else echo 'fa fa-twitter';?>" />
	</p>
	
	<p>
	<label for="<?php echo $this->get_field_id( 'twitter_link' ); ?>"><?php _e( 'Link','graphite' ); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id( 'twitter_link' ); ?>" name="<?php echo $this->get_field_name( 'twitter_link' ); ?>" type="text" value="<?php if($twitter_link) echo esc_attr($twitter_link); else echo 'http://www.twitter.com';?>" />
	<span><?php _e('Link to get fa-icon ','graphite'); ?><a href="<?php echo 'http://fortawesome.github.io/Font-Awesome/icons/'; ?>" target="_blank" ><?php _e('fa-icon','graphite'); ?></a></span>
	</p>
	
	
	<p>
	<label for="<?php echo $this->get_field_id('fa_linkdin_icon')?> " > <?php _e('Icon','graphite'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id( 'fa_linkdin_icon' ); ?>" name="<?php echo $this->get_field_name( 'fa_linkdin_icon' ); ?>" type="text" value="<?php if($fa_linkdin_icon) echo esc_attr( $fa_linkdin_icon ); else echo 'fa fa-twitter';?>" />
	</p>
	
	<p>
	<label for="<?php echo $this->get_field_id( 'linkdin_link' ); ?>"><?php _e( 'Link','graphite' ); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id( 'linkdin_link' ); ?>" name="<?php echo $this->get_field_name( 'linkdin_link' ); ?>" type="text" value="<?php if($linkdin_link) echo esc_attr($linkdin_link); else echo 'http://www.twitter.com';?>" />
	</p>
	
	<p>
	<span><?php _e('Link to get fa-icon ','graphite'); ?><a href="<?php echo 'http://fortawesome.github.io/Font-Awesome/icons/'; ?>" target="_blank" ><?php _e('fa-icon','graphite'); ?></a></span>
	
	
	 <input type="text" class="widefat custom_media_url_team"
                   name="<?php echo $this->get_field_name('image'); ?>" id="<?php echo $this->get_field_id('image'); ?>" value="<?php if( !empty($instance['image']) ): echo $instance['image']; endif; ?>"
                   style="margin-top:5px;">


            <input type="button" class="button button-primary custom_media_button_team" id="custom_media_button_clients"
                   name="<?php echo $this->get_field_name('image'); ?>" value="<?php _e('Upload image','graphite'); ?>"
                   style="margin-top:5px;"/>
	</p>			   
	
		<p>
			<label for="<?php echo $this->get_field_id( 'custom_class' ); ?>"><?php _e('CSS Classes (optional)','graphite' ); ?></label> 
		</p>
		<input class="widefat" id="<?php echo $this->get_field_id( 'custom_class' ); ?>" name="<?php echo $this->get_field_name( 'custom_class' ); ?>" type="text" value="<?php if($instance[ 'custom_class' ]) echo esc_attr($instance[ 'custom_class' ]);?>" />
	
	
	<?php
    }
	     
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
	
		$instance = array();
		$instance['name'] = ( ! empty( $new_instance['name'] ) ) ? strip_tags( $new_instance['name'] ) : '';
		$instance['post'] = ( ! empty( $new_instance['post'] ) ) ? $new_instance['post'] : '';
		$instance['fa_facebook_icon'] = ( ! empty( $new_instance['fa_facebook_icon'] ) ) ? $new_instance['fa_facebook_icon'] : '';
		$instance['facebook_link'] = ( ! empty( $new_instance['facebook_link'] ) ) ? $new_instance['facebook_link'] : '';
		$instance['fa_twitter_icon'] = ( ! empty( $new_instance['fa_twitter_icon'] ) ) ? $new_instance['fa_twitter_icon'] : '';
		$instance['twitter_link'] = ( ! empty( $new_instance['twitter_link'] ) ) ? $new_instance['twitter_link'] : '';
		$instance['fa_linkdin_icon'] = ( ! empty( $new_instance['fa_linkdin_icon'] ) ) ? $new_instance['fa_linkdin_icon'] : '';
		$instance['linkdin_link'] = ( ! empty( $new_instance['linkdin_link'] ) ) ? $new_instance['linkdin_link'] : '';
		$instance['image'] = ( ! empty( $new_instance['image'] ) ) ? $new_instance['image'] : '';
		$instance['custom_class'] = ( ! empty( $new_instance['custom_class'] ) ) ? $new_instance['custom_class'] : '';
		
		return $instance;
	}
	}
	?>
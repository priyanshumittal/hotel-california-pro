<?php 
/**
 * Template Name: Room 2 Column
 */
get_header(); 
graphite_breadcrumbs(); 
$footer_callout = get_theme_mod('footer_callout_enable','');
?>


<!--Accomodation Section-->
<section class="portfolio-section">
	<div class="container">
		<div class="row">
			<?php 
			the_post();
			
			if( has_post_thumbnail() ): ?>
				<div class="col-md-12">
				<?php echo '<div class="post-featured-area"><div class="blog-featured-img"><a class="post-thumbnail" href="'.get_the_permalink().'">';
				the_post_thumbnail( '', array( 'class'=>'img-responsive','alt' => get_the_title() ) );
				echo '</a></div></div>'; ?>
				</div>
			<?php endif; ?>			
			
			<div class="col-md-12 wow fadeInDown animated animated" data-wow-delay="0.4s">
				<?php the_content(); ?>
			</div>	
		</div>
	
		<div class="row">
			<?php graphite_get_child_pages($post->ID, 'col-md-6 col-sm-6 col-xs-12', 2);?>				   									
		</div>
		
		
		<!-- Pagination -->
	<?php //to be added later?>
		<!-- Pagination -->

	</div>
</section>
<!--/Accomodation Section-->



<div class="clearfix"></div>

<!-- Callout Section -->
<?php get_template_part('room','footer_callout');?>
<!-- /Callout Section -->

<?php get_footer(); ?>
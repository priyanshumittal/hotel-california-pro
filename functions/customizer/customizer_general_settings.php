<?php 
function graphite_general_settings_customizer( $wp_customize ){


/* Home Page Panel */
	$wp_customize->add_panel( 'general_settings', array(
		'priority'       => 125,
		'capability'     => 'edit_theme_options',
		'title'      => __('General settings','graphite'),
	) );
	
	/* Reservation section */
	$wp_customize->add_section( 'reservation_setting' , array(
		'title'      => __('Reservation button setting','graphite'),
		'panel'  => 'general_settings',
   	) );
	
	
			// Reservation Title
			$wp_customize->add_setting( 'reservation_title',array(
			'capability'     => 'edit_theme_options',
			'default' => __('RESERVATION','graphite'),
			'sanitize_callback' => 'graphite_home_page_sanitize_text',
			));	
			$wp_customize->add_control( 'reservation_title',array(
			'label'   => __('Title','graphite'),
			'section' => 'reservation_setting',
			'type' => 'text',
			));
    
	
			//Reservation link
			$wp_customize->add_setting( 'reservation_link',array(
			'capability'     => 'edit_theme_options',
			'default'=>'#',
			'sanitize_callback' => 'graphite_home_page_sanitize_text',
			));	
			$wp_customize->add_control( 'reservation_link',array(
			'label'   => __('Link','graphite'),
			'section' => 'reservation_setting',
			'type' => 'text',
			));
			
			//Reservation target
			$wp_customize->add_setting('reservation_target',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'graphite_home_page_sanitize_text',
			'default' => true,
			));	
			$wp_customize->add_control( 'reservation_target',array(
			'label'   => __('Open link in new tab','graphite'),
			'section' => 'reservation_setting',
			'type' => 'checkbox',
			));
			
			
	/* Remove animation */
	$wp_customize->add_section( 'remove_wow_animation_setting' , array(
		'title'      => __('Remove animation','graphite'),
		'panel'  => 'general_settings',
   	) );
	
	
			// Reservation Title
			$wp_customize->add_setting( 'remove_wow_animation',array(
			'capability'     => 'edit_theme_options',
			'default' => false,
			'sanitize_callback' => 'graphite_home_page_sanitize_text',
			));	
			$wp_customize->add_control( 'remove_wow_animation',array(
			'label'   => __('Remove wow effect from whloe website','graphite'),
			'section' => 'remove_wow_animation_setting',
			'type' => 'checkbox',
			));		

	/* footer copyright section */
	$wp_customize->add_section( 'graphite_footer_copyright' , array(
		'title'      => __('Footer copyright settings','graphite'),
		'panel'  => 'general_settings',
   	) );
	
	
	$wp_customize->add_setting(
		'footer_copyright_text',
		array(
			'default'           =>  '<p>'.__('Copyright @ 2015 Graphite. All right reserved','graphite').'</p>',
			'capability'        =>  'edit_theme_options',
			'sanitize_callback' =>  'graphite_copyright_sanitize_text',
		)	
	);
	$wp_customize->add_control('footer_copyright_text', array(
			'label' => __('Copyright text','graphite'),
			'section' => 'graphite_footer_copyright',
			'type'    =>  'textarea'
	));	 // footer copyright
	
	function graphite_copyright_sanitize_text( $input ) 
	{
		return wp_kses_post( force_balance_tags( $input ) );
	}
}
add_action( 'customize_register', 'graphite_general_settings_customizer' );
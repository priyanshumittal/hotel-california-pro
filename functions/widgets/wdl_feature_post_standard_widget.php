<?php
// business content top sidebar
add_action( 'widgets_init', 'graphite_feature_standard_post_widget' );
function graphite_feature_standard_post_widget() {
	register_widget( 'graphite_feature_standard_post_widget' );
}
	

// Creating the widget
class graphite_feature_standard_post_widget extends WP_Widget {
	 
	function __construct() {
		parent::__construct(
			'graphite_feature_standard_post_widget', // Base ID
			__('WBR : Rooms widget','graphite'), // Widget Name
			array(
				'classname' => 'widget_wdl_feature_post_standard',
				'description' => __('Featured post carousel widget','graphite'),
			),
			array(
				'width' => 600,
			)
		);	
	}
	 
	public function widget( $args, $instance ) { 
		$ids = array();
		//echo $args['before_widget'];
		echo '<div class="clearfix"></div><div class="widget">';
		$instance['blog_columns'] = (isset($instance['blog_columns'])?$instance['blog_columns']:3);
		$instance['blog_cat'] = (isset($instance['blog_cat'])?$instance['blog_cat']:'');
		$instance['blog_animation'] = (isset($instance['blog_animation'])?$instance['blog_animation']:'');
		$instance['slider_speed'] = (isset($instance['slider_speed'])?$instance['slider_speed']:'');
		$instance['blog_carousel_duration'] = (isset($instance['blog_carousel_duration'])?$instance['blog_carousel_duration']:'');
		$instance['blog_carousel_effect'] = (isset($instance['blog_carousel_effect'])?$instance['blog_carousel_effect']:false);
		$instance['exclude_posts'] = (isset($instance['exclude_posts'])?$instance['exclude_posts']:'');
		$custom_class=(isset($instance['custom_class'])?$instance['custom_class']:'');
		
		if($instance['blog_columns'] == null){
			$instance['blog_columns'] = 3;
		}
		
		if($instance['exclude_posts']!=null){
			$ids = explode(',',$instance['exclude_posts']);
		}
		if($custom_class !='')
		{
		$args['before_widget'] = str_replace('class="', 'class="'. $custom_class . ' ',$args['before_widget']);	
		}
	?>
	<script>
		jQuery(document).ready(function(){
			var column = <?php echo $instance['blog_columns']; ?>;
			jQuery('#slider-<?php echo $args['widget_id'];  ?> .item').each(function(){
				
				if(column==2){
					var next = jQuery(this).next();
					if (!next.length) {
						next = jQuery(this).siblings(':first');
					}
					next.children(':first-child').clone().appendTo(jQuery(this));
				}
				else if(column==3)
				{
					var next = jQuery(this).next();
					if (!next.length) {
						next = jQuery(this).siblings(':first');
					}
					next.children(':first-child').clone().appendTo(jQuery(this));
					
					for (var i=0;i<1;i++) {
						next=next.next();
						if (!next.length) {
							next = jQuery(this).siblings(':first');
						}
						next.children(':first-child').clone().appendTo(jQuery(this));
					}
				}
				else if(column==4){
					var next = jQuery(this).next();
					if (!next.length) {
						next = jQuery(this).siblings(':first');
					}
					next.children(':first-child').clone().appendTo(jQuery(this));
					
					for (var i=0;i<2;i++) {
						next=next.next();
						if (!next.length) {
							next = jQuery(this).siblings(':first');
						}
						next.children(':first-child').clone().appendTo(jQuery(this));
					}
				}
			});
		});
	</script>
		
		<!-- Item Scroll -->
	<?php if( $instance['blog_cat'] != null ): ?>
		
		<div class="wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="<?php echo (isset($instance['blog_carousel_duration'])?$instance['blog_carousel_duration']:300); ?>ms">
			<div class="carousel slide <?php echo ($instance['blog_animation']=='carousel-fade'?$instance['blog_animation']:''); ?>" data-ride="carousel" data-type="multi" data-interval="<?php echo (isset($instance['slider_speed'])?$instance['slider_speed']:3000); ?>" id="slider-<?php echo $args['widget_id'];  ?>">
				<div class="carousel-inner">
					<?php 
					$loop = new WP_Query(array( 'post_type' => 'post', 'cat' => $instance['blog_cat'],'post__not_in' => $ids ));
					$count=1;
					
					$temp_col = 12 / $instance['blog_columns'];
					
					if( $loop->have_posts() ) : 
						while ( $loop->have_posts() ) : $loop->the_post();
						if($instance['blog_carousel_effect']!=true){
						?>
						<div class="item <?php echo ( $count==1 ? 'active' : ''); ?>">						
							<div class="col-md-<?php echo $temp_col; ?> col-sm-6 col-xs-12 pull-left">
								<div class="portfolio-content-area">
									<div class="portfolio-img">
										<?php if(get_the_post_thumbnail_caption()) {?>
										<span class="wp-caption">
										<?php the_post_thumbnail_caption(); ?>
										</span>
										<?php } ?>
										<?php $defalt_arg =array('class' => "img-responsive"); ?>
										<?php if(has_post_thumbnail()): ?>
										<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('', $defalt_arg); ?></a>
										<?php endif; ?>
									</div>
										
									<div class="portfolio-content">
										<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>	
										
										<?php  the_content(__('Read More','graphite')); ?>
											
									</div>
								</div>
							</div>
							
						</div>
						<?php } else { ?>
						<div class="col-md-<?php echo $temp_col; ?> col-sm-6 col-xs-12 pull-left">
								<div class="portfolio-content-area">
									<div class="portfolio-img">
										
										<span class="wp-caption">
										<?php echo get_the_post_thumbnail_caption(); ?>
										</span>
										
										<?php $defalt_arg =array('class' => "img-responsive"); ?>
										<?php if(has_post_thumbnail()): ?>
										<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('', $defalt_arg); ?></a>
										<?php endif; ?>
									</div>
									<div class="portfolio-content">
										<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>	
										<?php  the_content(__('Read More','graphite')); ?>
									</div>
								</div>
						</div>
					<?php
							if($count%$instance['blog_columns']==0){
								echo "<div class='clearfix'></div>"; 
							} 
						}
						$count++;
						endwhile;
					endif;
					?>
				</div>
			</div>
		</div>
		
	
		
		<?php 
		if($instance['blog_carousel_effect']!=true){
			if($count>2): 
		?>
		<div class="col-md-12">
			<ul class="scroll-btn">
				<li><a class="btn-prev" href="#slider-<?php echo $args['widget_id'];  ?>" data-slide="prev"></a></li>
				<li><a class="btn-next" href="#slider-<?php echo $args['widget_id'];  ?>" data-slide="next"></a></li>    
			</ul>
		</div>
		<?php
			endif;
		}
		endif;
		
	//echo $args['after_widget'];
	echo '</div>';
	}
	         
	// Widget Backend
	public function form( $instance ) {
		
		$instance['blog_columns'] = (isset($instance['blog_columns'])?$instance['blog_columns']:'');
		$instance['blog_cat'] = (isset($instance['blog_cat'])?$instance['blog_cat']:'');
		$instance['blog_animation'] = (isset($instance['blog_animation'])?$instance['blog_animation']:'');
		$instance['slider_speed'] = (isset($instance['slider_speed'])?$instance['slider_speed']:'');
		$instance['blog_carousel_duration'] = (isset($instance['blog_carousel_duration'])?$instance['blog_carousel_duration']:'');
		$instance['blog_carousel_effect'] = (isset($instance['blog_carousel_effect'])?$instance['blog_carousel_effect']:false);
		$instance['exclude_posts'] = (isset($instance['exclude_posts'])?$instance['exclude_posts']:'');
		$instance['custom_class'] = (isset($instance['custom_class'])? $instance['custom_class']: '');
	?>
	<br/>
	<p>
		<label for="<?php echo $this->get_field_id( 'blog_columns' ); ?>"><?php _e( 'Select column layout','graphite' ); ?></label><br/> 
		<select id="<?php echo $this->get_field_id( 'blog_columns' ); ?>" name="<?php echo $this->get_field_name( 'blog_columns' ); ?>">
			<option value>-- <?php _e('Select column layout','graphite'); ?> --</option>
			<option value="1" <?php echo ($instance['blog_columns']==1?'selected':''); ?>><?php _e('1 Column','graphite'); ?></option>
			<option value="2" <?php echo ($instance['blog_columns']==2?'selected':''); ?>><?php _e('2 Column','graphite'); ?></option>
			<option value="3" <?php echo ($instance['blog_columns']==3?'selected':''); ?>><?php _e('3 Column','graphite'); ?></option>
			<option value="4" <?php echo ($instance['blog_columns']==4?'selected':''); ?>><?php _e('4 Column','graphite'); ?></option>
		</select>
	</p>
	<hr><br/>
	<p>
		<label for="<?php echo $this->get_field_id( 'exclude_posts' ); ?>"><?php _e( 'Exclude posts # id like (1,2,3...etc)','graphite' ); ?></label> 
		<textarea rows="5" class="widefat" id="<?php echo $this->get_field_id( 'exclude_posts' ); ?>" name="<?php echo $this->get_field_name( 'exclude_posts' ); ?>"><?php if($instance['exclude_posts']) echo $instance['exclude_posts']; ?></textarea>
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'blog_cat' ); ?>"><?php _e( 'Select blog category','graphite' ); ?></label><br/>
		<select id="<?php echo $this->get_field_id( 'blog_cat' ); ?>" name="<?php echo $this->get_field_name( 'blog_cat' ); ?>">
			<option value>-- <?php _e('Select category','graphite'); ?> --</option>
			<?php 
				$options = array();
				$cats = get_categories($options);

                foreach ( $cats as $cat )
                {
                    printf('<option value="%s" %s>%s</option>', $cat->term_id, selected($instance['blog_cat'], $cat->term_id, false), $cat->name);
                }
			?>
		</select>
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'blog_animation' ); ?>"><?php _e( 'Animation','graphite' ); ?></label><br/> 
		<select id="<?php echo $this->get_field_id( 'blog_animation' ); ?>" name="<?php echo $this->get_field_name( 'blog_animation' ); ?>">
			<option value>-- <?php _e('Select animation','graphite'); ?> --</option>
			<option value="slide" <?php echo ($instance['blog_animation']=='slide'?'selected':''); ?>><?php _e('Slide','graphite'); ?></option>
			<option value="carousel-fade" <?php echo ($instance['blog_animation']=='carousel-fade'?'selected':''); ?>><?php _e('Fade','graphite'); ?></option>
		</select>
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'slider_speed' ); ?>"><?php _e( 'Slider speed','graphite' ); ?></label><br/> 
		<select id="<?php echo $this->get_field_id( 'slider_speed' ); ?>" name="<?php echo $this->get_field_name( 'slider_speed' ); ?>">
			<option value>-- <?php _e('Slider speed','graphite'); ?> --</option>
			<option value="500" <?php echo ($instance['slider_speed']==500?'selected':''); ?>><?php echo '500'; ?></option>
			<option value="1000" <?php echo ($instance['slider_speed']==1000?'selected':''); ?>><?php echo '1000'; ?></option>
			<option value="1500" <?php echo ($instance['slider_speed']==1500?'selected':''); ?>><?php echo '1500'; ?></option>
			<option value="2000" <?php echo ($instance['slider_speed']==2000?'selected':''); ?>><?php echo '2000'; ?></option>
			<option value="2500" <?php echo ($instance['slider_speed']==2500?'selected':''); ?>><?php echo '2500'; ?></option>
			<option value="3000" <?php echo ($instance['slider_speed']==3000?'selected':''); ?>><?php echo '3000'; ?></option>
			<option value="3500" <?php echo ($instance['slider_speed']==3500?'selected':''); ?>><?php echo '3500'; ?></option>
			<option value="4000" <?php echo ($instance['slider_speed']==4000?'selected':''); ?>><?php echo '4000'; ?></option>
		</select>
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'blog_carousel_duration' ); ?>"><?php _e( 'Slider duration','graphite' ); ?></label><br/> 
		<select id="<?php echo $this->get_field_id( 'blog_carousel_duration' ); ?>" name="<?php echo $this->get_field_name( 'blog_carousel_duration' ); ?>">
			<option value>-- <?php _e('Slider duration','graphite'); ?> --</option>
			<option value="300" <?php echo ($instance['blog_carousel_duration']==300?'selected':''); ?>><?php echo '300'; ?></option>
			<option value="500" <?php echo ($instance['blog_carousel_duration']==500?'selected':''); ?>><?php echo '500'; ?></option>
			<option value="1000" <?php echo ($instance['blog_carousel_duration']==1000?'selected':''); ?>><?php echo '1000'; ?></option>
			<option value="1500" <?php echo ($instance['blog_carousel_duration']==1500?'selected':''); ?>><?php echo '1500'; ?></option>
			<option value="2000" <?php echo ($instance['blog_carousel_duration']==2000?'selected':''); ?>><?php echo '2000'; ?></option>
			<option value="2500" <?php echo ($instance['blog_carousel_duration']==2500?'selected':''); ?>><?php echo '2500'; ?></option>
			<option value="3000" <?php echo ($instance['blog_carousel_duration']==3000?'selected':''); ?>><?php echo '3000'; ?></option>
			<option value="3500" <?php echo ($instance['blog_carousel_duration']==3500?'selected':''); ?>><?php echo '3500'; ?></option>
			<option value="4000" <?php echo ($instance['blog_carousel_duration']==4000?'selected':''); ?>><?php echo '4000'; ?></option>
		</select>
	</p>
	<hr><br/>
	<p>
		<label for="<?php echo $this->get_field_id( 'blog_carousel_effect' ); ?>"><?php _e( 'Disable carousel effect','graphite' ); ?></label><br/> 
		<input type="checkbox" class="widefat" id="<?php echo $this->get_field_id( 'blog_carousel_effect' ); ?>" name="<?php echo $this->get_field_name( 'blog_carousel_effect' ); ?>" value="1" <?php if($instance['blog_carousel_effect']==true) echo 'checked'; ?>>
		<br/>
	</p>
		<br/>
		<p>
			<label for="<?php echo $this->get_field_id( 'custom_class' ); ?>"><?php _e('CSS Classes (optional)','graphite' ); ?></label> 
		</p>
		<input class="widefat" id="<?php echo $this->get_field_id( 'custom_class' ); ?>" name="<?php echo $this->get_field_name( 'custom_class' ); ?>" type="text" value="<?php if($instance[ 'custom_class' ]) echo esc_attr($instance[ 'custom_class' ]);?>" />
	
	<?php
    }
	     
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
	
		$instance = array();
		
		$instance['blog_columns'] = ( ! empty( $new_instance['blog_columns'] ) ) ? strip_tags( $new_instance['blog_columns'] ) :'';

		$instance['blog_cat'] = ( ! empty( $new_instance['blog_cat'] ) ) ?  $new_instance['blog_cat'] : '';
		
		$instance['blog_animation'] = ( ! empty( $new_instance['blog_animation'] ) ) ?  $new_instance['blog_animation'] : '';
		
		$instance['slider_speed'] = ( ! empty( $new_instance['slider_speed'] ) ) ?  $new_instance['slider_speed'] : '';
		
		$instance['blog_carousel_effect'] = ( ! empty( $new_instance['blog_carousel_effect'] ) ) ?  $new_instance['blog_carousel_effect'] : '';
		
		$instance['blog_carousel_duration'] = ( ! empty( $new_instance['blog_carousel_duration'] ) ) ?  $new_instance['blog_carousel_duration'] : '';
		
		$instance['exclude_posts'] = ( ! empty( $new_instance['exclude_posts'] ) ) ?  $new_instance['exclude_posts'] : '';
		
		$instance['custom_class'] = ( ! empty( $new_instance['custom_class'] ) ) ? $new_instance['custom_class'] : '';

		return $instance;
	}
}
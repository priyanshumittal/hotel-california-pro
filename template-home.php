<?php 
/**
 * Template Name: Home Page
 */
get_header(); ?>

<?php 
		
		
		$front_page = get_theme_mod('front_page_data','service,Room,Testimonial,News - About - Google Map,additional,gallery,client,Footer-callout');
		get_template_part('index','slider');
		
		$data =is_array($front_page) ? $front_page : explode(",",$front_page);
		
		if($data) 
		{
			foreach($data as $key=>$value)
			{
				get_sidebar('cta');
				switch($value) 
				{

					case 'service':
						get_template_part('index','service');
					break;
					
					case 'Room':
						get_template_part('index','room');
					break;
					
					case 'Testimonial':
						get_template_part('index','testimonial');
					break;
					
					case 'News - About - Google Map';
						get_template_part('index','footer_widget');
					break;
					
					case 'additional';
						get_template_part('index','additional');
					break;
					
					case 'gallery';
						get_template_part('index','gallery');
					break;
					
					case 'client';
						get_template_part('index','client');
					break;
					
					
					case 'Footer-callout';
						get_template_part('index','footer_callout');
					break;
				}
			}
		}

?>
<?php get_footer(); ?>
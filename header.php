<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>	
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
	
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>

	<?php wp_head(); ?>
</head>
	<?php 
	$layout_selector=get_theme_mod('graphite_layout_style','wide');
	if($layout_selector == "boxed")
	{ $class="boxed"; }
	else
	{ $class="wide"; }
	
	$grahpite_wrap_style = get_theme_mod('grahpite_wrap_style','default.css');
	if( $grahpite_wrap_style == 'dark.css')
	{$wrap = 'dark.css' ;}
	else{$wrap ='default.css';}
 ?>
<body <?php body_class($class); ?> >
<div id="wrapper" class="<?php echo $grahpite_wrap_style; ?>">
<?php get_template_part('header/header-topbar'); ?>
<?php get_template_part('header/header-navbar'); ?>
<?php //For pages

$slider_enable_page = sanitize_text_field( get_post_meta( get_the_ID(), 'slider_enable_page', true ));
if($slider_enable_page == true){
get_template_part('index','slider');

//For post
$slider_enable_post = sanitize_text_field( get_post_meta( get_the_ID(), 'slider_enable_post', true ));
if($slider_enable_post == true){
get_template_part('index','slider');
}
}
?>
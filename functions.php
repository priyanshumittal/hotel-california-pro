<?php
// Global variables define
define('WEBRITI_TEMPLATE_DIR_URI',get_template_directory_uri());	
define('WEBRITI_TEMPLATE_DIR',get_template_directory());
define('WEBRITI_THEME_FUNCTIONS_PATH',WEBRITI_TEMPLATE_DIR.'/functions');


// Theme functions file including

require( WEBRITI_TEMPLATE_DIR . '/css/custom_dark.php');
require( WEBRITI_TEMPLATE_DIR . '/css/cta_color.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/scripts/script.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/menu/default_menu_walker.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/menu/webriti_nav_walker.php');
require( WEBRITI_THEME_FUNCTIONS_PATH .'/font/font.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/pagination/webriti_pagination.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/breadcrumbs/breadcrumbs.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/template-tags.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/excerpt/excerpt.php');
require( WEBRITI_TEMPLATE_DIR . '/css/custom_light.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/widgets/sidebars.php');
require( WEBRITI_THEME_FUNCTIONS_PATH .'/widgets/wdl_header_topbar_info_widget.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/widgets/wdl_aboutpage_amenities_widget.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/widgets/wdl_team_widget.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/widgets/wdl_feature_page_widget.php');
require( WEBRITI_THEME_FUNCTIONS_PATH .'/widgets/wdl_featured_latest_news.php');
require( WEBRITI_THEME_FUNCTIONS_PATH .'/widgets/wdl_feature_post_standard_widget.php');
require( WEBRITI_THEME_FUNCTIONS_PATH .'/widgets/wdl_feature_post_quote_widget.php');
require( WEBRITI_THEME_FUNCTIONS_PATH .'/widgets/wdl_about_info_widget.php');
require( WEBRITI_THEME_FUNCTIONS_PATH .'/widgets/wdl_google_map_widget.php');
require( WEBRITI_THEME_FUNCTIONS_PATH .'/widgets/wdl_featured_callout.php');
require( WEBRITI_THEME_FUNCTIONS_PATH .'/widgets/wdl_contact_info_widget.php');
require( WEBRITI_THEME_FUNCTIONS_PATH .'/widgets/wdl_section_widget.php');
require( WEBRITI_THEME_FUNCTIONS_PATH .'/widgets/wdl_client_widget.php');
require( WEBRITI_THEME_FUNCTIONS_PATH .'/widgets/wdl_info_widget.php');


require( WEBRITI_THEME_FUNCTIONS_PATH . '/meta-box/metabox.php');

// Adding customizer files
require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_theme_style.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_home_page.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_layout_manager.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer-template.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_header_image.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_slider.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_general_settings.php');
require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_typography.php');


// set default content width
if ( ! isset( $content_width ) ) {
	$content_width = 696;
}
// Webriti theme title
if( !function_exists( 'graphite_head_title' ) ) 
{
	function graphite_head_title( $title , $sep ) {
		global $paged, $page;
		
		if ( is_feed() )
				return $title;
			
		// Add the site name
		$title .= get_bloginfo( 'name' );
		
		// Add the site description for the home / front page
		$site_description = get_bloginfo( 'description' );
		if ( $site_description && ( is_home() || is_front_page() ) )
				$title = "$title $sep $site_description";
			
		// Add a page number if necessary.
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
				$title = "$title $sep " . sprintf( __('Page', 'graphite' ), max( $paged, $page ) );
			
		return $title;
	}
}
add_filter( 'wp_title', 'graphite_head_title', 10, 2);


if ( ! function_exists( 'graphite_theme_setup' ) ) :

function graphite_theme_setup() {
	
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 */
	
	load_theme_textdomain( 'graphite', get_template_directory() . '/lang' );

	
	
	
	// Add default posts and comments RSS feed links to head.
	
	add_theme_support( 'automatic-feed-links' );

	
	
	/*
	 * Let WordPress manage the document title.
	 */
	 
	add_theme_support( 'title-tag' );
	
	// post format support
		add_theme_support( 'post-formats', array('aside', 'quote', 'status', 'video','gallery','audio') );
	
	
	
	
	// supports featured image
	
	add_theme_support( 'post-thumbnails' );

	
	
	// This theme uses wp_nav_menu() in two locations.
	
	register_nav_menus( array(
	
		'primary' => __( 'Primary Menu', 'graphite' ),
		
	) );
	
	
	// woocommerce support
	
	add_theme_support( 'woocommerce' );
	
	
	
	//Custom logo
	
	add_theme_support( 'custom-logo', array(
		'height'      => 34,
		'width'       => 282,
		'flex-height' => true,
		'header-text' => array( 'site-title', 'site-description' ),
		
	) );
	
	// Custom-header
	add_theme_support( 'custom-header', apply_filters( 'grahite_custom_header_args', array(
				'default-text-color'     => '333',
				'width'                  => 1600,
				'height'                 => 200,
				'flex-height'            => true,
				'wp-head-callback'       => 'graphite_header_style',
			) ) );
	
	
}
endif; 
add_action( 'after_setup_theme', 'graphite_theme_setup' );


function change_logo_class($html)
{
	$html = str_replace('custom-logo-link', 'navbar-brand', $html);
	return $html;
}
add_filter('get_custom_logo','change_logo_class');


// custom background
function graphite_custom_background_function()
{
	$page_bg_image_url = get_theme_mod('predefined_back_image','bg-img1.png');
	if($page_bg_image_url!='')
	{
	echo '<style>body.boxed{ background-image:url("'.WEBRITI_TEMPLATE_DIR_URI.'/images/bg-pattern/'.$page_bg_image_url.'");}</style>';
	}
}
add_action('wp_head','graphite_custom_background_function',10,0);


function graphite_registers() {

	wp_enqueue_script( 'graphite_script', get_template_directory_uri() . '/js/graphite.js', array("jquery"), '20120206', true  );
}
add_action( 'customize_controls_enqueue_scripts', 'graphite_registers' );

add_filter('widget_text','do_shortcode');
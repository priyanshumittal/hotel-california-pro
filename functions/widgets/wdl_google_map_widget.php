<?php
add_action( 'widgets_init', 'graphite_google_map_widget' );
function graphite_google_map_widget() {
	register_widget( 'graphite_google_map_widget' );
}


class graphite_google_map_widget extends WP_Widget {
	 
	function __construct() {
		parent::__construct(
			'graphite_google_map_widget', // Base ID
			__('WBR : Google map widget','graphite'), // Widget Name
			array(
				'classname' => 'featured_google_map_widget',
				'description' => __('Featured google map widget.','graphite'),
			),
			array(
				'width' => 600,
			)
		);
		
	 }
		public function widget( $args, $instance ) { 
		
		$instance['title'] = (isset($instance['title'])?$instance['title']:'');
		$instance['google_map'] = (isset($instance['google_map'])?$instance['google_map']:'<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d20428.63799164696!2d75.8711684264145!3d25.159105917557035!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1450768158722" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>');
		$instance['fa_icon'] = (isset($instance['fa_icon'])?$instance['fa_icon']:'');
		$instance['company_name'] = (isset($instance['company_name'])?$instance['company_name']:'');
		$instance['company_add'] = (isset($instance['company_add'])?$instance['company_add']:'');
		
		echo $args['before_widget'];
		
		echo $args['before_title'] . $instance['title'] . $args['after_title'];
		?>
			<div class="google-map">			
					<?php echo $instance['google_map']; ?>	
			</div>
			<div class="media sm-address">
					
					<?php if(!empty($instance['fa_icon'])) { ?>
					<div class="sm-address-icon">
						<i class="fa <?php echo $instance['fa_icon']; ?>"></i>
					</div>
					<?php } ?>
					
					<div class="media-body">
						<address>
							<?php if(!empty($instance['company_name'])) 
							{ 
							echo $instance['company_name']; 
							} 
							?>
							<br /><abbr>
							<?php 
							if(!empty($instance['company_add'])) 
							{ 
								echo $instance['company_add'];  
							} 
							?>
							</abbr>
						</address>
					</div>
			</div>

	<?php
	echo $args['after_widget'];
	}
	         
	public function form( $instance ) {
		
		$instance['title'] = (isset($instance['title'])?$instance['title']:'');
		$instance['google_map'] = (isset($instance['google_map'])?$instance['google_map']:'<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d20428.63799164696!2d75.8711684264145!3d25.159105917557035!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1450768158722" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>');
		$instance['fa_icon'] = (isset($instance['fa_icon'])?$instance['fa_icon']:'');
		$instance['company_name'] = (isset($instance['company_name'])?$instance['company_name']:'');
		$instance['company_add'] = (isset($instance['company_add'])?$instance['company_add']:'');
	?>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title','graphite' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
	</p>
	
	<p>
	<label for="<?php echo $this->get_field_id( 'google_map' ); ?>"><?php _e( 'Google map embed URL','graphite' ); ?></label>
	<textarea rows="5" class="widefat" id="<?php echo $this->get_field_id( 'google_map' ); ?>" name="<?php echo $this->get_field_name( 'google_map' ); ?>"><?php if($instance['google_map']) echo $instance['google_map']; ?></textarea>
	</p>
	
	<p>
	<label for="<?php echo $this->get_field_id( 'fa_icon' ); ?>"><?php _e( 'Fontawesome icon','graphite'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id( 'fa_icon' ); ?>" name="<?php echo $this->get_field_name( 'fa_icon' ); ?>" type="text" value="<?php if($instance['fa_icon']) echo esc_attr( $instance['fa_icon'] ); ?>" />
	<span><?php _e('Link to get fa-icon ','graphite'); ?><a href="<?php echo 'http://fortawesome.github.io/Font-Awesome/icons/'; ?>" target="_blank" ><?php _e('fa-icon','graphite'); ?></a></span>
	</p>
	<p>
	<label for="<?php echo $this->get_field_id( 'company_name' ); ?>"><?php _e( 'Name','graphite' ); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id( 'company_name' ); ?>" name="<?php echo $this->get_field_name( 'company_name' ); ?>" type="text" value="<?php if($instance['company_name']) echo esc_attr($instance['company_name']); ?>" />
	</p>
	<p>
	<label for="<?php echo $this->get_field_id( 'company_add' ); ?>"><?php _e( 'Address','graphite' ); ?></label>
	<textarea rows="5" class="widefat" id="<?php echo $this->get_field_id( 'company_add' ); ?>" name="<?php echo $this->get_field_name( 'company_add' ); ?>"><?php if($instance['company_add']) echo esc_attr($instance['company_add']); ?></textarea>
	</p>
	
	<?php
    }
	     
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
	
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? $new_instance['title'] : '';
		$instance['fa_icon'] = ( ! empty( $new_instance['fa_icon'] ) ) ? strip_tags( $new_instance['fa_icon'] ) : '';
		$instance['google_map'] = ( ! empty( $new_instance['google_map'] ) ) ? $new_instance['google_map'] : '';
		$instance['company_name'] = ( ! empty( $new_instance['company_name'] ) ) ? $new_instance['company_name'] : '';
		$instance['company_add'] = ( ! empty( $new_instance['company_add'] ) ) ? $new_instance['company_add'] : '';
		
		return $instance;
	}
	}
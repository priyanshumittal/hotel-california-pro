jQuery(document).ready(function() {
	
	
	/* Move page slider item widgets in the cta left panel */
	wp.customize.section( 'sidebar-widgets-cta_sidebar_left' ).panel( 'cta_section' );
	wp.customize.section( 'sidebar-widgets-cta_sidebar_left' ).priority( '12' );
	
	/* Move page slider item widgets in the cta right panel */
	wp.customize.section( 'sidebar-widgets-cta_sidebar_right' ).panel( 'cta_section' );
	wp.customize.section( 'sidebar-widgets-cta_sidebar_right' ).priority( '13' );
	
	/* Move page slider item widgets in the cta panel */
	wp.customize.section( 'sidebar-widgets-cta_sidebar_center' ).panel( 'cta_section' );
	wp.customize.section( 'sidebar-widgets-cta_sidebar_center' ).priority( '14' );
	
	/* Move page service item widgets in the page service panel */
	wp.customize.section( 'sidebar-widgets-wbr_service_sidebar' ).panel( 'service_section' );
	wp.customize.section( 'sidebar-widgets-wbr_service_sidebar' ).priority( '100' );
	
	/* Move page slider item widgets in the page slider panel */
	wp.customize.section( 'sidebar-widgets-wdl_top_sidebar' ).panel( 'room_section' );
	wp.customize.section( 'sidebar-widgets-wdl_top_sidebar' ).priority( '11' );
	
	/* Move testimonial widgets in the testimonials panel */
	wp.customize.section( 'sidebar-widgets-wdl_middle_sidebar' ).panel( 'testimonial_section' );
	wp.customize.section( 'sidebar-widgets-wdl_middle_sidebar' ).priority( '21' );
	
	/* Move latest news widgets in the latest news panel */
	wp.customize.section( 'sidebar-widgets-wdl_bottom_left_sidebar' ).panel( 'latest_news_section' );
	wp.customize.section( 'sidebar-widgets-wdl_bottom_left_sidebar' ).priority( '31' );
	
	/* Move about info widgets in the about info panel */
	wp.customize.section( 'sidebar-widgets-wdl_bottom_center_sidebar' ).panel( 'about_info_section' );
	wp.customize.section( 'sidebar-widgets-wdl_bottom_center_sidebar' ).priority( '41' );
	
	/* Move google map widgets in the google map panel */
	wp.customize.section( 'sidebar-widgets-wdl_bottom_right_sidebar' ).panel( 'google_map_section' );
	wp.customize.section( 'sidebar-widgets-wdl_bottom_right_sidebar' ).priority( '51' );
	
	/* Move Gallery section */
	wp.customize.section( 'sidebar-widgets-wdl_gallery_section' ).panel( 'gallery_section' );
	wp.customize.section( 'sidebar-widgets-wdl_gallery_section' ).priority( '61' );
	
	/* Move additional one section */
	wp.customize.section( 'sidebar-widgets-wdl_additional_sidebar' ).panel( 'additional_panel' );
	wp.customize.section( 'sidebar-widgets-wdl_additional_sidebar' ).priority( '71' );
	
	
	/* Move footer call out widgets in the footer call out panel */
	wp.customize.section( 'sidebar-widgets-wdl_bottom_footer_sidebar' ).panel( 'call_action_section' );
	wp.customize.section( 'sidebar-widgets-wdl_bottom_footer_sidebar' ).priority( '71' );
	
	/* Move Client call out widgets in the client panel */
	wp.customize.section( 'sidebar-widgets-wdl_client_sidebar' ).panel( 'client_panel' );
	wp.customize.section( 'sidebar-widgets-wdl_client_sidebar' ).priority( '520' );
	
	/* Move Amanities widgets in the amanities panel */
	wp.customize.section( 'sidebar-widgets-about_page_amenities' ).panel( 'about_setting' );
	wp.customize.section( 'sidebar-widgets-about_page_amenities' ).priority( '521' );
	

	/* Move team widgets in the team panel */
	wp.customize.section( 'sidebar-widgets-about_team_staff' ).panel( 'about_setting' );
	wp.customize.section( 'sidebar-widgets-about_team_staff' ).priority( '522' );
	
	/* Move callout widgets in the callout panel */
	wp.customize.section( 'sidebar-widgets-wdl_about_page_bottom_footer_sidebar' ).panel( 'about_setting' );
	wp.customize.section( 'sidebar-widgets-wdl_about_page_bottom_footer_sidebar' ).priority( '523' );
	
	
	
	
});
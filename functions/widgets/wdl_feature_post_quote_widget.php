<?php

add_action( 'widgets_init','graphite_feature_post_quote_widget'); 
function graphite_feature_post_quote_widget() 
{ 
	return   register_widget( 'graphite_feature_post_quote_widget' );
}

class graphite_feature_post_quote_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'graphite_feature_post_quote_widget', // Base ID
			__('WBR : Testimonial widget', 'graphite'), // Name
			array( 
			'classname' => 'graphite_feature_post_quote_widget',
			'description' => __( 'The recent testimonial display on your site ', 'graphite' )
			) // Args
		);
	}

	public function widget( $args, $instance ) {
		$ids = array();
		
		$instance['client_cat'] = (isset($instance['client_cat'])?$instance['client_cat']:1);
		$instance['client_effect'] = (isset($instance['client_effect'])?$instance['client_effect']:'');
		$instance['client_speed'] = (isset($instance['client_speed'])?$instance['client_speed']:'');
		$instance['exclude_posts'] = (isset($instance['exclude_posts'])?$instance['exclude_posts']:'');
		$custom_class=(isset($instance['custom_class'])?$instance['custom_class']:'');
		if($instance['exclude_posts']!=null){
			$ids = explode(',',$instance['exclude_posts']);
		}
		
		if($custom_class !='')
		{
			$args['before_widget'] = str_replace('class="', 'class="'. $custom_class . ' ',$args['before_widget']);
				
		}
		
		echo $args['before_widget'];
		?>
		
			<?php
			$query_args = array( 'cat'  => $instance['client_cat'],'ignore_sticky_posts' => 1,
			'tax_query' => array( array( 'taxonomy' => 'post_format', 'field' => 'slug','terms' => array( 'post-format-quote' ) ) ),
			'post__not_in' => $ids
			);	
			$t=true;

			$the_query = new WP_Query($query_args);  ?>
			
			<?php if( $instance['client_cat'] != null ): ?>
			<!-- Testimonial -->
			<div class="carousel slide <?php if($instance['client_effect']=='carousel-fade'){ echo $instance['client_effect'];} ?>" data-ride="carousel" data-type="multi" data-interval="<?php echo (isset($instance['client_speed'])?$instance['client_speed']:3000); ?>" id="slider-<?php echo $args['widget_id'];  ?>" >
			<div class="row carousel-inner width-auto">
				<?php
				$i=0;
				if ( $the_query->have_posts() ) {
				while ( $the_query->have_posts() ) {
				$the_query->the_post();
				?>
					<div class="col-md-12 pull-left item <?php if($t==true){echo 'active';}$t=false; ?>">
						<div class="media testmonial-area">
							<?php
							$defalt_arg =array('class' => "img-responsive img-circle");
							if(has_post_thumbnail()){ ?>
							<div class="author-box">
							<?php the_post_thumbnail('', $defalt_arg); ?>
							</div>
							<?php } ?>
							<div class="media-body">
								<div class="description-box">
									<div class="author-description">
									<?php the_content(); ?>
									</div>									
								</div>
								<h4><?php the_title();?> <?php if(get_post_meta( get_the_ID(),'testimonial_designation', true) !=null): ?> -<span><?php echo get_post_meta( get_the_ID(),'testimonial_designation', true); ?></span> <?php endif; ?></h4>
							</div>
						</div>	
					</div>
					<?php } 
					}else{ ?>
						<div class="col-md-12 pull-left item active">
						<div class="media testmonial-area">
							<div class="media-body">
								<div class="description-box">
									<div class="author-description">
									<?php _e("This widget only pick those post`s having format as Quote.","graphite"); ?>
									</div>									
								</div>
							</div>
						</div>	
					</div>
				<?php 	}
					?>
				</div>
				
				<!-- Testimonial Pagination -->
				<div class="row">
					<div class="testi-pager">
						<ol class="carousel-indicators testi-pagi">
						<?php
						$i=0;
						if ( $the_query->have_posts() ) {
						while ( $the_query->have_posts() ) {
						$the_query->the_post();  ?>
						<li data-target="#slider-<?php echo $args['widget_id'];  ?>" data-slide-to="<?php echo $i;?>" class="<?php if($i==0){ echo 'active';} ?>"></li>
						<?php $i++; } }?>
						</ol>	
					</div>	
				</div>
			</div>
			<!-- /Testimonial -->
			<?php endif; ?>
		<?php
		echo $args['after_widget']; 	
	}

	public function form( $instance ) {

		$instance['client_cat'] = (isset($instance['client_cat'])?$instance['client_cat']:1);
		$instance['client_effect'] = (isset($instance['client_effect'])?$instance['client_effect']:'');
		$instance['client_speed'] = (isset($instance['client_speed'])?$instance['client_speed']:'');
		$instance['exclude_posts'] = (isset($instance['exclude_posts'])?$instance['exclude_posts']:'');
		$instance['custom_class'] = (isset($instance['custom_class'])? $instance['custom_class']: '');
		?>
		
		
		<p>
			<label for="<?php echo $this->get_field_id( 'exclude_posts' ); ?>"><?php _e( 'Exclude posts quote format # id like (1,2,3...etc)','graphite' ); ?></label> 
			<textarea rows="5" class="widefat" id="<?php echo $this->get_field_id( 'exclude_posts' ); ?>" name="<?php echo $this->get_field_name( 'exclude_posts' ); ?>"><?php if($instance['exclude_posts']) echo $instance['exclude_posts']; ?></textarea>
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'client_cat' ); ?>"><?php _e( 'Select clients category','graphite' ); ?></label><br/>
		<select id="<?php echo $this->get_field_id( 'client_cat' ); ?>" name="<?php echo $this->get_field_name( 'client_cat' ); ?>">
			<option value>--<?php _e('Select category','graphite'); ?>--</option>
			<?php 
				$options = array();
				$cats = get_categories($options);

                foreach ( $cats as $cat )
                {
                    printf('<option value="%s" %s>%s</option>', $cat->term_id, selected($instance['client_cat'], $cat->term_id, false), $cat->name);
                }
			?>
		</select>
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'client_effect' ); ?>"><?php _e( 'Animation','graphite' ); ?></label><br/> 
		<select id="<?php echo $this->get_field_id( 'client_effect' ); ?>" name="<?php echo $this->get_field_name( 'client_effect' ); ?>">
			<option value>--<?php _e('Select animation','graphite'); ?>--</option>
			<option value="slide" <?php echo ($instance['client_effect']=='slide'?'selected':''); ?>><?php _e('Slide','graphite'); ?></option>
			<option value="carousel-fade" <?php echo ($instance['client_effect']=='carousel-fade'?'selected':''); ?>><?php _e('Fade','graphite'); ?></option>
		</select>
	</p>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'client_speed' ); ?>"><?php _e( 'Speed','graphite' ); ?></label><br/> 
		<select id="<?php echo $this->get_field_id( 'client_speed' ); ?>" name="<?php echo $this->get_field_name( 'client_speed' ); ?>">
			<option value>--<?php _e('Slider speed','graphite');?>--</option>
			<option value="500" <?php echo ($instance['client_speed']==500?'selected':''); ?>><?php echo '500'; ?></option>
			<option value="1000" <?php echo ($instance['client_speed']==1000?'selected':''); ?>><?php echo '1000'; ?></option>
			<option value="1500" <?php echo ($instance['client_speed']==1500?'selected':''); ?>><?php echo '1500'; ?></option>
			<option value="2000" <?php echo ($instance['client_speed']==2000?'selected':''); ?>><?php echo '2000'; ?></option>
			<option value="2500" <?php echo ($instance['client_speed']==2500?'selected':''); ?>><?php echo '2500'; ?></option>
			<option value="3000" <?php echo ($instance['client_speed']==3000?'selected':''); ?>><?php echo '3000'; ?></option>
			<option value="3500" <?php echo ($instance['client_speed']==3500?'selected':''); ?>><?php echo '3500'; ?></option>
			<?php echo '500'; ?>		
		</select>
	</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'custom_class' ); ?>"><?php _e('CSS Classes (optional)','graphite' ); ?></label> 
		</p>
		<input class="widefat" id="<?php echo $this->get_field_id( 'custom_class' ); ?>" name="<?php echo $this->get_field_name( 'custom_class' ); ?>" type="text" value="<?php if($instance[ 'custom_class' ]) echo esc_attr($instance[ 'custom_class' ]);?>" />
		
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		
		$instance['client_cat'] = ( ! empty( $new_instance['client_cat'] ) ) ? strip_tags( $new_instance['client_cat'] ) : '';
		
		$instance['client_effect'] = ( ! empty( $new_instance['client_effect'] ) ) ? strip_tags( $new_instance['client_effect'] ) : '';
		
		$instance['client_speed'] = ( ! empty( $new_instance['client_speed'] ) ) ? strip_tags( $new_instance['client_speed'] ) : '';
		
		$instance['exclude_posts'] = ( ! empty( $new_instance['exclude_posts'] ) ) ?  $new_instance['exclude_posts'] : '';
		
		$instance['custom_class'] = ( ! empty( $new_instance['custom_class'] ) ) ? $new_instance['custom_class'] : '';
		
		return $instance;
	}

} // class
?>
<!-- Callout Section -->
		<?php 
			if( is_active_sidebar('graphite_room_page_footer_sidebar') ) :
				echo '<section class="callout-section"><div class="container"><div class="row">';
				dynamic_sidebar( 'graphite_room_page_footer_sidebar' );
				echo '</div></div></section>';
			endif;
		?>
<!-- /Callout Section -->
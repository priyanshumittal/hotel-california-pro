<?php
function graphite_about_template_customizer( $wp_customize ) {

//About Page Template panel 
	$wp_customize->add_panel( 'about_setting', array(
		'priority'       => 920,
		'capability'     => 'edit_theme_options',
		'title'      => __('About Us page settings','graphite'),
	) );
	
	// add section to manage About
	$wp_customize->add_section(
        'about_section_settings',
        array(
            'title' => __('About Us page settings','graphite'),
			'panel'  => 'about_setting',
			'priority'   => 100,
			
			)
    );
	
	// enable/disable Amanities Section
	$wp_customize->add_setting(
		'amenities_section_enable',
		array('capability'  => 'edit_theme_options',
		'default' => false ,
		
		));

	$wp_customize->add_control(
		'amenities_section_enable',
		array(
			'type' => 'checkbox',
			'label' => __('Hide amenities section','graphite'),
			'section' => 'about_section_settings',
		)
	);
	
	
	// Number of Column layout
	$wp_customize->add_setting('amanities_column_layout',array(
	'default' => 4,
	'sanitize_callback' => 'sanitize_text_field',
	) );

	$wp_customize->add_control('amanities_column_layout',array(
	'type' => 'select',
	'label' => __('Select column layout','graphite'),
	'section' => 'about_section_settings',
	'choices' => array(1=>1,2=>2,3=>3,4=>4),
	) );
	
	
	// Amenities backgroud image for section
	$wp_customize->add_setting('amenities_background_image', array(
      'sanitize_callback' => 'esc_url_raw',
	   'capability'     => 'edit_theme_options', 
    ) );
    
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'amenities_background_image', array(
      'label'    => __( 'Amenities backgroud image', 'graphite' ),
      'section'  => 'about_section_settings',
    ) ) );
	
	 //Amenities Title
	 $wp_customize->add_setting(
    'amenities_title',
    array(
        'default' => __('Graphite Amenities','graphite'),
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'graphite_template_page_sanitize_text',
		)
	);	
	$wp_customize->add_control( 'amenities_title',array(
    'label'   => __('Title','graphite'),
    'section' => 'about_section_settings',
	 'type' => 'text',)  );	
	 
	  //Amenities Description
	 $wp_customize->add_setting(
    'amenities_des',
    array(
        'default' => 'Sea summo mazim ex, ea errem eleifend definitionem vim. Ut nec hinc dolor possim mei ludus efficiendi ei sea summo mazim ex.',
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'graphite_template_page_sanitize_text',
		)
	);	
	$wp_customize->add_control( 'amenities_des',array(
    'label'   => __('Description','graphite'),
    'section' => 'about_section_settings',
	 'type' => 'textarea',)  );	
	 
	 
	 class WP_amenities_Customize_Control extends WP_Customize_Control {
    public $type = 'new_menu';
    /**
    * Render the control's content.
    */
    public function render_content() {
    ?>
    <a href="<?php bloginfo ( 'url' );?>/wp-admin/widgets.php" class="button"  target="_blank"><?php _e( 'Click here to add amenities', 'graphite' ); ?></a>
    <?php
    }
}
$wp_customize->add_setting(
    'amenities',
    array(
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
    )	
);
$wp_customize->add_control( new WP_amenities_Customize_Control( $wp_customize, 'amenities', array(	
			'section' => 'about_section_settings',
    ))
);
	
	
	
	 // enable/disable Team Section
	$wp_customize->add_setting(
		'team_section_enable',
		array('capability'  => 'edit_theme_options',
		'default' => false ,
		
		));

	$wp_customize->add_control(
		'team_section_enable',
		array(
			'type' => 'checkbox',
			'label' => __('Hide team section from About Us page','graphite'),
			'section' => 'about_section_settings',
		)
	);
	
	
	// Number of Column layout
	$wp_customize->add_setting('team_column_layout',array(
	'default' => 4,
	'sanitize_callback' => 'sanitize_text_field',
	) );

	$wp_customize->add_control('team_column_layout',array(
	'type' => 'select',
	'label' => __('Select column layout','graphite'),
	'section' => 'about_section_settings',
	'choices' => array(1=>1,2=>2,3=>3,4=>4),
	) );
	
	  
	 //Team Title  
	 $wp_customize->add_setting(
    'team_title',
    array(
        'default' => __('Our Graphite Staff','graphite'),
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'graphite_template_page_sanitize_text',
		)
	);	
	
	$wp_customize->add_control( 'team_title',array(
    'label'   => __('Title','graphite'),
    'section' => 'about_section_settings',
	 'type' => 'text',)  );	
	 
	 
	 
	 
	 //Team Description
	 
	 $wp_customize->add_setting(
    'team_dec',
    array(
        'default' => 'Sea summo mazim ex, ea errem eleifend definitionem vim. Ut nec hinc dolor possim mei ludus efficiendi ei sea summo mazim ex.',
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'graphite_template_page_sanitize_text',
		)
	);	
	$wp_customize->add_control( 'team_dec',array(
    'label'   => __('Description','graphite'),
    'section' => 'about_section_settings',
	 'type' => 'textarea',)  );	
	 
	 // Add Team link
	 
	class WP_team_Customize_Control extends WP_Customize_Control {
    public $type = 'new_menu';
    /**
    * Render the control's content.
    */
    public function render_content() {
    ?>
    <a href="<?php bloginfo ( 'url' );?>/wp-admin/widgets.php" class="button"  target="_blank"><?php _e( 'Click here to add team member', 'graphite' ); ?></a>
    <?php
    }
}
$wp_customize->add_setting(
    'team',
    array(
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
    )	
);
$wp_customize->add_control( new WP_team_Customize_Control( $wp_customize, 'team', array(	
			'section' => 'about_section_settings',
    ))
);
	 
	 // enable/disable Footer Callout Section
	$wp_customize->add_setting(
		'footer_callout_enable',
		array('capability'  => 'edit_theme_options',
		'default' => false ,
		));

	$wp_customize->add_control(
		'footer_callout_enable',
		array(
			'type' => 'checkbox',
			'label' => __('Hide footer callout section from About Us page','graphite'),
			'section' => 'about_section_settings',
		)
	);
	
	$wp_customize->add_panel( 'contact_setting', array(
		'priority'       => 940,
		'capability'     => 'edit_theme_options',
		'title'      => __('Contact page setting','graphite'),
	) );
	
	// contact page section
	$wp_customize->add_section('contact_page_section',array(
	'title'=>'Contact page settings',
	'description'=>'',
	'panel'  => 'contact_setting',
	));
	
	
	// contact form title
	$wp_customize->add_setting('contact_form_title',array(
	'capability'  => 'edit_theme_options',
	'default' => __('Get in touch with us','graphite'),
	'sanitize_callback' => 'graphite_template_page_sanitize_text',
	));
	$wp_customize->add_control('contact_form_title',array(
	'label' => __('Contact form title','graphite'),
	'section' => 'contact_page_section',
	'type' => 'text',
	));
	
	// contact form enable / disable
	$wp_customize->add_setting('contact_form_enable',array(
	'default'=>true,
	'capability'  => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	));
	$wp_customize->add_control('contact_form_enable',array(
	'label' => __('Contact form enable','graphite'),
	'section' => 'contact_page_section',
	'type' => 'checkbox',
	));
	
	// google map title
	$wp_customize->add_setting('google_map_title',array(
	'capability'  => 'edit_theme_options',
	'default' => __('View us on the map','graphite'),
	'sanitize_callback' => 'graphite_template_page_sanitize_text',
	));
	$wp_customize->add_control('google_map_title',array(
	'label' => __('Contact Google Map title','graphite'),
	'section' => 'contact_page_section',
	'type' => 'text',
	));
	
	// google map url
	$wp_customize->add_setting('contact_google_map_shortcode',array(
	'capability'  => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	));
	$wp_customize->add_control('contact_google_map_shortcode',array(
	'label' => __('Google Map Shortcode','graphite'),
	'section' => 'contact_page_section',
	'type' => 'textarea',
	));
	
	// google map enable / disable
	$wp_customize->add_setting('google_map_enable',array(
	'default'=>true,
	'capability'  => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	));
	$wp_customize->add_control('google_map_enable',array(
	'label' => __('Enable Google map','graphite'),
	'section' => 'contact_page_section',
	'type' => 'checkbox',
	));
	
	// contact info enable / disable
	$wp_customize->add_setting('contact_info_enable',array(
	'default'=>true,
	'capability'  => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	));
	$wp_customize->add_control('contact_info_enable',array(
	'label' => __('Enable contact info','graphite'),
	'section' => 'contact_page_section',
	'type' => 'checkbox',
	));
	
	
	//Blog Page Panel
	$wp_customize->add_panel( 'blog_setting', array(
		'priority'       => 930,
		'capability'     => 'edit_theme_options',
		'title'      => __('Blog page settings', 'graphite'),
	) );
	
	// add section to manage About
	$wp_customize->add_section(
        'blog_page_setting',
        array(
            'title' => __('Blog page settings','graphite'),
            'panel'  => 'blog_setting',
			'priority'   => 100,
			
			)
    );
	
	// enable / disable meta section 
	$wp_customize->add_setting(
		'blog_meta_section_enable',
		array('capability'  => 'edit_theme_options',
		'default' => false,
		
		));
	$wp_customize->add_control(
		'blog_meta_section_enable',
		array(
			'type' => 'checkbox',
			'label' => __('Hide post meta values like author name, date , comment etc.','graphite'),
			'section' => 'blog_page_setting',
		)
	);
	
	// enable / disable testimonial post on blog page
	$wp_customize->add_setting(
		'blog_page_testimonial_enable',
		array('capability'  => 'edit_theme_options',
		'default' => true,
		
		));
	$wp_customize->add_control(
		'blog_page_testimonial_enable',
		array(
			'type' => 'checkbox',
			'label' => __('Hide Testimonial widget post from blog page','graphite'),
			'section' => 'blog_page_setting',
		)
	);
	
	// enable / disable room post on blog page
	$wp_customize->add_setting(
		'blog_page_room_enable',
		array('capability'  => 'edit_theme_options',
		'default' => true,
		));
	$wp_customize->add_control(
		'blog_page_room_enable',
		array(
			'type' => 'checkbox',
			'label' => __('Hide homepage rooms widget post from blog page','graphite'),
			'section' => 'blog_page_setting',
		)
	);
	
	// enable / disable blog title position
	$wp_customize->add_setting(
		'blog_title_position_enable',
		array('capability'  => 'edit_theme_options',
		'default' => false,
		));
	$wp_customize->add_control(
		'blog_title_position_enable',
		array(
			'type' => 'checkbox',
			'label' => __('Enable blog title before blog featured image','graphite'),
			'section' => 'blog_page_setting',
		)
	);
	

	//Breadcrumbs Section
	$wp_customize->add_panel( 'breadcrumb_section_setting', array(
		'priority'       => 930,
		'capability'     => 'edit_theme_options',
		'title'      => __('Archive page settings', 'graphite'),
	) );
	
	$wp_customize->add_section(
        'breadcrumbs_setting',
        array(
            'title' => __('Archive page title','graphite'),
            'description' =>'',
			'panel'  => 'breadcrumb_section_setting',
			)
    );

	$wp_customize->add_setting(
    'archive_prefix',
    array(
        'default' => __('Archive','graphite'),
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'graphite_template_page_sanitize_text',
		)
	);	
	$wp_customize->add_control( 'archive_prefix',array(
    'label'   => __('Archive','graphite'),
    'section' => 'breadcrumbs_setting',
	 'type' => 'text'
	));	
	
	$wp_customize->add_setting(
    'category_prefix',
    array(
        'default' => __('Category','graphite'),
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'graphite_template_page_sanitize_text',
		)
	);	
	$wp_customize->add_control( 'category_prefix',array(
    'label'   => __('Category','graphite'),
    'section' => 'breadcrumbs_setting',
	 'type' => 'text'
	));

	$wp_customize->add_setting(
    'author_prefix',
    array(
        'default' => __('All posts by','graphite'),
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'graphite_template_page_sanitize_text',
		)
	);	
	$wp_customize->add_control( 'author_prefix',array(
    'label'   => __('Author','graphite'),
    'section' => 'breadcrumbs_setting',
	 'type' => 'text'
	));
	
	$wp_customize->add_setting(
    'tag_prefix',
    array(
        'default' => __('Tag','graphite'),
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'graphite_template_page_sanitize_text',
		)
	);	
	$wp_customize->add_control( 'tag_prefix',array(
    'label'   => __('Tag','graphite'),
    'section' => 'breadcrumbs_setting',
	 'type' => 'text'
	));
	
	
	$wp_customize->add_setting(
    'search_prefix',
    array(
        'default' => __('Search results for','graphite'),
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'graphite_template_page_sanitize_text',
		)
	);	
	$wp_customize->add_control( 'search_prefix',array(
    'label'   => __('Search','graphite'),
    'section' => 'breadcrumbs_setting',
	 'type' => 'text'
	));
	
	$wp_customize->add_setting(
    '404_prefix',
    array(
        'default' => __('404','graphite'),
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'graphite_template_page_sanitize_text',
		)
	);	
	$wp_customize->add_control( '404_prefix',array(
    'label'   => __('404','graphite'),
    'section' => 'breadcrumbs_setting',
	 'type' => 'text'
	));
	
	
	$wp_customize->add_setting(
    'shop_prefix',
    array(
        'default' => __('Shop','graphite'),
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'graphite_template_page_sanitize_text',
		)
	);	
	$wp_customize->add_control( 'shop_prefix',array(
    'label'   => __('Shop','graphite'),
    'section' => 'breadcrumbs_setting',
	 'type' => 'text'
	));
	 
	
}
add_action( 'customize_register', 'graphite_about_template_customizer' );

function graphite_template_page_sanitize_text( $input ) {

			return wp_kses_post( force_balance_tags( $input ) );

			}
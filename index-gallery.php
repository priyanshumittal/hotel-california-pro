<?php 
$gallery_section_enable= get_theme_mod('gallery_section_enable',false); 
if( $gallery_section_enable == false )
{
	if( 
		
		get_theme_mod('home_gallery_title') != '' || 
		
		get_theme_mod('home_gallery_discription') != '' ||
		
		is_active_sidebar( 'graphite_gallery_section' )
		
		):
?>
<!-- Rooms Section -->
<section class="wbr-section" id="gallery">		
	<!--Gallery Section-->
	<section class="gallery-section">
	
		<?php if( get_theme_mod('home_gallery_title') != '' || get_theme_mod('home_gallery_discription') != '' ) { ?>
		<!-- Section Title -->
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h1 class="wow fadeInUp animated animated" data-wow-duration="500ms" data-wow-delay="0ms"><?php echo get_theme_mod('home_gallery_title');?></h1>
						<div class="separator"><span></span></div>
						<p class="wow fadeInDown animated"><?php echo get_theme_mod('home_gallery_discription'); ?></p>
					</div>
				</div>
			</div>
		</div>	
		<!-- /Section Title -->
		<?php } ?>
		

		<?php  if ( is_active_sidebar( 'graphite_gallery_section' ) ) :
				dynamic_sidebar('graphite_gallery_section' );
		endif; ?>
		
	</section>
</section>	
<!-- /Rooms Section -->
<div class="clearfix"></div>
<?php 
	endif;
} 
?>
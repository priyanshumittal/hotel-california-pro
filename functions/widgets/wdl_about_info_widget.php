<?php
add_action( 'widgets_init','graphite_about_info_widget'); 
function graphite_about_info_widget() 
{ 
	return   register_widget( 'graphite_about_info_widget' );
}

class graphite_about_info_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'graphite_about_info_widget', // Base ID
			__('WBR : About Info Widget', 'graphite'), // Name
			array( 'description' => __('About information widget section','graphite' ), ) // Args
		);
	}
	
	public function admin_setup() {
		wp_enqueue_media();
		wp_enqueue_script( 'tribe-image-widget', plugins_url('resources/js/image-widget.js', __FILE__), array( 'jquery', 'media-upload', 'media-views' ), self::VERSION );

		wp_localize_script( 'tribe-image-widget', 'TribeImageWidget', array(
			'frame_title' => __( 'Select an image', 'graphite' ),
			'button_title' => __( 'Insert into widget', 'graphite' ),
		) );
	}

	public function widget( $args , $instance ) {
		echo $args['before_widget'];
		
		$instance['title'] = (isset($instance['title'])?$instance['title']:'');
		$instance['description'] = (isset($instance['description'])?$instance['description']:'');
		$instance['more_text'] = (isset($instance['more_text'])?$instance['more_text']:'');
		$instance['more_url'] = (isset($instance['more_url'])?$instance['more_url']:'');
		$instance['morebtn_open'] = (isset($instance['morebtn_open'])?$instance['morebtn_open']:false);
		$instance['about_infoimage'] = (isset($instance['about_infoimage'])?$instance['about_infoimage']:'');
		
		echo $args['before_title'] . $instance['title'] . $args['after_title'];
		
		echo ($instance['about_infoimage']!=null?'<div class="sm-about-area"><img class="img-responsive" alt="img" src="'.$instance['about_infoimage'].'"></div>':'');
		
		echo ( $instance['description']!=null ? '<p>'.$instance['description'].'</p>' : '' ); 
		echo ( $instance['more_text']!=null ? '<div class="about-btn"><a href="'.$instance['more_url'].'" '.($instance['morebtn_open']==true?'target="_blank"':'').'>'.$instance['more_text'].'</a></div>' : '' );
		$custom_class=(isset($instance['custom_class'])?$instance['custom_class']:'');
		
		if($custom_class !='')
		{
			$args['before_widget'] = str_replace('class="', 'class="'. $custom_class . ' ',$args['before_widget']);
				
		}
		
		echo $args['after_widget']; 	
	}

	public function form( $instance ) {
		
		$instance['title'] = (isset($instance['title'])?$instance['title']:'');
		$instance['description'] = (isset($instance['description'])?$instance['description']:'');
		$instance['more_text'] = (isset($instance['more_text'])?$instance['more_text']:'');
		$instance['more_url'] = (isset($instance['more_url'])?$instance['more_url']:'');
		$instance['morebtn_open'] = (isset($instance['morebtn_open'])?$instance['morebtn_open']:false);
		$instance['about_infoimage'] = (isset($instance['about_infoimage'])?$instance['about_infoimage']:'');
		$instance['custom_class'] = (isset($instance['custom_class'])? $instance['custom_class']: '');
		?>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title','graphite' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e('Description','graphite' ); ?></label> 
		<textarea rows="5" class="widefat" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>"><?php echo esc_attr( $instance['description'] ); ?></textarea>
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'more_text' ); ?>"><?php _e('Button Text','graphite' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'more_text' ); ?>" name="<?php echo $this->get_field_name( 'more_text' ); ?>" type="text" value="<?php echo esc_attr( $instance['more_text'] ); ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'more_url' ); ?>"><?php _e('Button Link','graphite' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'more_url' ); ?>" name="<?php echo $this->get_field_name( 'more_url' ); ?>" type="text" value="<?php echo esc_attr( $instance['more_url'] ); ?>" />
		</p>
		
		<p>
		<input type="checkbox" id="<?php echo $this->get_field_id( 'morebtn_open' ); ?>" name="<?php echo $this->get_field_name( 'morebtn_open' ); ?>" value="1" <?php if($instance['morebtn_open']==true){ echo 'checked'; } ?>>
		<label for="<?php echo $this->get_field_id( 'morebtn_open' ); ?>"><?php _e('Open link in new tab','graphite' ); ?></label> 
		</p>
		
		<p>
		<input type="text" class="widefat custom_media_url_team"
                   name="<?php echo $this->get_field_name('about_infoimage'); ?>" id="<?php echo $this->get_field_id('about_infoimage'); ?>" value="<?php if( !empty($instance['about_infoimage']) ): echo $instance['about_infoimage']; endif; ?>"
                   style="margin-top:5px;">

        <input type="button" class="button button-primary custom_media_button_team" id="custom_media_button_team"
                   name="<?php echo $this->get_field_name('about_infoimage'); ?>" value="<?php _e('Upload image','graphite'); ?>"
                   style="margin-top:5px;"/>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'custom_class' ); ?>"><?php _e('CSS Classes (optional)','graphite' ); ?></label> 
		</p>
		<input class="widefat" id="<?php echo $this->get_field_id( 'custom_class' ); ?>" name="<?php echo $this->get_field_name( 'custom_class' ); ?>" type="text" value="<?php if($instance[ 'custom_class' ]) echo esc_attr($instance[ 'custom_class' ]);?>" />
		
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? $new_instance['title'] : '';
		$instance['description'] = ( ! empty( $new_instance['description'] ) ) ? $new_instance['description'] : '';
		
		$instance['more_text'] = ( ! empty( $new_instance['more_text'] ) ) ? strip_tags($new_instance['more_text']) : '';
		$instance['more_url'] = ( ! empty( $new_instance['more_url'] ) ) ? strip_tags($new_instance['more_url']) : '';
		$instance['morebtn_open'] = ( ! empty( $new_instance['morebtn_open'] ) ) ? strip_tags($new_instance['morebtn_open']) : '';
		$instance['about_infoimage'] = ( ! empty( $new_instance['about_infoimage'] ) ) ? $new_instance['about_infoimage'] : '';
		$instance['custom_class'] = ( ! empty( $new_instance['custom_class'] ) ) ? $new_instance['custom_class'] : '';
		
		return $instance;
	}

} // class
?>
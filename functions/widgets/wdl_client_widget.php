<?php
// Register and load the widget
function graphite_client_widget() {
    register_widget( 'graphite_client_widget' );
}
add_action( 'widgets_init', 'graphite_client_widget' );

function load_custom_wp_admin_style($hook) {
	wp_enqueue_script('media-upload');
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

// Creating the widget
	class graphite_client_widget extends WP_Widget {
	 
	function __construct() {
		parent::__construct(
			'graphite_client_widget', // Base ID
			__('WBR: Client widget','graphite'), // Widget Name
			array(
				'classname' => 'graphite_client_widget',
				'description' => __('About page team area','graphite'),
			),
			array(
				'width' => 600,
			)
		);
		
		
	} 
	 
	public function widget( $args, $instance ) {	
		echo $args['before_widget'];
		$instance['client_list'] = (isset($instance['client_list'])? $instance['client_list']: '');
		$instance['column'] = (isset($instance['column']) && $instance['column'] != null ? $instance['column']: 6);
		$instance['client_effect'] = (isset($instance['client_effect'])? $instance['client_effect']: '');
		$instance['total_client'] = (isset($instance['total_client'])? $instance['total_client']: '');
		$instance['target'] = (isset($instance['target'])? $instance['target']: '');
		
		if($instance['column']==1)
		{
			$clientcol = 12 / $instance['column'];
			$clientcol = 'col-md-'.$clientcol.' col-sm-12 col-xs-12';
		}
		elseif($instance['column']==5)
		{
			$clientcol = 'col-5 col-sm-6 col-xs-12';
		}
		else{
			$clientcol = 12 / $instance['column'];
			$clientcol = 'col-md-'.$clientcol.' col-sm-6 col-xs-12';
		}
		
		if($instance['total_client']){
			$total_client = $instance['total_client'];
		} else { $total_client = 0;
		}
		?>
		<div class="clients row">
									
			<div id="clients" class="carousel slide <?php if($instance['client_effect'] == 'fade') { echo 'carousel-fade'; } ?>" data-ride="carousel">
				
				<div class="carousel-inner">	<!-- <<-- added here for 5 column class - five-column -->
					
					<?php
					if($instance['client_list'])
					{
					$i=1;
					foreach($instance['client_list'] as $client_list)
					{
					?>
					<div class="item <?php if($i==1){ echo 'active'; }?>" >
					
						<div class="<?php echo $clientcol; ?>">
						<?php if(!empty($client_list['client_url'])) { ?>
						<a href="<?php echo $client_list['client_url']; ?>" <?php if(!empty($instance['target'])){ ?> target="_blank" <?php } ?>>
						<?php } ?>
						<img <?php if(!empty($client_list['client_title'])) { ?> title="<?php  echo $client_list['client_title']; ?>" <?php } ?> src="<?php echo $client_list['client_image_url']; ?>" <?php if(!empty($client_list['client_title'])) { ?> alt="<?php echo $client_list['client_title'];?>" <?php } ?> />
						<?php if(!empty($client_list['client_url'])) { ?>
						</a>
						<?php } ?>
					  </div>
						
					</div>
					<?php $i=$i+1;} } ?>
					
					
				</div>
				<?php ?>
				<div class="row">
					<div class="testi-pager">
						<ol class="carousel-indicators testi-pagi">
							<?php
							if($instance['client_list'])
							{
							$i=0;
							foreach($instance['client_list'] as $client_list)
							{
							?>
							<li data-target="#clients" data-slide-to="<?php echo $i; ?>" <?php if($i==0){ echo 'class="active"'; } ?>></li>
							<?php $i=$i+1;} } ?>
						</ol>
					</div>	
				</div>
				
			</div>
				

		</div>
				
<?php
if($instance['column'])
{
	$col = 12 / $instance['column'];
}

if($instance['column'] == 6)
{
	$clone=4;
	
}	
elseif($instance['column'] == 5)
{
	$clone=3;
	
}
elseif($instance['column'] == 4)
{
	$clone=2;
	
}
elseif($instance['column'] == 3)
{
	$clone=1;
	
}

elseif($instance['column'] == 2)
{
	$clone=0;
	
}
else
{
$clone='';
}
?>		
<script>
jQuery(function() {

	jQuery('#clients .item').each(function(){
			
	  var next = jQuery(this).next();
	  if (!next.length) {
		next = jQuery(this).siblings(':first');
	  }
	  next.children(':first-child').clone().appendTo(jQuery(this));
	  
	  // For Two Column Layout i=0
	  // For Three Column Layout i=1
	  // For four Column Layout i=2
	  // For five Column Layout i=3
	  // For six Column Layout i=4
	  for (var i=0;i< <?php echo $clone; ?>;i++) {
		next=next.next();
		if (!next.length) {
			next = jQuery(this).siblings(':first');
		}
		
		next.children(':first-child').clone().appendTo(jQuery(this));
	  }
		  
	});
					
});	
</script>
		<?php
		echo $args['after_widget'];
	}
	         
	// Widget Backend
	public function form( $instance ) {

		$instance['column'] = (isset($instance['column'])? $instance['column']: '');
		$instance['client_effect'] = (isset($instance['client_effect'])? $instance['client_effect']: '');
		$instance['client_list'] = (isset($instance['client_list'])? $instance['client_list']: '');
		$instance['total_client'] = (isset($instance['total_client'])? $instance['total_client']: '');
		$instance['target'] = (isset($instance['target'])? $instance['target']: '');
	
		if($instance['total_client']){
			$total_client = $instance['total_client'];
		} else { $total_client = 0; }
		
		
		?>
		<script type="text/javascript">
			function addInput(button) {
				
				var parentDiv = jQuery( button ).parents(".widget-content");
				var totalClient = parentDiv.find('.total_client');
				var removebutton = parentDiv.find('#remove_button2');
				
				var client = totalClient.val();
				client++;
				
				
				parentDiv.find('#webriti_client').append('<div id="client-client-'+client+'" class="section" ><p><label>Title </label><input class="widefat" type="text" value="" id="client_title_'+client+'" name="client_title_'+client+'" size="36"/></p><p><label>URL </label><input class="widefat" type="text" value="" id="client_url_'+client+'" name="client_url_'+client+'"/></p><p><label>Image </label><input class="widefat" type="text" value="" id="client_image_'+client+'" name="client_image_'+client+'"/><input type="button" id="upload_image_button_'+client+'" value="Upload Image" class="button button-primary upload_image_button_'+client+'" onClick="webriti_client(this)" /></div>');

				
				removebutton.show();
				totalClient.val(client);
			}
			
			function remove_field(button)
			{
				var parentDiv = jQuery( button ).parents(".widget-content");
				var totalClient = parentDiv.find('.total_client');
				
				var client = totalClient.val();
				if(client){
					parentDiv.find("#client-client-"+client).remove();
					client = client-1;
					totalClient.val(client);
				} 
			}
			
			function webriti_client(button)
			{
				// media upload js
				var uploadID = '';
				var uploadID = jQuery( button ).prev("input");
					uploadID.val('');
					
					//console.log(uploadID);
					formfield = jQuery('.upload').attr('name');
					tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
					
					window.send_to_editor = function(html)
					{
						imgurl = jQuery('img',html).attr('src');
						uploadID.val(imgurl); /*assign the value to the input*/
						tb_remove();
					};		
					return false;
			}
		</script>
		
		<div id="webriti_client">
		<?php if($instance['client_list'])
			{
				$i=1;
				foreach($instance['client_list'] as $client_list)
				{
			
				?>
					<div class="section" id="client-client-<?php echo $i ?>">
						
						<p><label><?php _e('Title','graphite');?></label>
						<input class="widefat" type="text" value="<?php if(!empty($client_list['client_title'])) { echo $client_list['client_title']; } ?>" id="client_title_<?php echo $i ?>" name="client_title_<?php echo $i ?>"/>
						</p>
						<p>
						<label><?php _e('URL','graphite');?></label>
						<input class="widefat" type="text" value="<?php echo $client_list['client_url']; ?>" id="client_url_<?php echo $i ?>" name="client_url_<?php echo $i ?>" />
						</p>

						<p>
						<label><?php _e('Image','graphite');?></label>
						<input class="widefat" type="text" value="<?php echo $client_list['client_image_url']; ?>" id="client_image_<?php echo $i ?>" name="client_image_<?php echo $i ?>">
						<input type="button" id="upload_image_button_<?php echo $i; ?>" value="Upload Image" class="button button-primary upload_image_button_<?php echo $i; ?>" onClick="webriti_client(this)"  />
						</p>
						
						<?php if($client_list['client_image_url']!=''){ ?>
						<p>
						<img src="<?php echo $client_list['client_image_url']; ?>" style="height:150px; width:250px;">
						</p>
						<?php } ?>
					
					</div>	
				<?php	
					
					$i=$i+1;
				}	
		} ?>
		</div>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'total_client' ); ?>"></label> 
		<input type="hidden" class="total_client" id="<?php echo $this->get_field_id( 'total_client' ); ?>" name="<?php echo $this->get_field_name( 'total_client' ); ?>" value="<?php echo $total_client; ?>"/>
		</p>
		
		<div class="section" style="margin-bottom:30px;">
			<a onclick="addInput(this)" class="button button-primary" name="add" id="add_client"><?php _e('Add Client','graphite');?></a>
			<a onclick="remove_field(this)" class="button button-default"  id="remove_button2" style="display:<?php if(!$total_client) { ?>none<?php } ?>;"><?php _e('Remove Last Client','graphite'); ?></a>		
		</div>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'target' ); ?>"><?php _e('Open link in new tab','graphite' ); ?></label> 
		<input type="checkbox" id="<?php echo $this->get_field_id( 'target' ); ?>" name="<?php echo $this->get_field_name( 'target' ); ?>" value="1" <?php if( $instance['target']==1){ echo "checked='checked'";} ?> />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'column' ); ?>"><?php _e('Select column layout','graphite' ); ?></label> 
			
			<select id="<?php echo $this->get_field_id( 'column' ); ?>" name="<?php echo $this->get_field_name( 'column' ); ?>">
			<option value>--<?php echo __('Select column layout','graphite'); ?>--</option>
				<option value="1" <?php echo ($instance['column']=='1'?'selected':''); ?>>
				<?php echo '1'; ?></option>
				<option value="2" <?php echo ($instance['column']=='2'?'selected':''); ?>>
				<?php echo '2'; ?></option>
				<option value="3" <?php echo ($instance['column']=='3'?'selected':''); ?>>
				<?php echo '3'; ?></option>
				<option value="4" <?php echo ($instance['column']=='4'?'selected':''); ?>>
				<?php echo '4'; ?></option>
				<option value="5" <?php echo ($instance['column']=='5'?'selected':''); ?>>
				<?php echo '5'; ?></option>
				<option value="6" <?php echo ($instance['column']=='6'?'selected':''); ?>>
				<?php echo '6'; ?></option>
			</select>

			<p>
			<label for="<?php echo $this->get_field_id( 'client_effect' ); ?>"><?php _e( 'Animation','graphite' ); ?></label><br/> 
			<select id="<?php echo $this->get_field_id( 'client_effect' ); ?>" name="<?php echo $this->get_field_name( 'client_effect' ); ?>">
				<option value>--<?php echo __('Select animation','graphite'); ?>--</option>
				<option value="slide" <?php echo ($instance['client_effect']=='slide'?'selected':''); ?>><?php echo __('Slide','graphite'); ?></option>
				<option value="fade" <?php echo ($instance['client_effect']=='fade'?'selected':''); ?>><?php echo __('Fade','graphite'); ?></option>
			</select>
		</p>
		</p>
	<?php
    }
	     
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
	
		$instance = array();
		$instance['total_client'] = ( ! empty( $new_instance['total_client'] ) ) ? $new_instance['total_client'] : '';
		$instance['target'] = ( ! empty( $new_instance['target'] ) ) ? $new_instance['target'] : '';
		$instance['column'] = ( ! empty( $new_instance['column'] ) ) ? $new_instance['column'] : '';
		$instance['client_effect'] = ( ! empty( $new_instance['client_effect'] ) ) ? $new_instance['client_effect'] : '';
		
		$total_client = $new_instance['total_client'];
		$client_list  = array();
		for($i=1; $i<= $total_client; $i++)
		{	
			$client_image ='client_image_'.$i;
			$client_image = $_POST[$client_image];
			$client_url   ='client_url_'.$i;
			$client_url = $_POST[$client_url];	
			$client_title   ='client_title_'.$i;
			$client_title = $_POST[$client_title];

			$client_list[$i]= array('client_title' => $client_title, 'client_url' => $client_url , 'client_image_url' => $client_image );
		}
		$instance['client_list'] = $client_list;
		
		return $instance;
	}
}
<?php
function graphite_slider_customizer( $wp_customize ) {
	
	
// list control categories	
if ( ! class_exists( 'WP_Customize_Control' ) ) return NULL;

 class Category_Dropdown_Custom_Control1 extends WP_Customize_Control
 {
    private $cats = false;
	
    public function __construct($wp_customize, $id, $args = array(), $options = array())
    {
        $this->cats = get_categories($options);
        parent::__construct( $wp_customize, $id, $args );
    }

    public function render_content()
       {
            if(!empty($this->cats))
            {
                ?>
                    <label>
                      <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
                      <select multiple="multiple" <?php $this->link(); ?>>
                           <?php
                                foreach ( $this->cats as $cat )
                                {
                                    printf('<option value="%s" %s>%s</option>', $cat->term_id, selected($this->value(), $cat->term_id, false), $cat->name);
                                }
                           ?>
                      </select>
                    </label>
                <?php
            }
       }
 }	

	//slider Section 
	$wp_customize->add_panel( 'graphite_slider_setting', array(
		'priority'       => 400,
		'capability'     => 'edit_theme_options',
		'title'      => __('Slider section', 'graphite'),
	) );
	
	
	/* slider settings */
	$wp_customize->add_section( 'slider_settings' , array(
		'title'      => __('Slider settings', 'graphite'),
		'panel'  => 'graphite_slider_setting',
   	) );
	
	
	$wp_customize->add_setting(
			'slider_enabled',
			array(
				'default' => true,
				'capability'     => 'edit_theme_options',
				'sanitize_callback' => 'sanitize_text_field',
			)	
			);
			$wp_customize->add_control(
			'slider_enabled',
			array(
				'label' => __('Enable slider from homepage','graphite'),
				'section' => 'slider_settings',
				'type' => 'checkbox',
			));
	
	
	// add section to manage featured slider on category basis	
	$wp_customize->add_setting(
    'slider_select_category',
    array(
        'default' => 2,
		'capability'     => 'edit_theme_options',
		));	
	$wp_customize->add_control( new Category_Dropdown_Custom_Control1( $wp_customize, 'slider_select_category', array(
    'label'   => __('Select category for slider','graphite'),
    'section' => 'slider_settings',
    'settings'   =>  'slider_select_category',
	) ) );
	
	//Slider animation
	$wp_customize->add_setting(
    'slider_options',
    array(
        'default' => 'slide',	
    ));

	$wp_customize->add_control('slider_options',
		array(
			'type' => 'select',
			'label' => __('Select slider animation','graphite'),
			'section' => 'slider_settings',
			 'choices' => array('slide'=>__('Slide', 'graphite'), 'carousel-fade'=>__('Fade', 'graphite')),
		));
		
	//Slider Animation duration
	$wp_customize->add_setting(
    'slider_transition_delay',
    array(
        'default' => '3000',
		'sanitize_callback' => 'sanitize_text_field',
    ));
	$wp_customize->add_control(
    'slider_transition_delay',
    array(
        'type' => 'text',
        'label' => __('Duration','graphite'),
        'section' => 'slider_settings',
		));	
		
		
	$wp_customize->add_setting(
		'enable_mobile_video',
		array(
			'default' => false,
		)	
		);
		$wp_customize->add_control(
		'enable_mobile_video',
		array(
			'label' => __('Show video on mobile device (only for portrait mode)','graphite'),
			'section' => 'slider_settings',
			'type' => 'checkbox',
		));

	
	//Slider Image height
	$wp_customize->add_setting(
    'slider_slide_height',
    array(
        'default' => __('200','graphite'),
		'sanitize_callback' => 'sanitize_text_field',
    ));
	$wp_customize->add_control(
    'slider_slide_height',
    array(
        'type' => 'text',
        'label' => __('Height of slider on mobile device (only for portrait mode)','graphite'),
        'section' => 'slider_settings',
	));
		
	
	}
	add_action( 'customize_register', 'graphite_slider_customizer' );
	?>
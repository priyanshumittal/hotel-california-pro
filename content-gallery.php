<?php
/**
 * The default template for "No Posts Found" message
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'post-content-area wow fadeInDown animated' ); ?> data-wow-delay="0.4s">

		<?php if(get_theme_mod('blog_title_position_enable',false) == true){ ?>
		<div class="entry-header">
			<?php
				if ( is_single() ) :
					the_title( '<h2 class="entry-title">', '</h2>' );
				else :
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' );
				endif;
			?>
		</div>	
		<?php } ?>
		
		<?php if ( is_single() ) {
			echo '<div class="blog-featured-img">';
			the_post_thumbnail( '', array( 'class'=>'img-responsive','alt' => get_the_title() ) );
			echo '</div>';
		}else{
			echo '<div class="blog-featured-img"><a class="post-thumbnail" href="'.get_the_permalink().'">';
			the_post_thumbnail( '', array( 'class'=>'img-responsive','alt' => get_the_title() ) );
			echo '</a></div>';
		} ?>
		
		<?php 
		//remove_shortcode('gallery');
		$gallery_content = get_the_content();
		preg_match('/\[gallery.*ids=.(.*).\]/', $gallery_content, $ids);
		
		if(!empty($ids[1])):
		$image_ids = explode(",", $ids[1]);
		$count = 0; 
		?>
		<section class="gallery-content">
			<div id="carousel-example-generic<?php the_ID(); ?>" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner" role="listbox">
						<?php
						foreach($image_ids as $image_id) {
							$image =  wp_get_attachment_image_src( $image_id, 'large', false, '' );
							echo '<div class="item '.($count==1?'active':'').'"><img src="'.$image[0].'" class="img-responsive" alt="img"></div>';
							$count++;
						}
						?>  
					</div>  
					<!-- Pagination --> 
					<ul class="carou-direction-nav">
						<li><a class="carou-prev" href="#carousel-example-generic<?php the_ID(); ?>" data-slide="prev"></a></li>
						<li><a class="carou-next" href="#carousel-example-generic<?php the_ID(); ?>" data-slide="next"></a></li>
					</ul> 
					<!-- /Pagination -->
			</div>
		</section>
		<?php endif; ?>
		
		<div class="post-content">
		
			<?php if(get_theme_mod('blog_title_position_enable',false) == false){ ?>
			<div class="entry-header">
				<?php
					if ( is_single() ) :
						the_title( '<h2 class="entry-title">', '</h2>' );
					else :
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' );
					endif;
				?>
			</div>	
			<?php } ?>
			
			<?php graphite_blog_meta_content(); ?>
			
			<div class="entry-content">
			<?php the_content(__('Read More','graphite')); ?>
			</div>
			
			
			<?php
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages', 'graphite' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
			
			// Edit link
			edit_post_link( __( 'Edit', 'graphite' ), '<span class="edit-link">', '</span>' );
			?>							
		</div>
</article>
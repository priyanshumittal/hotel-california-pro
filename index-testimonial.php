<!-- Testimonial Section -->
<?php
if( get_theme_mod('testimonial_section_enable') == false ) {
	
 $testimonial_callout_background = get_theme_mod('testimonial_callout_background',''); 	
 
if ( 
	
	get_theme_mod('home_testimonial_section_title') != '' || 
					
	get_theme_mod('home_testimonial_section_discription') != '' ||
	
	is_active_sidebar( 'graphite_middle_sidebar' )
	
	) : 

	if($testimonial_callout_background != '') { ?>
	<section class="wbr-section wbr-typography" id="testimonial" style="background-image:url('<?php echo $testimonial_callout_background;?>'); background-repeat: no-repeat; background-position: top left;">
	<?php } else { ?>
	<section class="wbr-section wbr-typography" id="testimonial" style="background-color:#ccc;">
	<?php } ?>

		<div class="overlay">
			<div class="container">	
			
			<?php if( 
					
					get_theme_mod('home_testimonial_section_title') != '' || 
					
					get_theme_mod('home_testimonial_section_discription') != ''
					
					) { ?>
            
			<!-- Section Title -->
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h1 class="white wow fadeInUp animated" data-wow-delay="0ms" data-wow-duration="500ms">
						<?php echo get_theme_mod('home_testimonial_section_title',''); ?>
						</h1>
						<div class="separator"><span></span></div>
						<p class="white wow fadeInDown animated">
						<?php echo get_theme_mod('home_testimonial_section_discription',''); ?>
						</p>
					</div>
				</div>
			</div>
			<!-- /Section Title -->
			<?php } 
			
			if( is_active_sidebar( 'graphite_middle_sidebar' ) ):
			
				echo '<div class="row">';
				
					dynamic_sidebar( 'graphite_middle_sidebar' );
					
				echo '</div>';
				
			endif;
			?>
			
			</div>	
			
		</div>
		
	</section>
	<!-- /Testimonial Section -->
	
	<div class="clearfix"></div>
<?php endif; } ?>
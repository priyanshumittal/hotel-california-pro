<?php
if(get_theme_mod('cta_section_enable') == false) {
if( is_active_sidebar('cta_sidebar_left') || is_active_sidebar('cta_sidebar_center') || is_active_sidebar('cta_sidebar_right') ) : ?>
<section class="cta-section" id="cta">
  <div class="top-contact-detail-section">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<?php if( is_active_sidebar('cta_sidebar_left') ) :
				if ( function_exists('dynamic_sidebar')) :
				dynamic_sidebar( 'cta_sidebar_left' );
				endif;
				endif; ?>
			</div>
			
			<div class="col-md-4">
				<?php if( is_active_sidebar('cta_sidebar_center') ) :
				if ( function_exists('dynamic_sidebar')) :
				dynamic_sidebar( 'cta_sidebar_center' );
				endif;
				endif; ?>
			</div>
			
			<div class="col-md-4">
				<?php if( is_active_sidebar('cta_sidebar_right') ) :
				if ( function_exists('dynamic_sidebar')) :
				dynamic_sidebar( 'cta_sidebar_right' );
				endif;
				endif; ?>
			</div>
		</div>
	</div>
  </div>
</section>
<?php endif; ?>
<div class="clearfix"></div>
<?php } ?>
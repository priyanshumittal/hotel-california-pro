<!--Logo & Menu Section-->	
<nav class="navbar navbar-default">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			        <?php the_custom_logo(); ?>

					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php
					$description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
						<p class="site-description"><?php echo $description; ?></p>
					<?php endif; ?>
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
		</div>
		

		<!-- Collect the nav links, forms, and other content for toggling -->
		<?php 
		$reservation_enable = get_theme_mod('reservation_enable',false);
		$reservation_title = get_theme_mod('reservation_title',__('RESERVATION','graphite'));
		$reservation_link = get_theme_mod('reservation_link','#');
		$reservation_target = get_theme_mod('reservation_target',true); 
		
		$shop_button = '<ul class="%2$s">%3$s';
			$shop_button .= '<div class="sm-top-btn">';
				if( $reservation_title != null ):
					$shop_button .= '<a '.( !empty($reservation_link)?'href="'.$reservation_link.'"':'').' '.( $reservation_target == 1?'target="_blank"':'').'>'.$reservation_title.'</a>';
				endif;
				
				if ( class_exists( 'WooCommerce' ) ) {
					global $woocommerce; 
					$shop_button .= '<a class="minicart-link" href="'.$woocommerce->cart->get_cart_url().'" >';
					if ($woocommerce->cart->cart_contents_count == 0){
							$shop_button .= '<i class="fa fa-shopping-cart" aria-hidden="true"></i>';
						}
						else
						{
							$shop_button .= '<i class="fa fa-cart-plus" aria-hidden="true"></i>';
						}
						   
						$shop_button .= '</a>';
						
						$shop_button .= '<a class="minicart-link" href="'.$woocommerce->cart->get_cart_url().'" >
							'.sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'graphite'), $woocommerce->cart->cart_contents_count).'</a>';
				}
		$shop_button .= '</ul>';
			
		?>
		
		
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<?php wp_nav_menu( array(
								'theme_location' => 'primary',
								'container'  => 'nav-collapse collapse navbar-inverse-collapse',
								'menu_class' => 'nav navbar-nav navbar-right',
								'fallback_cb' => 'graphite_fallback_page_menu',
								'items_wrap'  => $shop_button,
								'walker' => new webriti_nav_walker() 
							) ); 
						?>
				
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>	
<!--/Logo & Menu Section-->	

<div class="clearfix"></div>
<div class="row">
<?php 
			$width = '540';
			$height = '360';

			$parsedUrl  = parse_url(get_the_content());
			
			if(array_key_exists("host",$parsedUrl))
			{
				$enable_mobile_video = get_theme_mod('enable_mobile_video',false);
				if($enable_mobile_video != true)
				{
				echo '<div class="col-md-6 col-sm-6 col-xs-6 mobile-slider">';
				}
				else
				{
				echo '<div class="col-md-6 col-sm-6 col-xs-6">';
				}
				
				//specified condition for YouTube URL
				if($parsedUrl['host'] == 'www.youtube.com' || $parsedUrl['host'] == 'youtube.com')	{
				//get Youtube id from URL
				$embed = $parsedUrl['query'];
				parse_str($embed, $out);
				$embedUrl   = $out['v'];
				$embedUrl = explode('__',$embedUrl);
				$url = '//www.youtube.com/embed/'.$embedUrl[0] ;
				
				//echo the embed code for you tube
				echo '<div class="video-frame"><iframe width="'.$width.'" height="'.$height.'" src="'.$url.'" frameborder="0" allowfullscreen></iframe></div>';
				}
				
				//specified condition for vimeo URL
				if($parsedUrl['host'] == 'www.vimeo.com' || $parsedUrl['host'] == 'vimeo.com') {
				//get vimeo id from URL	
				$urlid = $parsedUrl['path'];
				$number = preg_replace("/[^0-9]/", '', $urlid);
				$url = '//player.vimeo.com/video/'.$number ;
			  
			  
				//echo $url;
				//echo the embed code for Vimeo
				echo '<div class="video-frame"><iframe width="'.$width.'" height="'.$height.'" src="'.$url.'" frameborder="0" allowfullscreen></iframe></div>';
				}
				
				//specified condition for vimeo URL
				if($parsedUrl['host'] == 'www.videopress.com' || $parsedUrl['host'] == 'videopress.com') {
				//get vimeo id from URL	
				$urlid = $parsedUrl['path'];
				//$number = preg_replace("/[^0-9]/", '', $urlid);
				$url = '//videopress.com'.$urlid ;
				//echo the embed code for Vimeo
				echo '<iframe width="'.$width.'" height="'.$height.'" src="'.$url.'" frameborder="0" allowfullscreen></iframe>
				<script src="https://videopress.com/videopress-iframe.js"></script>';
				}
				if($enable_mobile_video != true)
				{
				echo '</div>';
				}
				else
				{
				echo '</div>';
				}
				
			}
			
			
			?>

		<div class="col-md-<?php echo (array_key_exists("host",$parsedUrl)?'6':'12'); ?> col-sm-6 col-xs-6">
			<div class="video-content">
			<h1><?php the_title();?></h1>
			<?php echo get_slider_video_excerpt(); ?>
			</div>
		</div>   	
	</div>
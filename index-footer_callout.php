<?php 
$call_action_section_enable = get_theme_mod('call_action_section_enable',false);
if( $call_action_section_enable != true )
{
?>
<!-- Callout Section -->
		<?php 
			if( is_active_sidebar('graphite_bottom_footer_sidebar') ) :
				echo '<section class="wbr-section" id="callout"><div class="container"><div class="row">';
				dynamic_sidebar( 'graphite_bottom_footer_sidebar' );
				echo '</div></div></section>';
			endif;
		?>
<!-- /Callout Section -->
<?php } ?>
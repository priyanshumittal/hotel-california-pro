<?php 
$header_one_name = get_theme_mod('header_one_name','');
$header_one_text = get_theme_mod('header_one_text','');
if ( get_header_image() != '') {?>
<div class="header-img">
	<div class="header-content">
		<?php if($header_one_name != '') { ?>
		<h1><?php echo $header_one_name;?></h1>
		<?php }  if($header_one_text != '') { ?>
		<h3><?php echo $header_one_text ;?></h3>
		<?php } ?>
	</div>
	<img class="img-responsive" src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />
</div>
<?php } ?>
<!--/Top Header Detail-->
<section class="wbr-section wbr-typography top-header-detail">
	<div class="container">
		<div class="row">
		
			<div class="col-sm-6">
				<div id="top-header-sidebar-left">
				<?php 
				if( is_active_sidebar('home-header-sidebar_left') ) 
				{
						dynamic_sidebar( 'home-header-sidebar_left' ); 
				} 
				?>
				</div>
			</div>
			
			<div class="col-sm-6">
				<div id="top-header-sidebar-right">
				<?php 
				if( is_active_sidebar('home-header-sidebar_right') ) {
				dynamic_sidebar( 'home-header-sidebar_right' );
				} 
				?>
				</div>
			</div>	
			
		</div>	
	</div>
</section>
<!--/Top Header Detail-->
<div class="clearfix"></div>
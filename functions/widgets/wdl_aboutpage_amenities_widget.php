<?php
	// Register and load the widget
	function graphite_aboutpage_amenities_widget() {
	    register_widget( 'graphite_aboutpage_amenities_widget' );
	}
	add_action( 'widgets_init', 'graphite_aboutpage_amenities_widget' );

// Creating the widget
	class graphite_aboutpage_amenities_widget extends WP_Widget {
	 
	function __construct() {
		parent::__construct(
			'graphite_aboutpage_amenities_widget', // Base ID
			__('WBR: About page amenities','graphite'), // Widget Name
			array(
				'classname' => 'graphite_aboutpage_amenities_widget',
				'description' => __('About page amenities area','graphite'),
			),
			array(
				'width' => 600,
			)
		);
		
	 }
	
	public function widget( $args, $instance ) {
		
	$custom_class=(isset($instance['custom_class'])?$instance['custom_class']:'');

	if($custom_class !='')
	{
		$args['before_widget'] = str_replace('class="', 'class="'. $custom_class . ' ',$args['before_widget']);		
	}
	
	echo $args['before_widget']; 
	
	if($args['id']=='sidebar_primary')
	{
		$instance['before_title']='<div class="sm-widget-title wow fadeInDown animated" data-wow-delay="0.4s"><h3>';
		$instance['before_title']='</h3></div><div class="sm-sidebar-widget wow fadeInDown animated" data-wow-delay="0.4s">';
		
		echo $args['before_title'] .'Custom Amenity'. $args['after_title'];
	}
	?>
	     
			<span class="sm-amenities">
			<?php if(!empty($instance['fa_icon'])) { ?>
				<i class="fa <?php echo $instance['fa_icon']; ?>"></i>
				<?php } else { ?> 
				<i class="fa fa-car"></i>
				<?php } if(!empty($instance['description'])) { ?>
				<?php echo $instance['description']; ?>
				<?php } else { ?> 
				<?php _e('Free parking','graphite'); ?>
				<?php } ?>	
			</span>

	<?php
	echo $args['after_widget'];
	}
	         
	// Widget Backend
	public function form( $instance ) {
	if ( isset( $instance[ 'fa_icon' ])){
	$fa_icon = $instance[ 'fa_icon' ];
	}
	else {
	$fa_icon = 'fa fa-car';
	}
	if ( isset( $instance[ 'description' ])){
	$description = $instance[ 'description' ];
	}
	else {
	$description = __('Free parking','graphite' );
	}
	$instance['custom_class'] = (isset($instance['custom_class'])? $instance['custom_class']: '');

	// Widget admin form
	?>
	
	<label for="<?php echo $this->get_field_id( 'fa_icon' ); ?>"><?php _e('Get your Font Awesome','graphite' ); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id( 'fa_icon' ); ?>" name="<?php echo $this->get_field_name( 'fa_icon' ); ?>" type="text" value="<?php if($fa_icon) echo esc_attr( $fa_icon ); else echo 'fa fa-car';?>" />
	<span><?php _e('Link to get fa-icon ','graphite'); ?><a href="<?php echo 'http://fortawesome.github.io/Font-Awesome/icons/'; ?>" target="_blank" ><?php echo 'fa-icon'; ?></a></span>
	
	<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e( 'Description','graphite' ); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>" type="text" value="<?php if($description) echo esc_attr($description); else _e( 'Free parking', 'graphite' );?>" /><br><br>
	
	<p>
		<label for="<?php echo $this->get_field_id( 'custom_class' ); ?>"><?php _e('CSS Classes (optional)','graphite' ); ?></label> 
	</p>
	<input class="widefat" id="<?php echo $this->get_field_id( 'custom_class' ); ?>" name="<?php echo $this->get_field_name( 'custom_class' ); ?>" type="text" value="<?php if($instance[ 'custom_class' ]) echo esc_attr($instance[ 'custom_class' ]);?>" />
	
	<?php
    }
	     
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
	
		$instance = array();
		$instance['fa_icon'] = ( ! empty( $new_instance['fa_icon'] ) ) ? strip_tags( $new_instance['fa_icon'] ) : '';
		$instance['description'] = ( ! empty( $new_instance['description'] ) ) ? $new_instance['description'] : '';
		$instance['custom_class'] = ( ! empty( $new_instance['custom_class'] ) ) ? $new_instance['custom_class'] : '';
		
		return $instance;
	}
	}